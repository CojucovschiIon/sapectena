﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EmailSenderService
{
    [DataContract]
    public class CompanyProduct
    {
        [DataMember]
        public string Article { get; set; }
        [DataMember]
        public string Size { get; set; }
        [DataMember] public int? BagCount { get; set; }
        [DataMember] public int? Pieces { get; set; }
        [DataMember] public string BackgroundColor { get; set; }
        [DataMember] public string Category { get; set; }
    }
    [DataContract]
    public class Companies
    {
        [DataMember] public List<CompanyProduct> companyProducts { get; set; }
        [DataMember] public string CompanyName { get; set; }
        [DataMember] public int CompanyId { get; set; }
    }
}