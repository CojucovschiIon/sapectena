﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EmailSenderService.HtmlGenerator
{
    public class EmailGenerator
    {
        List<Companies> companies = new List<Companies>();
        public EmailGenerator()
        {
            for(int i=0;i<10; i++){
                List<CompanyProduct> prods = new List<CompanyProduct>();
                for (int j = 0; j < 40; j++)
                {
                    prods.Add(new CompanyProduct { Article = "some" + j, Size = "XXL" + j, BagCount = 30 + j, Pieces = 1 + j,BackgroundColor=j%2==0?"#cdcdcd":"#333" });

                }
                companies.Add(new Companies { CompanyId = i, CompanyName = "Company NAme" + i, companyProducts = prods });
            }
        }

        public string renderHtmlPageMessage(List<Companies> companiesList)
        {
            string response = "<!DOCTYPE html PUBLIC ' -//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>";
            string hreader = @"<html xmlns='http://www.w3.org/1999/xhtml'>
                                 <head>
                                  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
                                       <title>Email Design</title>
                                          <meta name = 'viewport' content = 'width=device-width, initial-scale=1.0' />
                                </head> ";
                                           //<h1>Hello!</h1><br/>
                                           //<h3>This message is confidential</h3>

            string bodddy = @"<body style='margin: 0; padding: 0;'>
                                     <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                      <tr>
                                       <td>
                                        <table align='center' border='0' cellpadding='0' cellspacing='0' width='600'>
                                         <tr>
                                          <td style='padding: 0 0 0 0; '>

                                          </td>
                                         </tr>
                                         <tr>
                                          <td style='padding: 20px 0 30px 0; '>
                                              <div style='
                                      overflow: auto;
                                      white-space: nowrap;'>
                                                " +
                                                RenderCompanyProducts(companiesList)
                                                + @"
                                             </div>
                                          </td>
                                         </tr>
                                         <tr>
                                          <td>
                                          </td>
                                         </tr>
                                        </table>
                                       </td>
                                      </tr>
                                     </table>
                                    </body>";
            string footer = "</html>";
            return response + hreader + bodddy + footer;

        }


        private string RenderCompanyProducts(List<Companies> companies)
        {
            string response = "";
            foreach (var elm in companies)
            {
                /// width='260' height='600'
                if (elm.companyProducts.Any(itm => !string.IsNullOrEmpty(itm.Article)))
                {
                    response += @"<div width='360' style='display: inline-block;
                                  border: 1px double black;
                                  border-radius: 25px;
                                  color: #000000;
                                  text-align: center;
                                  padding-top: 14px;
                                  padding-bottom: 14px;
                                  margin:10px;
                                  text-decoration: none;'>
                                    <div style='font-size:30px;'>" + elm.CompanyName + @"</div><br/>
                                   <table style='width:100%;height:25px;'><tbody>
                                    <tr>
                                    <td width='89' style='font-size:24px;'>Article</td>" +
                                 "<td width='89' style='font-size:24px;'>Size</td>" +
                                 "<td width='89' style='font-size:24px;'>Bag Count</td>" +
                                 "<td width='89' style='font-size:24px;'>Pieces</td></tr></tbody></table>";

                    foreach (var prod in elm.companyProducts)
                    {///+ prod.BackgroundColor +
                        response += @"<table style='width:100%;height:25px; background-color:#eeeeee;'><tbody>
                                    <tr>
                                    <td width='89'>" + prod.Article + "</td>" +
                                        "<td width='89'>" + prod.Size + "</td>" +
                                        "<td width='89'>" + prod.BagCount + "</td>" +
                                        "<td width='89'>" + prod.Pieces + "</td></tr></tbody></table>";
                    }
                    response += "</div>";
                }
            }
            return response;
        }
    }
}
