﻿using EmailSenderService.HtmlGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace EmailSenderService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public bool SendEmailService(List<Companies> models, string toClientEmail)
        {
              string SMTPSERVER = "smtp.gmail.com";
              int PORTNO = 587;
            EmailGenerator emaiGenerator = new EmailGenerator();

            SmtpClient smtpClient = new SmtpClient(SMTPSERVER, PORTNO);
            smtpClient.EnableSsl = true;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new NetworkCredential("essitynoreply@gmail.com", "essity1994");
            using (MailMessage message = new MailMessage())
            {
                message.From = new MailAddress(toClientEmail);
                message.Subject = "Comparation result";
                message.Body = emaiGenerator.renderHtmlPageMessage(models);
                message.IsBodyHtml = true;

                message.To.Add(toClientEmail);

                //if (ccemailTo != null && ccemailTo.Length > 0)
                //{
                //    foreach (string emailCc in ccemailTo)
                //    {
                //        message.CC.Add(emailCc);
                //    }
                //}
                try
                {
                    smtpClient.Send(message);
                    return true;
                }
                catch(Exception ex)
                {
                    string mesa = ex.Message;
                    return false;
                }
            }
        }
    }
}
