using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using UIKit;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ContentPage), typeof(SCAMobile.iOS.Renderers.SCAContentPageRenderer))]
namespace SCAMobile.iOS.Renderers
{
    public class SCAContentPageRenderer : PageRenderer
    {
        public override void WillMoveToParentViewController(UIViewController parent)
        {
            base.WillMoveToParentViewController(parent);

            var page = (ContentPage)Element;

            if (!string.IsNullOrEmpty(page.Title))
            {
                UIView titleView = new UIView();

                UILabel titleLabel = new UILabel();
                titleLabel.LineBreakMode = UILineBreakMode.TailTruncation;
                titleLabel.Lines = 1;
                titleLabel.Text = page.Title;
                titleLabel.TextColor = UIColor.White;
                titleLabel.Font = UIFont.SystemFontOfSize(17, UIFontWeight.Bold);
                titleLabel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
                titleLabel.TextAlignment = UITextAlignment.Center;

                //Be sure to set the Frame to 'something' other than the default 0,0,0,0 otherwise you won't see anything!
                titleLabel.Frame = new CoreGraphics.CGRect(0, 0, 1, 1);
                titleLabel.SizeToFit();

                UILabel confidentialityLabel = new UILabel();
                confidentialityLabel.Lines = 1;
                confidentialityLabel.Text = @"CONFIDENTIAL � FOR INTERNAL USE ONLY";
                confidentialityLabel.TextColor = UIColor.White;
                confidentialityLabel.Frame = new CoreGraphics.CGRect(0, titleLabel.Frame.Height + 1, 1, 1);
                confidentialityLabel.Font = UIFont.SystemFontOfSize(13);
                confidentialityLabel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
                confidentialityLabel.TextAlignment = UITextAlignment.Center;
                confidentialityLabel.SizeToFit();

                titleView.AddSubview(titleLabel);
                titleView.AddSubview(confidentialityLabel);
                titleView.Frame = new CoreGraphics.CGRect(
                    0, 0, 
                    Math.Max(titleLabel.Frame.Width, confidentialityLabel.Frame.Width),
                    confidentialityLabel.Frame.Y + confidentialityLabel.Frame.Height);

                confidentialityLabel.Frame = new CoreGraphics.CGRect(
                    0, confidentialityLabel.Frame.Y,
                    titleView.Frame.Width,
                    confidentialityLabel.Frame.Height);

                titleLabel.Frame = new CoreGraphics.CGRect(
                    0, titleLabel.Frame.Y,
                    titleView.Frame.Width,
                    titleLabel.Frame.Height);

                parent.NavigationItem.TitleView = titleView;
            }
        }
    }
}