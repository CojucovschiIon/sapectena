using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using UIKit;
using Xamarin.Forms.Platform.iOS;
using SCAMobile.Controls;

[assembly: ExportRenderer(typeof(SCAEntry), typeof(SCAMobile.iOS.Renderers.SCAEntryRenderer))]
namespace SCAMobile.iOS.Renderers
{
    public class SCAEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            SCAEntry entry = (SCAEntry)this.Element;

            if (this.Control != null)
            {
                if (entry != null)
                {
                    Control.ShouldReturn += (UITextField tf) =>
                    {
                        entry.InvokeCompleted();
                        return true;
                    };
                }
            }
        }
    }
}
