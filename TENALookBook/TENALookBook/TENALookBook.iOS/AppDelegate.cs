﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using CrashlyticsKit;
using FabricSdk;
using CarouselView.FormsPlugin.iOS;
using FFImageLoading.Forms.Touch;

namespace SCAMobile.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            Crashlytics.Instance.Initialize();
            Fabric.Instance.Debug = true;
            Fabric.Instance.Initialize();


            global::Xamarin.Forms.Forms.Init();
            CarouselViewRenderer.Init();
            CachedImageRenderer.Init();

            var t = new FFImageLoading.Transformations.RoundedTransformation(10);

            //            if (UserDialogs.Instance == null)
            //                UserDialogs.Init(() => { return Acr.Support.iOS.Extensions.GetTopViewController(app); });

            SCAMobile.App portableApp = new SCAMobile.App();
            portableApp.LoginCompleted += loginCompleted;
            portableApp.LogoutCompleted += logoutCompleted;
            portableApp.NotificationsRead += notificationsRead;

            LoadApplication(portableApp);

            return base.FinishedLaunching(app, options);
        }

        private void notificationsRead(object sender, EventArgs e)
        {
            BeginInvokeOnMainThread(() =>
            {
                UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
            });
        }

        private void logoutCompleted(object sender, EventArgs e)
        {
            BeginInvokeOnMainThread(() =>
            {
                UIApplication.SharedApplication.UnregisterForRemoteNotifications();
            });
        }

        private void loginCompleted(object sender, EventArgs e)
        {
            BeginInvokeOnMainThread(() =>
            {
                var settings = UIUserNotificationSettings.GetSettingsForTypes(
                    UIUserNotificationType.Alert
                    | UIUserNotificationType.Badge
                    | UIUserNotificationType.Sound,
                    new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
                UIApplication.SharedApplication.RegisterForRemoteNotifications();
            });
        }

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            App.notificationReceived();
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            App app = App.Current as App;
            app.DeviceToken = deviceToken.ToString().Replace(@" ", @"").Replace(@"<", @"").Replace(@">", @"");
        }

        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            new UIAlertView("Error registering push notifications", error.LocalizedDescription, null, "OK", null).Show();
        }
    }
}
