﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCAMobile.Constants
{
    public static class ConfigKeys
    {
        public const string LoginToken = @"LoginToken";
        public const string FirstName = @"FirstName";
        public const string LastName = @"LastName";
        public const string SCAId = @"SCAId";
        public const string ConfidentialStatementSeen = @"ConfidentialStatementSeen";
        public const string SearchConfidentialityNoticeSeen = @"SearchConfidentialityNoticeSeen";
        public const string RecentAccounts = @"RecentAccounts";
        public const string RecentComparisons = @"RecentComparisons";
        public const string DeviceToken = @"DeviceToken";
        public const string HasNewNotifications = @"HasNewNotifications";
        public const string LastLoginDate = @"LastLoginDate";
    }
}
