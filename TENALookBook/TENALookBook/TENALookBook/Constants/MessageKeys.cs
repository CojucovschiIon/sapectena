﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCAMobile.Constants
{
    public static class MessageKeys
    {
        public const string DisplayAlert = "DisplayAlert";
        public const string DisplayQuestion = "DisplayQuestion";
        public const string ComparisonAdded = "ComparisonAdded";
        public const string ComparisonDeleted = "ComparisonDeleted";
        public const string ComparisonUpdated = "ComparisonUpdated";
        public const string NotificationReceived = "NotificationReceived";
        public const string ResetNotificationBadge = "ResetNotificationBadge";
    }
}
