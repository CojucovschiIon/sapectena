﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace SCAMobile.Pages
{
    public partial class CatalogueSearchResults : ContentPage
    {
        private ObservableCollection<ProductSet> _products = new ObservableCollection<ProductSet>();

        public string Tab { get; set; }

        public ObservableCollection<ProductSet> Products
        {
            get { return _products; }
        }

        public CatalogueSearchResults(IList<SCAService.Product> products, string tab, bool showBottomTabBar = true)
        {
            Tab = tab;
            BindingContext = this;

            int cols = 2;
            int rows = 4;
            int numOfProductPages = products.Count() + (cols * rows - 1) / (cols * rows);
            int col = 0, row = 0;
            ProductSet productSet = new ProductSet();

            foreach (SCAService.Product p in products)
            {
                switch (col)
                {
                    case 0:
                        switch (row)
                        {
                            case 0:
                                productSet.Product1 = p;
                                break;

                            case 1:
                                productSet.Product3 = p;
                                break;

                            case 2:
                                productSet.Product5 = p;
                                break;

                            case 3:
                                productSet.Product7 = p;
                                break;
                        }
                        break;

                    case 1:
                        switch (row)
                        {
                            case 0:
                                productSet.Product2 = p;
                                break;

                            case 1:
                                productSet.Product4 = p;
                                break;

                            case 2:
                                productSet.Product6 = p;
                                break;

                            case 3:
                                productSet.Product8 = p;
                                break;
                        }
                        break;
                }

                if (++col == cols)
                {
                    col = 0;

                    if (++row == rows)
                    {
                        row = 0;
                        _products.Add(productSet);
                        productSet = new ProductSet();
                    }
                }
            }

            if (productSet.Product1 != null)
            {
                if (productSet.Product2 == null)
                    productSet.Product2 = new SCAService.Product();
                if (productSet.Product3 == null)
                    productSet.Product3 = new SCAService.Product();
                if (productSet.Product4 == null)
                    productSet.Product4 = new SCAService.Product();
                if (productSet.Product5 == null)
                    productSet.Product5 = new SCAService.Product();
                if (productSet.Product6 == null)
                    productSet.Product6 = new SCAService.Product();
                if (productSet.Product7 == null)
                    productSet.Product7 = new SCAService.Product();
                if (productSet.Product8 == null)
                    productSet.Product8 = new SCAService.Product();

                _products.Add(productSet);
            }

            InitializeComponent();

            navTabBar.IsVisible = showBottomTabBar;
        }

        private void onProductSelected(object sender, Controls.ProductSelectedEventArgs e)
        {
            Navigation.PushAsync(new SCAMobile.Pages.ViewProduct(e.Product, Tab, Tab == @"Catalogue", navTabBar.IsVisible));
        }
    }
}
