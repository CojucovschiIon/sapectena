﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace SCAMobile.Pages
{
    public partial class VideoViewer : ContentPage
    {
        public VideoViewer(SCAService.Product product, string url, string tab, bool navBarVisible)
        {
            Tab = tab;
            BindingContext = this;

            InitializeComponent();

            Title = product.MarketDesc;
            navBar.IsVisible = navBarVisible;

            string youtubeId = App.getYouTubeVideoId(url);

            if (string.IsNullOrWhiteSpace(youtubeId))
                webView.Source = url;
            else
                webView.Source = @"https://www.youtube.com/embed/" + youtubeId + @"?autoplay=1";
        }

        public string Tab { get; set; }
    }
}
