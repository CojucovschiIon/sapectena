﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using SCAMobile.Controls;
using FormsToolkit;
using SCAMobile.Constants;
using System.Collections.ObjectModel;

namespace SCAMobile.Pages
{
    public partial class AdvancedSearch : ContentPage
    {
        public static int ABSORPTION_MIN = 0;
        public static int ABSORPTION_MAX = 4000;

        private int _rowCount;
        private int _colCount = 1;
        private Command _resetCommand;

//        public int selectedAbsorptionMin { get; set; }
//        public int selectedAbsorptionMax { get; set; }

        public Command ResetCommand
        {
            get
            {
                return _resetCommand ?? (_resetCommand = new Command(resetForm));
            }
        }

        public AdvancedSearch()
        {
            BindingContext = this;

//            selectedAbsorptionMin = ABSORPTION_MIN;
//            selectedAbsorptionMax = ABSORPTION_MAX;

            InitializeComponent();

            Title = "Search Competitor Products";

            RangeSlider.MinimumValue = ABSORPTION_MIN;
            RangeSlider.MaximumValue = ABSORPTION_MAX;
            RangeSlider.LowerValue = ABSORPTION_MIN;
            RangeSlider.UpperValue = ABSORPTION_MAX;

            loadCategories();
            loadCompanies();
        }

        private void loadCategories()
        {
            App app = Application.Current as App;
            SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

            client.getAllCategoriesCompleted += getAllCategoriesCompleted;
            client.getAllCategoriesAsync(app.LoginToken);
        }

        private void getAllCategoriesCompleted(object sender, SCAService.getAllCategoriesCompletedEventArgs e)
        {

            Device.BeginInvokeOnMainThread(() =>
            {
                if (e.Error == null)
                {
                    _rowCount = e.Result.Count();

                    foreach (SCAService.Category cat in e.Result)
                    {
                        if (cat.Children.Count() > _colCount)
                            _colCount = cat.Children.Count();
                    }

                    for (int i = 0; i < _rowCount; i++)
                    {
                        RowDefinition rowDef = new RowDefinition
                        {
                            Height = GridLength.Auto
                        };

                        this.categories.RowDefinitions.Add(rowDef);
                    }

                    for (int i = 0; i < _colCount; i++)
                    {
                        ColumnDefinition colDef = new ColumnDefinition
                        {
                            Width = GridLength.Auto
                        };

                        this.categories.ColumnDefinitions.Add(colDef);
                    }

                    for (int i = 0; i < _rowCount; i++)
                    {
                        SCAService.Category cat = e.Result.ElementAt(i);
                        Image img = new Image
                        {
                            Source = cat.Logo,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.Center
                        };
                        this.categories.Children.Add(img, 0, i);
                        /*
                                            FFImageLoading.Forms.CachedImage image = new CachedImage
                                            {
                                                Source = cat.Logo,
                                                WidthRequest = 100
                                            };

                                            this.categories.Children.Add(image, 0, i);
                        */

                        for (int j = 0; j < cat.Children.Count(); j++)
                        {
                            SCAService.Category subCat = cat.Children.ElementAt(j);
                            CategoryButton categoryButton = new CategoryButton
                            {
                                CategoryId = subCat.Id,
                                Title = subCat.Name,
                                HorizontalOptions = LayoutOptions.Start
                            };

                            this.categories.Children.Add(categoryButton, j + 1, i);
                        }
                    }
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }

        private void loadCompanies()
        {
            App app = Application.Current as App;
            SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

            client.getCompaniesCompleted += getCompaniesCompleted;
            client.getCompaniesAsync(app.LoginToken);
        }

        private void getCompaniesCompleted(object sender, SCAService.getCompaniesCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (e.Error == null)
                {
                    ObservableCollection<SCAService.Company> activeCompanies = new ObservableCollection<SCAService.Company>(e.Result.Where(m => m.Active != 0));
                    int cols = 4;
                    int rows = e.Result.Count() / cols + (activeCompanies.Count() % cols != 0 ? 1 : 0);
                    int row = 0, col = 0;

                    for (int i = 0; i < rows; i++)
                    {
                        RowDefinition rowDef = new RowDefinition
                        {
                            Height = GridLength.Auto
                        };

                        this.companies.RowDefinitions.Add(rowDef);
                    }

                    for (int i = 0; i < cols; i++)
                    {
                        ColumnDefinition colDef = new ColumnDefinition
                        {
                            Width = GridLength.Auto
                        };

                        this.companies.ColumnDefinitions.Add(colDef);
                    }

                    foreach (SCAService.Company c in activeCompanies)
                    {
                        if (c.IsSCA != 0)
                            continue;

                        CompanyButton btn = new CompanyButton
                        {
                            CompanyId = c.Id,
                            CompanyName = c.Name,
                            CompanyLogo = c.Logo
                        };

                        this.companies.Children.Add(btn, col, row);

                        if (++col == cols)
                        {
                            col = 0;
                            row++;
                        }
                    }
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }

        private void categoryBtnClicked(object sender, EventArgs e)
        {
//            CategoryButton btn = sender as CategoryButton;
//            btn.Image = @"Selected Product BG.png";
        }

        private void resetForm()
        {
            this.searchTerm.Text = @"";
            this.sizeXS.Checked = false;
            this.sizeS.Checked = false;
            this.sizeM.Checked = false;
            this.sizeR.Checked = false;
            this.sizeL.Checked = false;
            this.sizeXL.Checked = false;
            this.sizeBari.Checked = false;
            this.RangeSlider.LowerValue = ABSORPTION_MIN;
            this.RangeSlider.UpperValue = ABSORPTION_MAX;
            //            this.selectedAbsorptionMin = ABSORPTION_MIN;
            //            this.selectedAbsorptionMax = ABSORPTION_MAX;
            //            this.absorptionMin.Text = ABSORPTION_MIN.ToString();
            //            this.absorptionMax.Text = ABSORPTION_MAX.ToString();

            foreach (CompanyButton companyBtn in this.companies.Children)
                companyBtn.Checked = false;

            foreach (var child in this.categories.Children)
            {
                if (child is CategoryButton)
                    ((CategoryButton)child).Checked = false;
            }
        }

        private void absorptionMinChanged(object sender, TextChangedEventArgs e)
        {
            float absorption;

            if (float.TryParse(this.absorptionMin.Text, out absorption))
            {
                if (absorption >= ABSORPTION_MIN && absorption <= ABSORPTION_MAX && absorption != this.RangeSlider.LowerValue)
                    this.RangeSlider.LowerValue = absorption;
            }
        }

        private void absorptionMaxChanged(object sender, TextChangedEventArgs e)
        {
            float absorption;

            if (float.TryParse(this.absorptionMax.Text, out absorption))
            {
                if (absorption >= ABSORPTION_MIN && absorption <= ABSORPTION_MAX && absorption != this.RangeSlider.UpperValue)
                    this.RangeSlider.UpperValue = absorption;
            }
        }

        private void absorptionSliderDragCompleted(object sender, EventArgs e)
        {
            this.absorptionMin.Text = RangeSlider.LowerValue.ToString();
            this.absorptionMax.Text = RangeSlider.UpperValue.ToString();
        }

        private void OnSearchClicked(object sender, EventArgs e)
        {
            App app = Application.Current as App;
            SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();
            ObservableCollection<int> companies = new ObservableCollection<int>();
            ObservableCollection<int> categories = new ObservableCollection<int>();
            ObservableCollection<string> sizes = new ObservableCollection<string>();

            foreach (CompanyButton c in this.companies.Children)
            {
                if (c.Checked)
                    companies.Add(c.CompanyId);
            }

            foreach (var child in this.categories.Children)
            {
                if (child is CategoryButton && ((CategoryButton)child).Checked)
                    categories.Add(((CategoryButton)child).CategoryId);
            }

            if (this.sizeXS.Checked)
                sizes.Add(@"XS");
            if (this.sizeS.Checked)
                sizes.Add(@"S");
            if (this.sizeM.Checked)
                sizes.Add(@"M");
            if (this.sizeR.Checked)
                sizes.Add(@"R");
            if (this.sizeL.Checked)
                sizes.Add(@"L");
            if (this.sizeXL.Checked)
                sizes.Add(@"XL");
            if (this.sizeBari.Checked)
                sizes.Add(@"BARI");

            App.ShowLoading(true, @"Searching");

            client.searchProductsCompleted += searchProductsCompleted;
            client.searchProductsAsync(
                this.searchTerm.Text, companies, categories, 
                (decimal)this.RangeSlider.LowerValue, (decimal)this.RangeSlider.UpperValue, 
                sizes, false, app.LoginToken);
        }

        private void searchProductsCompleted(object sender, SCAService.searchProductsCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                {
                    if (e.Result.Count() == 0)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                        {
                            Title = @"Information",
                            Cancel = @"OK",
                            Message = @"No products found"
                        });
                    }
                    else
                    {
                        Navigation.PushAsync(new SCAMobile.Pages.SearchResults(e.Result));
                    }
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }
    }
}
