﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace SCAMobile.Pages
{
    public partial class ConfidentialStatement : ContentPage
    {
        public ConfidentialStatement()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
        }

        void OnAcceptClicked(object sender, EventArgs e)
        {
            App app = Application.Current as App;

            app.MainPage = app.TabBar.getPage(MainUITabBar.PAGE_INDEX_SEARCH);
        }
    }
}
