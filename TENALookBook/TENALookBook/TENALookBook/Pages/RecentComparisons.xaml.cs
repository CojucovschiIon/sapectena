﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using FormsToolkit;
using SCAMobile.Constants;
using SCAMobile.Messaging;

namespace SCAMobile.Pages
{
    public partial class RecentComparisons : ContentPage
    {
        private class ComparisonViewModel : BindableObject
        {
            public static readonly BindableProperty IsNotLastProperty =
                BindableProperty.Create("IsNotLast", typeof(bool), typeof(ComparisonViewModel), false, BindingMode.TwoWay);

            public SCAService.Opportunity Comparison { get; set; }

            public string CompProductImage { get; set; }

            public string SCAProductImage { get; set; }

            public bool IsNotLast
            {
                get
                {
                    return (bool)GetValue(IsNotLastProperty);
                }

                set
                {
                    SetValue(IsNotLastProperty, value);
                }
            }
        }

        private ObservableCollection<ComparisonViewModel> _comparisons;

        public string Tab { get; set; }

        public RecentComparisons(IList<SCAService.Opportunity> comparisons, string tab, bool showTabBar)
        {
            Tab = tab;
            BindingContext = this;

            InitializeComponent();

            navTabBar.IsVisible = showTabBar;

            _comparisons = new ObservableCollection<ComparisonViewModel>(
                comparisons.Select(m => new ComparisonViewModel
                {
                    Comparison = m,
                    CompProductImage = App.getProductImage(m.CompProduct),
                    SCAProductImage = App.getProductImage(m.TenaProducts[0]),
                    IsNotLast = true
                })
            );

            if (_comparisons.Count > 0)
                _comparisons.Last().IsNotLast = false;

            comparisonList.ItemsSource = _comparisons;

            MessagingService.Current.Subscribe<MessagingServiceComparisonDeleted>(MessageKeys.ComparisonDeleted, (service, msg) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ComparisonViewModel comp = _comparisons.Where(m => m.Comparison.Id == msg.Id).FirstOrDefault();

                    if (comp != null)
                    {
                        _comparisons.Remove(comp);

                        if (_comparisons.Count > 0)
                            _comparisons.Last().IsNotLast = false;
                    }
                });
            });

            MessagingService.Current.Subscribe<MessagingServiceComparisonAdded>(MessageKeys.ComparisonAdded, (service, msg) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    _comparisons.Insert(0, new ComparisonViewModel
                    {
                        Comparison = msg.Comparison,
                        CompProductImage = App.getProductImage(msg.Comparison.CompProduct),
                        SCAProductImage = App.getProductImage(msg.Comparison.TenaProducts[0]),
                        IsNotLast = true
                    });
                });
            });

        }

        private void comparisonListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            comparisonList.SelectedItem = null;
        }

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            SCAService.Opportunity comparison = (e.Item as ComparisonViewModel).Comparison;

            Navigation.PushAsync(new ViewComparison(comparison, false, Tab, navTabBar.IsVisible));
        }

        private void OnSelectClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ViewComparison((SCAService.Opportunity)((Button)sender).CommandParameter, false, Tab, navTabBar.IsVisible));
        }
    }
}
