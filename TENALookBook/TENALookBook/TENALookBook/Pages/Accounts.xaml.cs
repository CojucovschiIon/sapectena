﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using FormsToolkit;
using SCAMobile.Constants;
using System.Collections.ObjectModel;

namespace SCAMobile.Pages
{
    public partial class Accounts : ContentPage
    {
        public Accounts()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (Device.OS == TargetPlatform.Windows)
            {
                NavigationPage.SetHasNavigationBar(this, true);
                NavigationPage.SetHasNavigationBar(this, false);
            }
        }

        private void OnSearchClicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.searchTerm.Text))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = @"Error",
                    Cancel = @"OK",
                    Message = @"Please enter Company Name, City or Account Number"
                });
            }
            else
            {
                App.ShowLoading(true, @"Searching");

                App app = Application.Current as App;
                SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

                client.searchAccountsCompleted += searchAccountsCompleted;
                client.searchAccountsAsync(this.searchTerm.Text, app.LoginToken);
            }
        }

        private void searchAccountsCompleted(object sender, SCAService.searchAccountsCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                {

                    if (e.Result.Count() == 0)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                        {
                            Title = @"Information",
                            Cancel = @"OK",
                            Message = @"No accounts found"
                        });
                    }
                    else
                    {
                        Navigation.PushAsync(new SCAMobile.Pages.AccountSearchResults(e.Result));
                    }
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }

        private void OnRecentlyAccessedAccountsClicked(object sender, EventArgs e)
        {
            App app = Application.Current as App;

            if (app.RecentAccounts.Count > 0)
            {
                App.ShowLoading(true, @"Loading");

                SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();
                client.getAccountsByIdCompleted += getAccountsByIdCompleted;
                client.getAccountsByIdAsync(new ObservableCollection<int>(app.RecentAccounts), app.LoginToken);
            }
            else
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = @"Information",
                    Cancel = @"OK",
                    Message = @"You have not accessed any account yet"
                });
            }
        }

        private void getAccountsByIdCompleted(object sender, SCAService.getAccountsByIdCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                    Navigation.PushAsync(new RecentAccounts(e.Result));
                else
                    App.scaServiceUnavailableErrorMessage(e.Error);
            });
        }
        private void onGridLayoutChanged(object sender, EventArgs e)
        {
            if (logoContainer.Y < formContainer.Y + recentAccounts.Y + recentAccounts.Height)
                logoImage.IsVisible = false;
            else
                logoImage.IsVisible = true;
        }

    }
}
