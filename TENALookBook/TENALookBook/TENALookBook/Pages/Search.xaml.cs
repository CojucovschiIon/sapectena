﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using FormsToolkit;
using SCAMobile.Constants;

namespace SCAMobile.Pages
{
    public partial class Search : ContentPage
    {
        public Search()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (Device.OS == TargetPlatform.Windows)
            {
                NavigationPage.SetHasNavigationBar(this, true);
                NavigationPage.SetHasNavigationBar(this, false);
            }

            if (!((App)Application.Current).SearchConfidentialityNoticeShown)
            {
                ((App)Application.Current).SearchConfidentialityNoticeShown = true;

                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = @"Notice",
                    Cancel = @"Continue",
                    Message = @"This app is CONFIDENTIAL and to be used internally"
                });
            }
        }

        void OnSearchClicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.searchTerm.Text))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = @"Error",
                    Cancel = @"OK",
                    Message = @"Please enter Product Name or Article Number"
                });
            }
            else
            {
                App.ShowLoading(true, @"Searching");

                App app = Application.Current as App;
                SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

                client.searchProductsCompleted += searchProductsCompleted;
                client.searchProductsAsync(this.searchTerm.Text, null, null, null, null, null, false, app.LoginToken);
            }
        }

        private void searchProductsCompleted(object sender, SCAService.searchProductsCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                {
                    if (e.Result.Count() == 0)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                        {
                            Title = @"Information",
                            Cancel = @"OK",
                            Message = @"No products found"
                        });
                    }
                    else
                    {
                        Navigation.PushAsync(new SCAMobile.Pages.SearchResults(e.Result));
                    }
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }

        void OnAdvancedSearchClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new SCAMobile.Pages.AdvancedSearch());
        }

        void OnRecentComparisonsClicked(object sender, EventArgs e)
        {
            App app = Application.Current as App;

            if (app.RecentComparisons.Count > 0)
            {
                App.ShowLoading(true, @"Loading");

                SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();
                client.getOpportunitiesByIdCompleted += getOpportunitiesByIdCompleted;
                client.getOpportunitiesByIdAsync(new ObservableCollection<int>(app.RecentComparisons), app.LoginToken);
            }
            else
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = @"Information",
                    Cancel = @"OK",
                    Message = @"You have not saved any comparisons yet"
                });
            }

        }

        private void getOpportunitiesByIdCompleted(object sender, SCAService.getOpportunitiesByIdCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                    Navigation.PushAsync(new RecentComparisons(e.Result, @"Search", true));
                else
                    App.scaServiceUnavailableErrorMessage(e.Error);
            });
        }

        protected override bool OnBackButtonPressed()
        {
            return true;// base.OnBackButtonPressed();
        }

        private void onGridLayoutChanged(object sender, EventArgs e)
        {
            if (logoContainer.Y < formContainer.Y + recentComparisons.Y + recentComparisons.Height)
                logoImage.IsVisible = false;
            else
                logoImage.IsVisible = true;
        }
    }
}
