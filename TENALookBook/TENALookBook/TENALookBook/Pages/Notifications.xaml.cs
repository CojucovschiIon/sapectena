﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using FormsToolkit;
using SCAMobile.Constants;

namespace SCAMobile.Pages
{
    public partial class Notifications : ContentPage
    {
        private class NotificationViewModel : BindableObject
        {
            public static readonly BindableProperty IsEditingProperty =
                BindableProperty.Create("IsEditing", typeof(bool), typeof(NotificationViewModel), false);

            public static readonly BindableProperty IsViewingProperty =
                BindableProperty.Create("IsViewing", typeof(bool), typeof(NotificationViewModel), true);

            public static readonly BindableProperty IsSelectedProperty =
                BindableProperty.Create("IsSelected", typeof(bool), typeof(NotificationViewModel), false, BindingMode.TwoWay);

            public static readonly BindableProperty IsNotLastProperty =
                BindableProperty.Create("IsNotLast", typeof(bool), typeof(NotificationViewModel), false, BindingMode.TwoWay);

            public SCAService.Notification Notification { get; set; }

            public bool IsEditing
            {
                get
                {
                    return (bool)GetValue(IsEditingProperty);
                }

                set
                {
                    SetValue(IsEditingProperty, value);
                    SetValue(IsViewingProperty, !value);
                }
            }

            public bool IsViewing
            {
                get
                {
                    return (bool)GetValue(IsViewingProperty);
                }

                set
                {
                    SetValue(IsViewingProperty, value);
                    SetValue(IsEditingProperty, !value);
                }
            }

            public bool IsSelected
            {
                get
                {
                    return (bool)GetValue(IsSelectedProperty);
                }

                set
                {
                    SetValue(IsSelectedProperty, value);
                }
            }

            public bool IsNotLast
            {
                get
                {
                    return (bool)GetValue(IsNotLastProperty);
                }

                set
                {
                    SetValue(IsNotLastProperty, value);
                }
            }

            public string CompProductImage { get; set; }

            public string SCAProductImage { get; set; }
        }

        private Command _refreshCommand;
        private Command _editCommand;
        private Command _deleteCommand;
        private Command _cancelCommand;
        private ObservableCollection<NotificationViewModel> _notifications;
        private List<ToolbarItem> _initialMenu;

        public Notifications()
        {
            BindingContext = this;

            InitializeComponent();

            MessagingService.Current.Subscribe<object>(MessageKeys.NotificationReceived, (service, info) => {
                Device.BeginInvokeOnMainThread(new Action(() =>
                {
                    if (Parent.Parent != null)
                        loadNotificationsFromPush();
                }));
            });

            _initialMenu = new List<ToolbarItem>(ToolbarItems);
            loadNotifications();

            Controls.NavigationTabBar.NavTabChanged += OnNavTabChanged;
        }

        private void OnNavTabChanged(object sender, Controls.NavTabChangedEventArgs e)
        {
            if (e.NewTab == @"Notifications")
            {
                loadNotifications();
            }
        }

        public Command RefreshCommand
        {
            get
            {
                return _refreshCommand ?? (_refreshCommand = new Command(loadNotifications));
            }
        }

        public Command EditCommand
        {
            get
            {
                return _editCommand ?? (_editCommand = new Command(editNotifications));
            }
        }

        public Command DeleteCommand
        {
            get
            {
                return _deleteCommand ?? (_deleteCommand = new Command(deleteNotifications));
            }
        }

        public Command CancelCommand
        {
            get
            {
                return _cancelCommand ?? (_cancelCommand = new Command(cancelEditNotifications));
            }
        }

        private void loadNotificationsFromPush()
        {
            App app = Application.Current as App;
            SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

            client.getNotificationsCompleted += getNotificationsCompleted;
            client.getNotificationsAsync(app.LoginToken, true);
        }

        private void loadNotifications()
        {
            App.ShowLoading(true, @"Loading");

            App app = Application.Current as App;
            SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

            client.getNotificationsCompleted += getNotificationsCompleted;
            client.getNotificationsAsync(app.LoginToken, false);
        }

        private void getNotificationsCompleted(object sender, SCAService.getNotificationsCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                bool fromPush = (bool)e.UserState;

                if (!fromPush)
                    App.ShowLoading(false);

                if (e.Error == null)
                {
                    App.resetNotifications();

                    _notifications = new ObservableCollection<NotificationViewModel>(
                        e.Result.Select(
                            m => new NotificationViewModel
                            {
                                Notification = m,
                                IsEditing = false,
                                IsNotLast = true,
                                CompProductImage = App.getProductImage(m.Opportunity.CompProduct),
                                SCAProductImage = App.getProductImage(m.Opportunity.TenaProducts[0])
                            }
                    ));

                    if (_notifications.Count > 0)
                        _notifications[_notifications.Count - 1].IsNotLast = false;

                    notificationList.ItemsSource = _notifications;
                }
                else
                {
                    if (!fromPush)
                        App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }

        private void editNotifications()
        {
            foreach (NotificationViewModel model in _notifications)
            {
                model.IsEditing = true;
                model.IsSelected = false;
            }

            ToolbarItems.Clear();
            ToolbarItems.Add(new ToolbarItem
            {
                Text = @"Delete",
                Command = DeleteCommand,
                Icon = (Device.OS == TargetPlatform.WinPhone || Device.OS == TargetPlatform.Windows) ? (FileImageSource)FileImageSource.FromFile(@"trash-128.png") : null
            });

            ToolbarItems.Add(new ToolbarItem
            {
                Text = @"Cancel",
                Command = CancelCommand,
                Icon = (Device.OS == TargetPlatform.WinPhone || Device.OS == TargetPlatform.Windows) ? (FileImageSource)FileImageSource.FromFile(@"cancel-icon.png") : null
            });

            navBar.IsVisible = false;
//            selectButtonSeparator.IsVisible = true;
            selectButtonContainer.IsVisible = true;
            selectButton.Checked = false;
        }

        private void notificationListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            notificationList.SelectedItem = null;
        }

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            NotificationViewModel model = e.Item as NotificationViewModel;

            Navigation.PushAsync(new ViewComparison(model.Notification.Opportunity, true));
        }

        private void OnSelectClicked(object sender, EventArgs e)
        {
            NotificationViewModel model = ((Button)sender).CommandParameter as NotificationViewModel;

            Navigation.PushAsync(new ViewComparison(model.Notification.Opportunity, true));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

//            loadNotifications();
        }

        private void deleteNotifications()
        {
            ObservableCollection<int> selectedNotifications = new ObservableCollection<int>(_notifications.Where(m => m.IsSelected).Select(m => m.Notification.Id).ToList());

            if (selectedNotifications.Count() != 0)
            {
                App.ShowLoading(true, @"Deleting");

                App app = Application.Current as App;
                SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

                client.deleteNotificationsCompleted += deleteNotificationsCompleted;
                client.deleteNotificationsAsync(
                    selectedNotifications,
                    app.LoginToken);
            }
            else
            {
                cancelEditNotifications();
            }
        }

        private void deleteNotificationsCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                {

                    for (int i = 0; i < _notifications.Count(); i++)
                    {
                        NotificationViewModel model = _notifications[i];

                        if (model.IsSelected)
                            _notifications.RemoveAt(i--);
                        else
                            model.IsEditing = false;
                    }

                    if (_notifications.Count > 0)
                        _notifications[_notifications.Count - 1].IsNotLast = false;

                    navBar.IsVisible = true;
//                    selectButtonSeparator.IsVisible = false;
                    selectButtonContainer.IsVisible = false;

                    ToolbarItems.Clear();
                    foreach (ToolbarItem i in _initialMenu)
                        ToolbarItems.Add(i);
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }

        private void cancelEditNotifications()
        {
            foreach (NotificationViewModel model in _notifications)
            {
                model.IsEditing = false;
                model.IsSelected = false;
            }

            navBar.IsVisible = true;
//            selectButtonSeparator.IsVisible = false;
            selectButtonContainer.IsVisible = false;

            ToolbarItems.Clear();
            foreach (ToolbarItem i in _initialMenu)
                ToolbarItems.Add(i);
        }

        private void OnSelectAllClicked(object sender, EventArgs e)
        {
            if (_notifications.Count() > 0)
            {
                if (selectButton.Checked)
                {
                    foreach (NotificationViewModel model in _notifications)
                        model.IsSelected = true;
                }
                else
                {
                    foreach (NotificationViewModel model in _notifications)
                        model.IsSelected = false;
                }
            }
        }

        private void OnSelectForDeleteClicked(object sender, EventArgs e)
        {
            int selectedCount = _notifications.Count(m => m.IsSelected);

            if (selectedCount == _notifications.Count)
                selectButton.Checked = true;
            else if (selectedCount == 0)
                selectButton.Checked = false;
        }
    }
}
