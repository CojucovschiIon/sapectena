﻿using System;
using System.Reflection;
using Xamarin.Forms;

namespace SCAMobile.Pages
{
    public partial class Settings : ContentPage
    {
        public Settings()
        {
            InitializeComponent();

            App app = Application.Current as App;

            firstName.Text = app.Login.FirstName;
            lastName.Text = app.Login.LastName;
            scaId.Text = app.Login.SCAId;
            NavigationPage.SetHasNavigationBar(this, false);

            Version ver = typeof(App).GetTypeInfo().Assembly.GetName().Version;
            appVersion.Text = string.Format(@"Version: {0}.{1}", ver.Major, ver.Minor);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (Device.OS == TargetPlatform.Windows)
            {
                NavigationPage.SetHasNavigationBar(this, true);
                NavigationPage.SetHasNavigationBar(this, false);
            }
        }

        private void OnResetClicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri(@"http://www.tenalookbook.com/lookbook_password.asp"));
        }

        private void OnSignOutClicked(object sender, EventArgs e)
        {
            App.ShowLoading(true, @"Signing Out");

            App app = Application.Current as App;
            SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

            client.logoutCompleted += logoutCompleted;
            client.logoutAsync(app.LoginToken);
        }

        private void logoutCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                {
                    App app = Application.Current as App;

                    app.Login = new SCAService.Login
                    {
                        Token = Guid.Empty
                    };
                    app.RecentAccounts.Clear();
                    app.RecentComparisons.Clear();
                    app.TabBar.Reset();

                    app.Properties.Clear();
                    app.SavePropertiesAsync();

                    app.DeviceToken = null;

                    App.resetNotifications();
                    App.notifyLogoutCompleted();

                    var navPage = new NavigationPage(new SCAMobile.Pages.MainPage());
                    app.MainPage = navPage;
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }

        private void OnHelpClicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri(@"http://www.tenalookbook.com/lookbook_help.asp"));
        }

        private void OnFaqClicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri(@"http://www.tenalookbook.com/lookbook_faq.asp"));
//            Navigation.PushAsync(new FAQ());
        }
    }
}
