﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using FFImageLoading.Forms;

namespace SCAMobile.Pages
{
    public partial class ViewProduct : ContentPage
    {
        private struct TabDescription
        {
            public string image;
            public string selectedImage;
            public ContentView view;
            public Button button;
            public Label label;
        }

        private int _currentTab;
        private List<TabDescription> _tabs = new List<TabDescription>();
        private bool _forNewComparision;
        private SCAService.Product _product;

        public string Tab { get; set; }

        public ViewProduct(SCAService.Product product, string tab, bool forNewComparision, bool showBottomTabBar = true)
        {
            Tab = tab;
            _forNewComparision = forNewComparision;
            _product = product;
            BindingContext = this;

            NavigationPage.SetBackButtonTitle(this, @"Back");

            InitializeComponent();

            Title = product.MarketDesc;

            if (!forNewComparision)
                actionButton.Text = @"Add Product";

            //navTabBar.IsVisible = showBottomTabBar;

            _tabs.Add(new TabDescription
            {
                image = @"Description-Large.png",
                selectedImage = @"Description-Large-Selected.png",
                view = this.productDescription,
                button = this.descriptionBtn,
                label = this.descriptionLbl
            });

            _tabs.Add(new TabDescription
            {
                image = @"Image-Large.png",
                selectedImage = @"Image-Large-Selected.png",
                view = this.productMedia,
                button = this.mediaBtn,
                label = this.mediaLbl
            });

            _tabs.Add(new TabDescription
            {
                image = @"Performance-Large.png",
                selectedImage = @"Performance-Large-Selected.png",
                view = this.productPerformance,
                button = this.performanceBtn,
                label = this.performanceLbl
            });

            _tabs.Add(new TabDescription
            {
                image = @"Selling Points-Large.png",
                selectedImage = @"SellingPoints-Large-Selected.png",
                view = this.productSellingPoints,
                button = this.sellingPointsBtn,
                label = this.sellingPointsLbl
            });

            this.scaCategory.Text = product.Category;
            this.scaArticle.Text = product.ArticleNumber;
            this.scaBagCount.Text = string.Format(@"{0}", product.BagCount);
            this.scaCaseCount.Text = string.Format(@"{0}", product.CaseCount);
            this.scaPiecesPerCase.Text = string.Format(@"{0}", product.PiecesPerCase);
            this.scaProductSize.Text = product.ProductSizeDesc;
            this.scaWaistSize.Text = product.WaistSize;
            this.scaProductName.Text = product.MarketDesc;
//            this.scaProductName2.Text = product.MarketDesc;
            this.scaProductName3.Text = product.MarketDesc;
            this.scaProductName4.Text = product.MarketDesc;

            if (product.Benefits.Count > 0)
            {
                this.scaSellingPoints.ItemsSource = 
                    new ObservableCollection<SellingPointViewModel>(product.Benefits.Select(m => new SellingPointViewModel
                    {
                        Name = m.BenefitName,
                        Description = m.LongBenefit,
                        IsNameVisible = true
                    }));
            }
            else
            {
                this.scaSellingPoints.ItemsSource = new ObservableCollection<SellingPointViewModel>
                {
                    new SellingPointViewModel
                    {
                        Description = product.ProductDesc
                    }
                };
            }
            this.productImage.Source = App.getProductImage(product);

            loadImages(product, scaImageList, scaNoImagesLabel);
            loadVideos(product, scaVideoList, scaNoVideosLabel);
            loadPerformance(product);
        }

        private void OnDescriptionClicked(object sender, EventArgs e)
        {
            if (_currentTab != 0)
                showTab(0);
        }

        private void OnMediaClicked(object sender, EventArgs e)
        {
            if (_currentTab != 1)
                showTab(1);
        }

        private void OnPerformanceClicked(object sender, EventArgs e)
        {
            if (_currentTab != 2)
                showTab(2);
        }

        private void OnSellingPointsClicked(object sender, EventArgs e)
        {
            if (_currentTab != 3)
                showTab(3);
        }

        private void showTab(int tabIndex)
        {
            _currentTab = tabIndex;

            for (int i = 0; i < _tabs.Count(); i++)
            {
                TabDescription tab = _tabs[i];

                if (i == tabIndex)
                {
                    if (!tab.view.IsVisible)
                    {
                        tab.view.IsVisible = true;
                        tab.button.Image = tab.selectedImage;
                        tab.label.TextColor = Color.FromHex(@"85ba35");
                    }
                }
                else
                {
                    if (tab.view.IsVisible)
                    {
                        tab.view.IsVisible = false;
                        tab.button.Image = tab.image;
                        tab.label.TextColor = Color.FromHex(@"a2a2a0");
                    }
                }
            }
        }

        private void OnActionClicked(object sender, EventArgs e)
        {
            if (_forNewComparision)
            {
                Navigation.PushAsync(new CompareProducts(_product, new List<object> { 1 }, Tab));
            }
            else
            {
                do
                {
                    Page p = Navigation.NavigationStack[Navigation.NavigationStack.Count() - 2];

                    if (p is CompareProducts)
                    {
                        CompareProducts comparePage = (CompareProducts)p;
                        comparePage.AddProduct(_product);

                        break;
                    }
                    else if (p is EditComparison)
                    {
                        EditComparison comparePage = (EditComparison)p;
                        comparePage.AddProduct(_product);

                        break;
                    }
                    else
                    {
                        Navigation.RemovePage(p);
                    }
                }
                while (true);

                Navigation.PopAsync(true);
            }
        }

        private void loadImages(SCAService.Product product, Grid grid, Label noImagesLabel)
        {
            var productImages = product.Images.Where(m => m.Type != @"VD").OrderBy(m => m.orderby);

            if (productImages.Count() > 0)
            {
                int cols = 8;
                int rows = productImages.Count() + (cols - 1) / cols;
                int col = 0, row = 0;

                grid.IsVisible = true;
                noImagesLabel.IsVisible = false;

                foreach (SCAService.ProductImage image in productImages)
                {
                    CachedImage cachedImage = new CachedImage
                    {
                        Source = image.ImageUrl,
                        HeightRequest = 80,
                        WidthRequest = 80,
                        HorizontalOptions = LayoutOptions.Center,
                        Aspect = Aspect.AspectFit
                    };

                    TapGestureRecognizer gesture = new TapGestureRecognizer();
                    gesture.Tapped += OnImageTapped;
                    cachedImage.GestureRecognizers.Add(gesture);

                    grid.Children.Add(cachedImage, col, row);

                    if (++col == cols)
                    {
                        row++;
                        col = 0;
                    }
                }
            }
            else
            {
                grid.IsVisible = false;
                noImagesLabel.IsVisible = true;
            }
        }

        private void loadVideos(SCAService.Product product, Grid grid, Label noVideosLabel)
        {
            App app = Application.Current as App;
            var productVideos = product.Images.Where(m => m.Type == @"VD").OrderBy(m => m.orderby);

            if (productVideos.Count() > 0)
            {
                int cols = 4;
                int rows = productVideos.Count() + (cols - 1) / cols;
                int col = 0, row = 0;

                grid.IsVisible = true;
                noVideosLabel.IsVisible = false;

                foreach (SCAService.ProductImage i in productVideos)
                {
                    string thumbNail = @"YouTube.png";
                    string videoId = App.getYouTubeVideoId(i.ImageUrl);

                    if (!string.IsNullOrWhiteSpace(videoId))
                        thumbNail = string.Format(@"https://img.youtube.com/vi/{0}/0.jpg", videoId);

                    CachedImage image = new CachedImage
                    {
                        Source = thumbNail,
                        HeightRequest = 80,
                        WidthRequest = 80,
                        HorizontalOptions = LayoutOptions.Center,
                        Aspect = Aspect.AspectFit,
                        LoadingPlaceholder = app.Login.SCACompanyId == product.CompanyId ? @"tena-product.png" : @"product.png"
                    };

                    TapGestureRecognizer gesture = new TapGestureRecognizer();
                    gesture.Tapped += OnVideoTapped;
                    image.GestureRecognizers.Add(gesture);

                    grid.Children.Add(image, col, row);

                    if (++col == cols)
                    {
                        row++;
                        col = 0;
                    }
                }
            }
            else
            {
                grid.IsVisible = false;
                noVideosLabel.IsVisible = true;
            }
        }

        private void OnImageTapped(object sender, EventArgs e)
        {
            CachedImage image = sender as CachedImage;
            List<string> productImages = new List<string>();
            int imageIndex;

            imageIndex = scaImageList.Children.IndexOf(image);

            foreach (SCAService.ProductImage i in _product.Images.Where(m => m.Type != @"VD").OrderBy(m => m.orderby))
                productImages.Add(i.ImageUrl);

            if (image.Source is UriImageSource)
                Navigation.PushAsync(new ImageViewer(productImages, imageIndex, Tab, _product.MarketDesc + " images", false));
        }

        private void OnVideoTapped(object sender, EventArgs e)
        {
            CachedImage image = sender as CachedImage;
            var productVideos = _product.Images.Where(m => m.Type == @"VD").OrderBy(m => m.orderby).ToList();
            int videoIndex;

            videoIndex = scaVideoList.Children.IndexOf(image);

            Navigation.PushAsync(new VideoViewer(_product, productVideos[videoIndex].ImageUrl, Tab, false));
//            Device.OpenUri(new Uri(productVideos[videoIndex].ImageUrl));
        }

        private void loadPerformance(SCAService.Product product)
        {
            ObservableCollection<PerformanceMetricViewModel> performance = new ObservableCollection<PerformanceMetricViewModel>();
            string value;

            value = product.Absorption == null ? @"" : product.Absorption.ToString();
            addPerformanceMetric(@"Rothwell", ref value, performance);

            TypeInfo productTypeInfo = typeof(SCAService.Product).GetTypeInfo();

            foreach (PerformanceMetric pm in Globals.PerformanceMetrics)
            {
                PropertyInfo propInfo = productTypeInfo.GetDeclaredProperty(pm.PropName);

                if (propInfo != null)
                {
                    value = propInfo.GetValue(product) as string;
                    addPerformanceMetric(pm.DisplayName, ref value, performance);
                }
            }

            this.productPerformanceList.ItemsSource = performance;
            this.productPerformanceList.Header = new ProductNameViewModel
            {
                ScaName = product.MarketDesc
            };
        }

        private void addPerformanceMetric(string name, ref string value, ObservableCollection<PerformanceMetricViewModel> performanceList)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                performanceList.Add(new PerformanceMetricViewModel
                {
                    Name = name,
                    ScaValue = value,
                });
            }
        }

        /*
                private void loadPerformance(SCAService.Product product)
                {
                    string value;

                    while (productPerformanceList.RowDefinitions.Count() > 1)
                    {
                        productPerformanceList.Children.RemoveAt(1);
                        productPerformanceList.Children.RemoveAt(1);
                        productPerformanceList.RowDefinitions.RemoveAt(1);
                    }

                    value = product.Absorption == null ? @"" : product.Absorption.ToString();
                    addPerformanceMetric(@"Rothwell", ref value);

                    TypeInfo productTypeInfo = typeof(SCAService.Product).GetTypeInfo();

                    foreach (PerformanceMetric pm in Globals.PerformanceMetrics)
                    {
                        PropertyInfo propInfo = productTypeInfo.GetDeclaredProperty(pm.PropName);

                        if (propInfo != null)
                        {
                            value = propInfo.GetValue(product) as string;
                            addPerformanceMetric(pm.DisplayName, ref value);
                        }
                    }

        //            if (productPerformanceList.RowDefinitions.Count == 1)
                    {
                        int row = productPerformanceList.RowDefinitions.Count();

                        productPerformanceList.RowDefinitions.Add(new RowDefinition
                        {
                            Height = GridLength.Star
                        });

                        productPerformanceList.Children.Add(new ContentView
                        {
                            Content = new Label
                            {
                                HorizontalTextAlignment = TextAlignment.Center,
                                TextColor = Color.FromHex(@"006aa9")
                            },
                            Padding = 0,
                            BackgroundColor = Color.FromHex(@"f9f6f2")
                        }, 0, row);

                        productPerformanceList.Children.Add(new ContentView
                        {
                            Content = new Label
                            {
                                HorizontalTextAlignment = TextAlignment.Center
                            },
                            Padding = 10,
                            BackgroundColor = Color.FromHex(@"f9f6f2")
                        }, 1, row);
                    }
                }

                private void addPerformanceMetric(string name, ref string value)
                {
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        int row = productPerformanceList.RowDefinitions.Count();

                        productPerformanceList.RowDefinitions.Add(new RowDefinition
                        {
                            Height = GridLength.Auto
                        });

                        productPerformanceList.Children.Add(new ContentView
                        {
                            Content = new Label
                            {
                                Text = name,
                                HorizontalTextAlignment = TextAlignment.Center,
                                VerticalOptions = LayoutOptions.CenterAndExpand,
                                LineBreakMode = LineBreakMode.WordWrap,
                                TextColor = Color.FromHex(@"006aa9")
                            },
                            Padding = 10,
                            BackgroundColor = Color.FromHex(@"f9f6f2")
                        }, 0, row);

                        productPerformanceList.Children.Add(new ContentView
                        {
                            Content = new Label
                            {
                                Text = value,
                                HorizontalTextAlignment = TextAlignment.Center,
                                VerticalOptions = LayoutOptions.CenterAndExpand,
                                LineBreakMode = LineBreakMode.WordWrap,
                            },
                            Padding = 10,
                            BackgroundColor = Color.FromHex(@"f9f6f2")
                        }, 1, row);
                    }
                }
        */
    }
}
