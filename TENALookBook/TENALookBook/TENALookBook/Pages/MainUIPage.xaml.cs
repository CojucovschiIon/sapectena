﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace SCAMobile.Pages
{
    public partial class MainUIPage : ContentPage
    {
        public MainUIPage()
        {
            InitializeComponent();

            ContentPage p = new SCAMobile.Pages.ConfidentialStatement();

            Grid grid = this.Content as Grid;

            grid.Children.Add(p.Content);

        }
    }
}
