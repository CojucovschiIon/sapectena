﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using FormsToolkit;
using SCAMobile.Constants;
using SCAMobile.Messaging;
using System.Collections.ObjectModel;

namespace SCAMobile.Pages
{
    public class AccountViewModel
    {
        public SCAService.Account Account { get; set; }

        public bool IsNotLast { get; set; }
    }

    public partial class AccountSearchResults : ContentPage
    {
        private ObservableCollection<AccountViewModel> _accounts;

        public ObservableCollection<AccountViewModel> Accounts
        {
            get { return _accounts; }
        }
        public AccountSearchResults(IList<SCAService.Account> accounts)
        {
            _accounts = new ObservableCollection<AccountViewModel>(accounts.Select(m => new AccountViewModel
            {
                Account = m,
                IsNotLast = true
            }));

            if (_accounts.Count > 0)
                _accounts[_accounts.Count - 1].IsNotLast = false;

                BindingContext = this;

            InitializeComponent();

            MessagingService.Current.Subscribe<MessagingServiceComparisonDeleted>(MessageKeys.ComparisonDeleted, (service, msg) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    var accountsViewModels = _accounts.Where(m => m.Account.Opportunities.Where(c => c.Id == msg.Id).Count() > 0);

                    foreach (AccountViewModel acc in accountsViewModels)
                    {
                        SCAService.Opportunity comp = acc.Account.Opportunities.Where(m => m.Id == msg.Id).FirstOrDefault();
                        if (comp != null)
                            acc.Account.Opportunities.Remove(comp);
                    }
                });
            });

            MessagingService.Current.Subscribe<MessagingServiceComparisonAdded>(MessageKeys.ComparisonAdded, (service, msg) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    AccountViewModel acc = _accounts.Where(m => m.Account.Id == msg.Comparison.AccountId).FirstOrDefault();

                    if (acc != null)
                    {
                        SCAService.Opportunity comparison = acc.Account.Opportunities.Where(m => m.Id == msg.Comparison.Id).FirstOrDefault();

                        if (comparison == null)
                            acc.Account.Opportunities.Add(msg.Comparison);
                    }
                });
            });
        }

        private void accountListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            accountList.SelectedItem = null;
        }

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            App app = Application.Current as App;
            AccountViewModel accountViewModel = e.Item as AccountViewModel;

            app.AddAccountToRecentList(accountViewModel.Account.Id);
            Navigation.PushAsync(new SCAMobile.Pages.AccountSummary(accountViewModel.Account));
        }

        private void OnSelectClicked(object sender, EventArgs e)
        {
            App app = Application.Current as App;
            Button btn = sender as Button;
            SCAService.Account account = btn.CommandParameter as SCAService.Account;

            app.AddAccountToRecentList(account.Id);
            Navigation.PushAsync(new SCAMobile.Pages.AccountSummary(account));
        }

        private void OnRecentlyAccessedAccountsClicked(object sender, EventArgs e)
        {
            App app = Application.Current as App;

            if (app.RecentAccounts.Count > 0)
            {
                App.ShowLoading(true, @"Loading");

                SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();
                client.getAccountsByIdCompleted += getAccountsByIdCompleted;
                client.getAccountsByIdAsync(new ObservableCollection<int>(app.RecentAccounts), app.LoginToken);
            }
            else
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = @"Information",
                    Cancel = @"OK",
                    Message = @"You have not accessed any account yet"
                });
            }
        }
        private void getAccountsByIdCompleted(object sender, SCAService.getAccountsByIdCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                    Navigation.PushAsync(new RecentAccounts(e.Result));
                else
                    App.scaServiceUnavailableErrorMessage(e.Error);
            });
        }
    }
}
