﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using FormsToolkit;
using SCAMobile.Constants;
using SCAMobile.Messaging;
using System.Collections.ObjectModel;

namespace SCAMobile.Pages
{
    public partial class SaveComparision : ContentPage
    {
        public class AccountViewModel : BindableObject
        {
            public static readonly BindableProperty FrameColorProperty =
                BindableProperty.Create("FrameColor", typeof(string), typeof(AccountViewModel), null);
            public static readonly BindableProperty IsNotLastProperty =
                BindableProperty.Create("IsNotLast", typeof(bool), typeof(AccountViewModel), false, BindingMode.TwoWay);

            public SCAService.Account Account { get; set; }

            public string FrameColor
            {
                get
                {
                    return (string)GetValue(FrameColorProperty);
                }

                set
                {
                    SetValue(FrameColorProperty, value);
                }
            }

            public bool IsNotLast
            {
                get
                {
                    return (bool)GetValue(IsNotLastProperty);
                }

                set
                {
                    SetValue(IsNotLastProperty, value);
                }
            }
        }

        private SCAService.Account _account = null;
        private SCAService.Product _compProduct;
        private SCAService.Product _tenaProduct;

        public SaveComparision(SCAService.Product compProduct, SCAService.Product tenaProduct)
        {
            InitializeComponent();

            _compProduct = compProduct;
            _tenaProduct = tenaProduct;
            challenge.Text = compProduct.MarketDesc + @" vs " + tenaProduct.MarketDesc;
        }

        public SaveComparision(SCAService.Opportunity comparison)
        {
            InitializeComponent();

            _compProduct = comparison.CompProduct;
            _tenaProduct = comparison.TenaProducts[0];
            challenge.Text = _compProduct.MarketDesc + @" vs " + _tenaProduct.MarketDesc;
            notes.Text = comparison.Notes;
        }

        private void OnSearchClicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.searchTerm.Text))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = @"Error",
                    Cancel = @"OK",
                    Message = @"Please enter Company Name, City or Account Number"
                });
            }
            else
            {
                App.ShowLoading(true, @"Searching");

                App app = Application.Current as App;
                SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

                _account = null;
                client.searchAccountsCompleted += searchAccountsCompleted;
                client.searchAccountsAsync(this.searchTerm.Text, app.LoginToken);
            }
        }

        private void searchAccountsCompleted(object sender, SCAService.searchAccountsCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                {

                    if (e.Result.Count() == 0)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                        {
                            Title = @"Information",
                            Cancel = @"OK",
                            Message = @"No accounts found"
                        });
                    }
                    else
                    {
                        ObservableCollection<AccountViewModel> accounts = new ObservableCollection<AccountViewModel>();

                        foreach (SCAService.Account a in e.Result)
                        {
                            AccountViewModel model = new AccountViewModel
                            {
                                Account = a,
                                FrameColor = @"#f9f7f4",
                                IsNotLast = true
                            };

                            accounts.Add(model);
                        }

                        if (accounts.Count > 0)
                            accounts.Last().IsNotLast = false;

                        accountList.ItemsSource = accounts;
                    }
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }

        private void OnFinishClicked(object sender, EventArgs e)
        {
            if (_account != null)
            {
                App.ShowLoading(true, @"Saving");

                App app = Application.Current as App;
                SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

                client.addOpportunityCompleted += addOpportunityCompleted;
                client.addOpportunityAsync(new ObservableCollection<int> { _tenaProduct.Id }, _compProduct.Id, _account.Id, notes.Text, app.LoginToken);
            }
            else
            {
                MessagingService.Current.SendMessage(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = @"Error",
                    Cancel = @"OK",
                    Message = @"You need to attach a comparison to an account to be saved"
                });
            }
        }

        private void addOpportunityCompleted(object sender, SCAService.addOpportunityCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                {
                    App app = Application.Current as App;

                    app.AddComparisonToRecentList(e.Result.Id);

                    MessagingService.Current.SendMessage(MessageKeys.ComparisonAdded, new MessagingServiceComparisonAdded { Comparison = e.Result });

                    Navigation.PopToRootAsync();
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }

        private void accountListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            accountList.SelectedItem = null;
        }

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            AccountViewModel model = e.Item as AccountViewModel;

            _account = model.Account;

            foreach (AccountViewModel am in accountList.ItemsSource)
            {
                if (am != model)
                    am.FrameColor = @"#f9f7f4";
            }
            model.FrameColor = @"#85ba35";
        }

        private void OnSelectClicked(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            AccountViewModel model = btn.CommandParameter as AccountViewModel;

            _account = model.Account;

            foreach (AccountViewModel am in accountList.ItemsSource)
            {
                if (am != model)
                    am.FrameColor = @"#f9f7f4";
            }
            model.FrameColor = @"#85ba35";
        }
    }
}
