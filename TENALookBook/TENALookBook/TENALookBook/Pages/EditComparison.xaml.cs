﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using FormsToolkit;
using SCAMobile.Constants;
using SCAMobile.Messaging;

namespace SCAMobile.Pages
{
    public partial class EditComparison : ContentPage
    {
        private List<object> _products;
        private SCAService.Opportunity _comparison;
        SCAService.Product _scaProduct;
        private Command _deleteCommand;

        public EditComparison(SCAService.Opportunity comparison)
        {
            BindingContext = this;

            _comparison = comparison;
            _scaProduct = comparison.TenaProducts[0];

            _products = new List<object>();
            _products.Add(new ProductViewModel { Product = _scaProduct, ProductImage = App.getProductImage(_scaProduct) });

            InitializeComponent();

            notes.Text = comparison.Notes;
            compProductImage.Source = App.getProductImage(comparison.CompProduct);
        }

        public List<object> AlternativeProducts
        {
            get { return _products; }

            set
            {
                _products = value;
            }
        }

        public Command DeleteCommand
        {
            get
            {
                return _deleteCommand ?? (_deleteCommand = new Command(deleteComparison));
            }
        }

        public void AddProduct(SCAService.Product product)
        {
            scaAlternativeCarousel.ItemsSource = new List<object> { new ProductViewModel { Product = product, ProductImage = App.getProductImage(product) } };
            _scaProduct = product;
        }

        private void OnPositionSelected(object sender, EventArgs e)
        {
            if (scaAlternativeCarousel.Position < _products.Count() - 1)
            {
                _scaProduct = ((ProductViewModel)_products[scaAlternativeCarousel.Position]).Product;
            }
            else
            {
                _scaProduct = null;
            }
        }


        private void OnSaveClicked(object sender, EventArgs e)
        {
            if (_scaProduct != null)
            {
                App.ShowLoading(true, @"Saving");

                App app = Application.Current as App;
                SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

                client.saveOpportunityCompleted += saveOpportunityCompleted;
                client.saveOpportunityAsync(_comparison.Id, new ObservableCollection<int> { _scaProduct.Id }, notes.Text, app.LoginToken);

                _comparison.Notes = notes.Text;
                _comparison.TenaProducts = new ObservableCollection<SCAService.Product> { _scaProduct };
            }
            else
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = @"Error",
                    Cancel = @"OK",
                    Message = @"You need to attach an SCA Product to this comparison."
                });
            }
        }

        private void saveOpportunityCompleted(object sender, SCAService.saveOpportunityCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                {
                    if (e.Result != null)
                    {
                        App app = Application.Current as App;

                        app.AddComparisonToRecentList(e.Result.Id);

                        MessagingService.Current.SendMessage<MessagingServiceComparisonUpdated>(MessageKeys.ComparisonUpdated, new MessagingServiceComparisonUpdated { Id = _comparison.Id });

                        Navigation.PopAsync();
                    }
                    else
                    {
                        App.comparisionDeletedErrorMessage();
                    }
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }

            });
        }

        private void OnDeleteClicked(object sender, EventArgs e)
        {
            scaAlternativeCarousel.ItemsSource = new List<object> { 1 };
            _scaProduct = null;
        }

        private void OnAddClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Catalogue(@"Accounts", true, false));
        }

        private void deleteComparison()
        {
            App.ShowLoading(true, @"Deleting");

            App app = Application.Current as App;
            SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

            client.deleteOpportunityCompleted += deleteOpportunityCompleted;
            client.deleteOpportunityAsync(_comparison.Id, app.LoginToken);
        }

        private void deleteOpportunityCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                {
                    App app = Application.Current as App;

                    app.RemoveComparisonFromRecentList(_comparison.Id);

                    MessagingService.Current.SendMessage<MessagingServiceComparisonDeleted>(MessageKeys.ComparisonDeleted, new MessagingServiceComparisonDeleted { Id = _comparison.Id });

                    Navigation.RemovePage(Navigation.NavigationStack[Navigation.NavigationStack.Count() - 2]);
                    Navigation.PopAsync();
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }
    }
}
