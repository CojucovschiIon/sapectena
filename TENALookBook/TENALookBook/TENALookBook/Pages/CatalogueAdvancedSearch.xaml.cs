﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using SCAMobile.Controls;
using FormsToolkit;
using SCAMobile.Constants;
using System.Collections.ObjectModel;

namespace SCAMobile.Pages
{
    public partial class CatalogueAdvancedSearch : ContentPage
    {
        public static int ABSORPTION_MIN = 0;
        public static int ABSORPTION_MAX = 4000;

        private int _rowCount;
        private int _colCount = 1;
        private Command _resetCommand;

        public Command ResetCommand
        {
            get
            {
                return _resetCommand ?? (_resetCommand = new Command(resetForm));
            }
        }

        public string Tab { get; set; }

        public CatalogueAdvancedSearch(string tab, bool showBottomTabBar = true)
        {
            Tab = tab;
            BindingContext = this;

            InitializeComponent();

            navTabBar.IsVisible = showBottomTabBar;
            Title = @"Search Product Catalogue";

            RangeSlider.MinimumValue = ABSORPTION_MIN;
            RangeSlider.MaximumValue = ABSORPTION_MAX;
            RangeSlider.LowerValue = ABSORPTION_MIN;
            RangeSlider.UpperValue = ABSORPTION_MAX;

            loadCategories();
        }

        private void loadCategories()
        {
            App app = Application.Current as App;
            SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

            client.getAllCategoriesCompleted += getAllCategoriesCompleted;
            client.getAllCategoriesAsync(app.LoginToken);
        }

        private void getAllCategoriesCompleted(object sender, SCAService.getAllCategoriesCompletedEventArgs e)
        {

            Device.BeginInvokeOnMainThread(() =>
            {
                if (e.Error == null)
                {
                    _rowCount = e.Result.Count();

                    foreach (SCAService.Category cat in e.Result)
                    {
                        if (cat.Children.Count() > _colCount)
                            _colCount = cat.Children.Count();
                    }

                    for (int i = 0; i < _rowCount; i++)
                    {
                        RowDefinition rowDef = new RowDefinition
                        {
                            Height = GridLength.Auto
                        };

                        this.categories.RowDefinitions.Add(rowDef);
                    }

                    for (int i = 0; i < _colCount; i++)
                    {
                        ColumnDefinition colDef = new ColumnDefinition
                        {
                            Width = GridLength.Auto
                        };

                        this.categories.ColumnDefinitions.Add(colDef);
                    }

                    for (int i = 0; i < _rowCount; i++)
                    {
                        SCAService.Category cat = e.Result.ElementAt(i);
                        Image img = new Image
                        {
                            Source = cat.Logo,
                            VerticalOptions = LayoutOptions.Center,
                            HorizontalOptions = LayoutOptions.Center
                        };
                        this.categories.Children.Add(img, 0, i);
                        /*
                                            FFImageLoading.Forms.CachedImage image = new CachedImage
                                            {
                                                Source = cat.Logo,
                                                WidthRequest = 100
                                            };

                                            this.categories.Children.Add(image, 0, i);
                        */

                        for (int j = 0; j < cat.Children.Count(); j++)
                        {
                            SCAService.Category subCat = cat.Children.ElementAt(j);
                            CategoryButton categoryButton = new CategoryButton
                            {
                                CategoryId = subCat.Id,
                                Title = subCat.Name,
                                HorizontalOptions = LayoutOptions.Start
                            };

                            this.categories.Children.Add(categoryButton, j + 1, i);
                        }
                    }
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }

        private void resetForm()
        {
            this.searchTerm.Text = @"";
            this.sizeXS.Checked = false;
            this.sizeS.Checked = false;
            this.sizeM.Checked = false;
            this.sizeR.Checked = false;
            this.sizeL.Checked = false;
            this.sizeXL.Checked = false;
            this.sizeBari.Checked = false;
            this.RangeSlider.LowerValue = ABSORPTION_MIN;
            this.RangeSlider.UpperValue = ABSORPTION_MAX;

            foreach (var child in this.categories.Children)
            {
                if (child is CategoryButton)
                    ((CategoryButton)child).Checked = false;
            }
        }

        private void absorptionMinChanged(object sender, TextChangedEventArgs e)
        {
            float absorption;

            if (float.TryParse(this.absorptionMin.Text, out absorption))
            {
                if (absorption >= ABSORPTION_MIN && absorption <= ABSORPTION_MAX && absorption != this.RangeSlider.LowerValue)
                    this.RangeSlider.LowerValue = absorption;
            }
        }

        private void absorptionMaxChanged(object sender, TextChangedEventArgs e)
        {
            float absorption;

            if (float.TryParse(this.absorptionMax.Text, out absorption))
            {
                if (absorption >= ABSORPTION_MIN && absorption <= ABSORPTION_MAX && absorption != this.RangeSlider.UpperValue)
                    this.RangeSlider.UpperValue = absorption;
            }
        }

        private void absorptionSliderDragCompleted(object sender, EventArgs e)
        {
            this.absorptionMin.Text = RangeSlider.LowerValue.ToString();
            this.absorptionMax.Text = RangeSlider.UpperValue.ToString();
        }

        private void OnSearchClicked(object sender, EventArgs e)
        {
            App app = Application.Current as App;
            SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();
            ObservableCollection<int> categories = new ObservableCollection<int>();
            ObservableCollection<string> sizes = new ObservableCollection<string>();

            foreach (var child in this.categories.Children)
            {
                if (child is CategoryButton && ((CategoryButton)child).Checked)
                    categories.Add(((CategoryButton)child).CategoryId);
            }

            if (this.sizeXS.Checked)
                sizes.Add(@"XS");
            if (this.sizeS.Checked)
                sizes.Add(@"S");
            if (this.sizeM.Checked)
                sizes.Add(@"M");
            if (this.sizeR.Checked)
                sizes.Add(@"R");
            if (this.sizeL.Checked)
                sizes.Add(@"L");
            if (this.sizeXL.Checked)
                sizes.Add(@"XL");
            if (this.sizeBari.Checked)
                sizes.Add(@"BARI");

            App.ShowLoading(true, @"Searching");

            client.searchProductsCompleted += searchProductsCompleted;
            client.searchProductsAsync(
                this.searchTerm.Text, null, categories,
                (decimal)this.RangeSlider.LowerValue, (decimal)this.RangeSlider.UpperValue,
                sizes, true, app.LoginToken);
        }

        private void searchProductsCompleted(object sender, SCAService.searchProductsCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                {
                    if (e.Result.Count() == 0)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                        {
                            Title = @"Information",
                            Cancel = @"OK",
                            Message = @"No products found"
                        });
                    }
                    else
                    {
                        Navigation.PushAsync(new SCAMobile.Pages.CatalogueSearchResults(e.Result, Tab, navTabBar.IsVisible));
                    }
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }
    }
}
