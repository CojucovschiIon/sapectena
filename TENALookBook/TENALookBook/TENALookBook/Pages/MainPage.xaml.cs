﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using FormsToolkit;
using SCAMobile.Constants;

namespace SCAMobile.Pages
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            try
            {
                if (Application.Current.Properties.ContainsKey(ConfigKeys.LastLoginDate))
                {
                    DateTime? lastLoginDate = Application.Current.Properties[ConfigKeys.LastLoginDate] as DateTime?;

                    if (lastLoginDate != null && lastLoginDate.Value.AddMonths(1) > DateTime.Now)
                    {
                        Guid loginToken = (Guid)Application.Current.Properties[ConfigKeys.LoginToken];

                        App.ShowLoading(true, @"Verifying login");

                        SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

                        client.checkLoginCompleted += checkLoginCompleted;
                        client.checkLoginAsync(loginToken);

                        return;
                    }
                }

                Application.Current.Properties.Clear();
            }
            catch
            {
                try
                {
                    Application.Current.Properties.Clear();
                }
                catch
                {}
            }
        }

        private void checkLoginCompleted(object sender, SCAService.checkLoginCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                {
                    App app = Application.Current as App;

                    if (e.Result.Token != Guid.Empty)
                    {
                        app.Login = e.Result;
                        app.MainPage = app.TabBar.getPage(MainUITabBar.PAGE_INDEX_SEARCH);
                        app.Properties[ConfigKeys.LastLoginDate] = new DateTime?(DateTime.Now);
                        app.SavePropertiesAsync();
                        App.notifyLoginCompleted();
                    }
                    else
                    {
                        try
                        {
                            app.Properties.Clear();
                            app.DeviceToken = null;
                        }
                        catch
                        { }
                    }
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }

        void OnLoginClicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.email.Text) || string.IsNullOrWhiteSpace(this.password.Text))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = @"Error",
                    Cancel = @"OK",
                    Message = @"Please enter Email Address and Password"
                });
            }
            else
            {
                App.ShowLoading(true, @"Signing In");

                SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

                client.loginCompleted += loginCompleted;
                client.loginAsync(this.email.Text, this.password.Text);
            }
        }

        private void loginCompleted(object sender, SCAService.loginCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);
            });

            if (e.Error == null)
            {

                if (e.Result.Token.Equals(Guid.Empty))
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                    {
                        Title = @"Error",
                        Cancel = @"OK",
                        Message = @"Invalid Email Address or Password"
                    });
                }
                else
                {
                    App app = Application.Current as App;
                    app.Login = e.Result;

                    app.Properties.Clear();

                    app.Properties[ConfigKeys.LoginToken] = e.Result.Token;
                    app.Properties[ConfigKeys.LastLoginDate] = new DateTime?(DateTime.Now);
                    app.SavePropertiesAsync();
                    App.notifyLoginCompleted();

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Navigation.PushAsync(new SCAMobile.Pages.ConfidentialStatement());
                    });
                }
            }
            else
            {
                App.scaServiceUnavailableErrorMessage(e.Error);
            }
        }

        private void OnForgotPasswordClicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri(@"http://www.tenalookbook.com/forgot_password.asp"));
        }
    }
}
