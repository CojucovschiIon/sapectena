﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using FormsToolkit;
using SCAMobile.Controls;
using SCAMobile.Constants;
using SCAMobile.Messaging;

namespace SCAMobile.Pages
{
    public partial class AccountSummary : ContentPage
    {
        private class ComparisonViewModel : BindableObject
        {
            public static readonly BindableProperty IsNotLastProperty =
                BindableProperty.Create("IsNotLast", typeof(bool), typeof(ComparisonViewModel), false, BindingMode.TwoWay);

            public SCAService.Opportunity Comparison { get; set; }

            public string CompProductImage { get; set; }

            public string SCAProductImage { get; set; }

            public bool IsNotLast
            {
                get
                {
                    return (bool)GetValue(IsNotLastProperty);
                }

                set
                {
                    SetValue(IsNotLastProperty, value);
                }
            }
        }

        private List<int> _selectedCategories = new List<int>();
        private bool _allCategories = true;
        private ObservableCollection<ComparisonViewModel> _allComparisons;

        public AccountSummary(SCAService.Account account)
        {
            BindingContext = account;

            InitializeComponent();

            _allComparisons = new ObservableCollection<ComparisonViewModel>(
                account.Opportunities.Select(m => new ComparisonViewModel
                {
                    Comparison = m,
                    CompProductImage = App.getProductImage(m.CompProduct),
                    SCAProductImage = App.getProductImage(m.TenaProducts[0]),
                    IsNotLast = true
                })
            );

            loadComparisons();

            MessagingService.Current.Subscribe<MessagingServiceComparisonDeleted>(MessageKeys.ComparisonDeleted, (service, msg) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ObservableCollection<ComparisonViewModel> comparisons = comparisonList.ItemsSource as ObservableCollection<ComparisonViewModel>;
                    ComparisonViewModel compViewModel = comparisons.Where(m => m.Comparison.Id == msg.Id).FirstOrDefault();

                    if (compViewModel != null)
                    {
                        comparisons.Remove(compViewModel);
                        if (comparisons.Count > 0)
                            comparisons.Last().IsNotLast = false;

                        SCAService.Account acc = BindingContext as SCAService.Account;
                        SCAService.Opportunity comp = acc.Opportunities.Where(m => m.Id == msg.Id).FirstOrDefault();
                        if (comp != null)
                            acc.Opportunities.Remove(comp);
                    }
                });
            });

            MessagingService.Current.Subscribe<MessagingServiceComparisonAdded>(MessageKeys.ComparisonAdded, (service, msg) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    SCAService.Account acc = BindingContext as SCAService.Account;

                    if (acc.Id == msg.Comparison.AccountId)
                    {
                        if (acc.Opportunities.Where(m => m.Id == msg.Comparison.Id).FirstOrDefault() == null)
                            acc.Opportunities.Add(msg.Comparison);

//                        ObservableCollection<ComparisonViewModel> comparisons = comparisonList.ItemsSource as ObservableCollection<ComparisonViewModel>;

                        _allComparisons.Add(new ComparisonViewModel
                        {
                            Comparison = msg.Comparison,
                            CompProductImage = App.getProductImage(msg.Comparison.CompProduct),
                            SCAProductImage = App.getProductImage(msg.Comparison.TenaProducts[0]),
                            IsNotLast = false
                        });

                        loadComparisons();
                    }
                });
            });
        }

        protected override bool OnBackButtonPressed()
        {
            MessagingService.Current.Unsubscribe<MessagingServiceComparisonDeleted>(MessageKeys.ComparisonDeleted);

            return base.OnBackButtonPressed();

        }

        private void loadComparisons()
        {
            SCAService.Account account = BindingContext as SCAService.Account;
            ObservableCollection<ComparisonViewModel> selectedComparisons;

            if (_allCategories)
            {
                comparisonList.ItemsSource = _allComparisons;
                selectedComparisons = _allComparisons;
            }
            else
            {
                selectedComparisons = new ObservableCollection<ComparisonViewModel>();

                foreach (ComparisonViewModel comp in _allComparisons)
                {
                    bool matches = false;

                    foreach (SCAService.Category cat in comp.Comparison.CompProduct.Categories)
                    {
                        var q = cat.Path.Join(_selectedCategories, pc => pc, catId => catId, (pc, catId) => pc);

                        if (q.Count() > 0)
                        {
                            matches = true;
                            selectedComparisons.Add(comp);
                            break;
                        }
                    }

                    if (!matches)
                    {
                        foreach (SCAService.Category cat in comp.Comparison.TenaProducts[0].Categories)
                        {
                            var q = cat.Path.Join(_selectedCategories, pc => pc, catId => catId, (pc, catId) => pc);

                            if (q.Count() > 0)
                            {
                                selectedComparisons.Add(comp);
                                break;
                            }
                        }
                    }
                }

                comparisonList.ItemsSource = selectedComparisons;
            }

            if (selectedComparisons.Count > 0)
            {
                foreach (ComparisonViewModel c in selectedComparisons)
                    c.IsNotLast = true;
                selectedComparisons.Last().IsNotLast = false;
            }
        }

        private void comparisonListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            comparisonList.SelectedItem = null;
        }

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            SCAService.Opportunity comparison = (e.Item as ComparisonViewModel).Comparison;

            Navigation.PushAsync(new ViewComparison(comparison));
        }

        private void OnSelectClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ViewComparison((SCAService.Opportunity)((Button)sender).CommandParameter));
        }

        private void OnCategoryClicked(object sender, EventArgs e)
        {
            ToggleButton btn = sender as ToggleButton;

            if (btn == catAll)
            {
                _allCategories = btn.Checked;
                if (_allCategories)
                {
                    catBriefs.Checked = false;
                    catUnderwear.Checked = false;
                    catPads.Checked = false;
                    catUnderpads.Checked = false;
                    catSkincare.Checked = false;
                    catFixationPants.Checked = false;
                    _selectedCategories.Clear();
                }
            }
            else
            {
                if (_allCategories)
                {
                    _allCategories = false;
                    catAll.Checked = false;
                }

                if (btn.Checked)
                    _selectedCategories.Add(int.Parse(btn.UserData.ToString()));
                else
                    _selectedCategories.Remove(int.Parse(btn.UserData.ToString()));
            }

            loadComparisons();
        }
    }
}
