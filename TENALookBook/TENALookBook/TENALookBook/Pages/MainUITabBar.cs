﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SCAMobile.Pages
{
    public class MainUITabBar
    {
        public const uint PAGE_INDEX_SEARCH = 0;
        public const uint PAGE_INDEX_CATALOGUE = 1;
        public const uint PAGE_INDEX_ACCOUNTS = 2;
        public const uint PAGE_INDEX_NOTIFICATIONS = 3;
        public const uint PAGE_INDEX_SETTINGS = 4;
        public const uint PAGE_INDEX_COMPARE = 5;

        private List<Page> _pages = new List<Page>()
        {
            null,
            null,
            null,
            null,
            null,
            null
        };

        public Page getPage(uint index)
        {
            Page page = null;
            NavigationPage navPage = null;

            if (index > PAGE_INDEX_COMPARE)
                return null;

            page = _pages[(int)index];

            if (page == null)
            {
                switch (index)
                {
                    case PAGE_INDEX_SEARCH:
                    {
                        navPage = new NavigationPage(new SCAMobile.Pages.Search());
                        page = navPage;

                        break;
                    }

                    case PAGE_INDEX_CATALOGUE:
                    {
                        navPage = new NavigationPage(new SCAMobile.Pages.Catalogue(@"Catalogue", false));
                        page = navPage;

                        break;
                    }

                    case PAGE_INDEX_ACCOUNTS:
                    {
                        navPage = new NavigationPage(new SCAMobile.Pages.Accounts());
                        page = navPage;

                        break;
                    }

                    case PAGE_INDEX_NOTIFICATIONS:
                        navPage = new NavigationPage(new SCAMobile.Pages.Notifications());
                        page = navPage;
                        break;

                    case PAGE_INDEX_SETTINGS:
                        navPage = new NavigationPage(new SCAMobile.Pages.Settings());
                        page = navPage;
                        break;

                    case PAGE_INDEX_COMPARE:
                        navPage = new NavigationPage(new SCAMobile.Pages.Compare());
                        page = navPage;
                        break;
                }

                if (navPage != null)
                {
                    navPage.BarBackgroundColor = Color.FromHex(@"065695");
                    navPage.BarTextColor = Color.White;
                }

                _pages[(int)index] = page;
            }

            return page;
        }

        public void Reset()
        {
            _pages = new List<Page>()
            {
                null,
                null,
                null,
                null,
                null,
                null
            };
        }
    }
}
