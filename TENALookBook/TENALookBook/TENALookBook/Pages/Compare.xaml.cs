﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SCAMobile.Models;
using SCAMobile.Controls;
using FormsToolkit;
using SCAMobile.Constants;
using SCAMobile.SCAService;
using System.Collections.ObjectModel;
using System.ComponentModel;
using SCAMobile.Data;
using System.Text.RegularExpressions;

namespace SCAMobile.Pages
{
    public partial class Compare : ContentPage
    {
        class AlternativeProduct
        {
            public int EssityId { get; set; }
            public Product EssityProduct { get; set; }
            public Product ConcurentProduct { get; set; }
        }
        private List<AlternativeProduct> alternativeProducts = new List<AlternativeProduct>();
        private List<IGrouping<int?, Product>> companyProducts;
        private List<SCAEmlService.Companies> companies;
        private List<string> companiesName = new List<string>();

        private List<CategoryViewModel> categoriesList { get; set; } = new List<CategoryViewModel>();

        SendEmailServiceClient serviceSendEmail = new SendEmailServiceClient();
        public Compare()
        {
            InitializeComponent();
            App app = Application.Current as App;


            NavigationPage.SetHasNavigationBar(this, false);
            InitializateList();
           
            stackLayoutContent.Orientation = StackOrientation.Horizontal;
            Version ver = typeof(App).GetTypeInfo().Assembly.GetName().Version;            
        }
        private void btnPopupButton_Clicked(object sender, EventArgs e)
        {
            ShowContent(false);
        }
        private void btnSendButton_Clicked(object sender, EventArgs e)
        {
            fullContent.IsVisible = false;
            sendEmailView.IsVisible = true;
        }
        private void OnSendPopUpClicked(object sender, EventArgs e)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            if (string.IsNullOrEmpty(emailText.Text)) return;

            if (regex.IsMatch(emailText.Text) && companies.Count>0)
            {
                serviceSendEmail.SendEmailMEssage(companies, emailText.Text);
                emailText.Text = "";
                fullContent.IsVisible = true;
                sendEmailView.IsVisible = false;
            }
            else
            {
                if(companies.Count==0)MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = @"Error",
                    Cancel = @"OK",
                    Message = @"Before sending the data by email you must first search them."
                });
                else MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = @"Wrong Email",
                    Cancel = @"OK",
                    Message = @"Please enter a valid email."
                });
            }
        }
        #region UI Implementation
        private void InitializateList()
        {
            companyProducts = new List<IGrouping<int?, Product>>();
            companies = new List<SCAEmlService.Companies>();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (Device.OS == TargetPlatform.Windows)
            {
                NavigationPage.SetHasNavigationBar(this, true);
                NavigationPage.SetHasNavigationBar(this, false);
            }
        }

        void OnSearchClicked(object sender, EventArgs e)
        {
            ShowContent();

            if (string.IsNullOrWhiteSpace(this.searchTerm.Text))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = @"Error",
                    Cancel = @"OK",
                    Message = @"Please enter Product Name or Article Number"
                });
            }
            else
            {
                App.ShowLoading(true, @"Searching");

                App app = Application.Current as App;
                SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

                client.searchProductsCompleted += searchProductsCompleted;
                client.searchProductsAsync(this.searchTerm.Text, null, null, null, null, null, false, app.LoginToken);
            }
        }

        private void AddStackLayoutChild()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                stackLayoutContent.Children.Clear();
                foreach (var el in companies)
                {
                    if (el.companyProducts.Any(itm => !string.IsNullOrEmpty(itm.Article)))
                        stackLayoutContent.Children.Add(new CompanyProducts(el));
                }
                App.ShowLoading(false);
            });

        }
        private void UpdateCategoriesListContainer()
        {
            filterCategories.Children.Clear();
            foreach (var el in categoriesList)
            {
                CategoryViewFilter view1 = new CategoryViewFilter(el);
                view1.OnCategorySelected += onCategorySelected;
                filterCategories.Children.Add(view1);
            }
        }

        private void searchProductsCompleted(object sender, SCAService.searchProductsCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {

                if (e.Error == null)
                {
                    if (e.Result.Count() == 0)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                        {
                            Title = @"Information",
                            Cancel = @"OK",
                            Message = @"No products found"
                        });
                    }
                    else
                    {
                        companies = new List<SCAEmlService.Companies>();
                        alternativeProducts = new List<AlternativeProduct>();
                        companiesName.Add("Essity");
                        companiesName.AddRange(e.Result.GroupBy(itm => itm.CompanyId).Select(el => el.FirstOrDefault().CompanyName).ToList());
                        companyProducts = e.Result.GroupBy(itm => itm.CompanyId).ToList();
                        var categoriesList1 = e.Result.Select(el => el.Category).Distinct().ToList();
                        categoriesList = categoriesList1.Select(el=>new CategoryViewModel { Category=el}).ToList();
                        UpdateCategoriesListContainer();
                        InitializeAlternativesProductsFromEssity();
                    }
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }

        private void InitializeAlternativesProductsFromEssity()
        {
            foreach (var itm in companyProducts)
            {
                Parallel.ForEach(itm, func =>
                {

                    App app = Application.Current as App;
                    SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();
                    client.getProductAlternativesCompleted += (o, e) =>
                    {
                        if (e.Error == null)
                        {
                            if (e.Result.Count() > 0)
                            {
                                foreach (var elemt in e.Result)
                                {
                                    alternativeProducts.Add(new AlternativeProduct
                                    {
                                        ConcurentProduct = func,
                                        EssityProduct = elemt,
                                        EssityId = elemt.Id
                                    });
                                }
                                if (itm.Key==companyProducts.LastOrDefault().Key) { InitializeViewModel(); }
                                //if (itm.LastOrDefault().Id == companyProducts.LastOrDefault().LastOrDefault().Id)
                                //{
                                //    InitializeViewModel();
                                //}
                            }
                        }
                        else
                        {
                            App.scaServiceUnavailableErrorMessage(e.Error);
                        }

                    };
                    client.getProductAlternativesAsync(func.Id, app.LoginToken);
                });
            }
            var elmenrt = alternativeProducts.GroupBy(itm => itm.EssityId);

        }

        private void InitializeViewModel()
        {
            companies = new List<SCAEmlService.Companies>();
            string[] stringColors = { "#80e3e1e1", "#80fae996", "#806df2b0", "#809efff9", "#8091c6ff", "#80db94ff", "#80ff87cd" };
            foreach (var name in companiesName)
            {
                companies.Add(new SCAEmlService.Companies { CompanyName = name, companyProducts = new ObservableCollection<SCAEmlService.CompanyProduct>() });
            }
            int colorIterator = 0;
            var groupAllElementsByEssityID = alternativeProducts.GroupBy(it => it.EssityId).ToList();
            foreach (var grin in groupAllElementsByEssityID)
            {
                string color = stringColors[colorIterator % stringColors.Length];
                colorIterator++;
                companies.FirstOrDefault().companyProducts.Add(
                    new SCAEmlService.CompanyProduct
                    {
                        Article = grin.FirstOrDefault().EssityProduct.ArticleNumber,
                        BagCount = grin.FirstOrDefault().EssityProduct.BagCount,
                        Pieces = grin.FirstOrDefault().EssityProduct.PiecesPerCase,
                        Size = grin.FirstOrDefault().EssityProduct.SizePrinted,
                        BackgroundColor = color
                    });
                foreach (var itm in grin)
                {
                    foreach (var el in companies)
                    {
                        if (itm.ConcurentProduct.CompanyName.Contains(el.CompanyName))
                        {
                            el.companyProducts.Add(
                                new SCAEmlService.CompanyProduct
                                {
                                    Article = itm.ConcurentProduct.ArticleNumber,
                                    BagCount = itm.ConcurentProduct.BagCount,
                                    Pieces = itm.ConcurentProduct.PiecesPerCase,
                                    Size = itm.ConcurentProduct.ProductSize,
                                    BackgroundColor = color
                                });
                        }
                        else
                        {
                            if (!el.CompanyName.Contains("Essity"))
                            {

                            }
                            el.companyProducts.Add(
                                new SCAEmlService.CompanyProduct
                                {
                                    Article = "",
                                    BagCount = null,
                                    Pieces = null,
                                    Size = "",
                                    BackgroundColor = color
                                });

                        }
                    }
                }
                companies.FirstOrDefault().companyProducts.RemoveAt(companies.FirstOrDefault().companyProducts.Count-1);
            }
            AddStackLayoutChild();
        }
                
        private void InitializeFilteredViewModel(bool IsFiltered = false)
        {
            ///When user try to filter all list by categories 
            var selectedListOfCategories = categoriesList.Where(it => it.IsSelected).Select(el => el.Category).ToList();
            var filteredAlternativeProducts = alternativeProducts.Where(el => selectedListOfCategories.Contains(el.EssityProduct.Category) &&
                                                                             selectedListOfCategories.Contains(el.ConcurentProduct.Category)).ToList();

            var alternativeProductsFiltered = IsFiltered ? filteredAlternativeProducts : alternativeProducts;
            ///end implementation of filtering prerequisites 

            companies = new List<SCAEmlService.Companies>();
            string[] stringColors = { "#80e3e1e1", "#80fae996", "#806df2b0", "#809efff9", "#8091c6ff", "#80db94ff", "#80ff87cd" };
            foreach (var name in companiesName)
            {
                companies.Add(new SCAEmlService.Companies { CompanyName = name, companyProducts = new ObservableCollection<SCAEmlService.CompanyProduct>() });
            }
            int colorIterator = 0;
            var groupAllElementsByEssityID = alternativeProductsFiltered.GroupBy(it => it.EssityId).ToList();
            foreach (var grin in groupAllElementsByEssityID)
            {
                string color = stringColors[colorIterator % stringColors.Length];
                colorIterator++;
                companies.FirstOrDefault().companyProducts.Add(
                    new SCAEmlService.CompanyProduct
                    {
                        Article = grin.FirstOrDefault().EssityProduct.ArticleNumber,
                        BagCount = grin.FirstOrDefault().EssityProduct.BagCount,
                        Pieces = grin.FirstOrDefault().EssityProduct.PiecesPerCase,
                        Size = grin.FirstOrDefault().EssityProduct.SizePrinted,
                        BackgroundColor = color
                    });
                foreach (var itm in grin)
                {
                    foreach (var el in companies)
                    {
                        if (itm.ConcurentProduct.CompanyName.Contains(el.CompanyName))
                        {
                            el.companyProducts.Add(
                                new SCAEmlService.CompanyProduct
                                {
                                    Article = itm.ConcurentProduct.ArticleNumber,
                                    BagCount = itm.ConcurentProduct.BagCount,
                                    Pieces = itm.ConcurentProduct.PiecesPerCase,
                                    Size = itm.ConcurentProduct.ProductSize,
                                    BackgroundColor = color
                                });
                        }
                        else
                        {
                            if (!el.CompanyName.Contains("Essity"))
                            {

                            }
                            el.companyProducts.Add(
                                new SCAEmlService.CompanyProduct
                                {
                                    Article = "",
                                    BagCount = null,
                                    Pieces = null,
                                    Size = "",
                                    BackgroundColor = color
                                });

                        }
                    }
                }
                companies.FirstOrDefault().companyProducts.RemoveAt(companies.FirstOrDefault().companyProducts.Count - 1);
            }
            AddStackLayoutChild();
        }

        private void ShowContent(bool Show = true)
        {
            fullContent.IsVisible = Show;
            //stackLayoutContent.IsVisible = Show;
            popupListView.IsVisible = !Show;
            sendEmailView.IsVisible = !Show;
        }
        #endregion

        private void onCategorySelected(object sender, Controls.CategoryViewSelectedEventArgs e)
        {
            ContentView cnt = (ContentView)sender;
            e.CategoryName.IsSelected = !e.CategoryName.IsSelected;
            var element = categoriesList.Where(it => it.Category == e.CategoryName.Category).FirstOrDefault();
            element.IsSelected = e.CategoryName.IsSelected;

            //if (e.CategoryName.IsSelected){
            //    cnt.BackgroundColor= Color.FromHex("#14ff30");
            //}else{cnt.BackgroundColor = Color.FromHex("#ffffff");}
        }

        void OnCancelPopUpClicked(object sender, EventArgs e)
        {
            ShowContent();
        }
        void OnApplyPopUpClicked(object sender,EventArgs e)
        {
            bool isFiltered = categoriesList.Any(it => it.IsSelected);
            InitializeFilteredViewModel(isFiltered);
            ShowContent();
        }
    }
}