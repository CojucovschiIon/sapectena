﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using CarouselView.FormsPlugin.Abstractions;
using FFImageLoading.Forms;
using FormsToolkit;
using SCAMobile.Constants;

namespace SCAMobile.Pages
{
    public class ProductViewModel
    {
        public SCAService.Product Product { get; set; }

        public string ProductImage { get; set; }
    }

    public class SellingPointViewModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsNameVisible { get; set; }
    }

    public class PerformanceMetricViewModel
    {
        public string Name { get; set; }

        public string CompValue { get; set; }

        public string ScaValue { get; set; }
    }

    public class ProductNameViewModel
    {
        public string CompName { get; set; }

        public string ScaName { get; set; }
    }

    public class AltProductsListDataTemplateSelector : DataTemplateSelector
    {
        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if (item is ProductViewModel)
                return ProductTemplate;
            else
                return AddProductTemplate;
        }

        public DataTemplate ProductTemplate { get; set; }
        public DataTemplate AddProductTemplate { get; set; }
    }

    public partial class CompareProducts : ContentPage
    {
        private struct TabDescription
        {
            public string image;
            public string selectedImage;
            public Layout view;
            public Button button;
            public Label label;
        }

        private int _currentTab;
        private List<TabDescription> _tabs = new List<TabDescription>();
        private List<object> _products;
        SCAService.Product _compProduct;
        SCAService.Product _scaProduct;

        public CompareProducts(SCAService.Product product, List<object> altProducts, string tab)
        {
            BindingContext = this;
            Tab = tab;

            _products = new List<object>();
            foreach (object o in altProducts)
            {
                if (o is SCAService.Product)
                {
                    _products.Add(
                        new ProductViewModel
                        {
                            Product = (SCAService.Product)o,
                            ProductImage = App.getProductImage((SCAService.Product)o)
                        });
                }
                else
                {
                    _products.Add(o);
                }
            }

            _compProduct = product;

            InitializeComponent();

            App app = Application.Current as App;

            if (app.Login.SCACompanyId == product.CompanyId)
                productImage.Source = @"tena-product.png";

            _tabs.Add(new TabDescription
            {
                image = @"Description-Large.png",
                selectedImage = @"Description-Large-Selected.png",
                view = this.productDescriptionComparision,
                button = this.descriptionBtn,
                label = this.descriptionLbl
            });

            _tabs.Add(new TabDescription
            {
                image = @"Image-Large.png",
                selectedImage = @"Image-Large-Selected.png",
                view = this.productMediaComparision,
                button = this.mediaBtn,
                label = this.mediaLbl
            });

            _tabs.Add(new TabDescription
            {
                image = @"Performance-Large.png",
                selectedImage = @"Performance-Large-Selected.png",
                view = this.productPerformanceComparision,
                button = this.performanceBtn,
                label = this.performanceLbl
            });

            _tabs.Add(new TabDescription
            {
                image = @"Selling Points-Large.png",
                selectedImage = @"SellingPoints-Large-Selected.png",
                view = this.productSellingPointsComparision,
                button = this.sellingPointsBtn,
                label = this.sellingPointsLbl
            });

            competitorCompany.Text = product.CompanyName;
            competitorProductName.Text = product.MarketDesc;
//            competitorProductName2.Text = product.MarketDesc;
            competitorProductName4.Text = product.MarketDesc;
            competitorCategory.Text = product.Category;
            competitorArticle.Text = product.ArticleNumber;
            competitorBagCount.Text = string.Format(@"{0}", product.BagCount);
            competitorCaseCount.Text = string.Format(@"{0}", product.CaseCount);
            competitorPiecesPerCase.Text = string.Format(@"{0}", product.PiecesPerCase);
            competitorProductSize.Text = product.ProductSizeDesc;
            competitorWaistSize.Text = product.WaistSize;
            productImage.Source = App.getProductImage(product);
            loadImages(product, competitorImageList, competitorNoImagesLabel);
            loadVideos(product, competitorVideoList, competitorNoVideosLabel);

            scaAlternativeCarousel.PositionSelected = OnPositionSelected;
            if (_products.Count() > 1)
            {
                _scaProduct = ((ProductViewModel)_products[0]).Product;
                loadSCAProduct(_scaProduct);
            }

            loadPerformance(_compProduct, _scaProduct);
        }

        public string Tab { get; set; }

        public List<object> AlternativeProducts
        {
            get { return _products; }

            set
            {
                _products = value;
//                OnPropertyChanged(@"AlternativeProducts");
            }
        }

        public void AddProduct(SCAService.Product product)
        {
            _products = new List<object>(_products);
            _products.Insert(0, new ProductViewModel { Product = product, ProductImage = App.getProductImage(product) });
            scaAlternativeCarousel.Position = 0;
            scaAlternativeCarousel.ItemsSource = _products;
            _scaProduct = product;
            loadSCAProduct(product);
            loadPerformance(_compProduct, _scaProduct);
        }

        private void OnDescriptionClicked(object sender, EventArgs e)
        {
            if (_currentTab != 0)
                showTab(0);
        }

        private void OnMediaClicked(object sender, EventArgs e)
        {
            if (_currentTab != 1)
                showTab(1);
        }

        private void OnPerformanceClicked(object sender, EventArgs e)
        {
            if (_currentTab != 2)
                showTab(2);
        }

        private void OnSellingPointsClicked(object sender, EventArgs e)
        {
            if (_currentTab != 3)
                showTab(3);
        }

        private void showTab(int tabIndex)
        {
            _currentTab = tabIndex;

            for (int i = 0; i < _tabs.Count(); i++)
            {
                TabDescription tab = _tabs[i];

                if (i == tabIndex)
                {
                    if (!tab.view.IsVisible)
                    {
                        tab.view.IsVisible = true;
                        tab.button.Image = tab.selectedImage;
                        tab.label.TextColor = Color.FromHex(@"85ba35");
                    }
                }
                else
                {
                    if (tab.view.IsVisible)
                    {
                        tab.view.IsVisible = false;
                        tab.button.Image = tab.image;
                        tab.label.TextColor = Color.FromHex(@"a2a2a0");
                    }
                }
            }
        }

        private void OnSaveClicked(object sender, EventArgs e)
        {
            if (_scaProduct != null)
            {
                Navigation.PushAsync(new SaveComparision(_compProduct, _scaProduct));
            }
            else
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = @"Error",
                    Cancel = @"OK",
                    Message = @"You need to attach an SCA Product to this comparison."
                });
            }
        }

        private void OnPositionSelected(object sender, EventArgs e)
        {
            if (scaAlternativeCarousel.Position < _products.Count() - 1)
            {
                _scaProduct = ((ProductViewModel)_products[scaAlternativeCarousel.Position]).Product;
                loadSCAProduct(_scaProduct);
            }
            else
            {
                _scaProduct = null;
                loadSCAProduct(null);
            }

            loadPerformance(_compProduct, _scaProduct);
        }

        private void loadSCAProduct(SCAService.Product product)
        {
            if (product != null)
            {
                scaCompany.Text = product.CompanyName;
                this.scaCategory.Text = product.Category;
                this.scaArticle.Text = product.ArticleNumber;
                this.scaBagCount.Text = string.Format(@"{0}", product.BagCount);
                this.scaCaseCount.Text = string.Format(@"{0}", product.CaseCount);
                this.scaPiecesPerCase.Text = string.Format(@"{0}", product.PiecesPerCase);
                this.scaProductSize.Text = product.ProductSizeDesc;
                this.scaWaistSize.Text = product.WaistSize;
                this.scaProductName.Text = product.MarketDesc;

                this.scaProductName2.Text = product.MarketDesc;

//                this.scaProductName3.Text = product.MarketDesc;

                this.scaProductName4.Text = product.MarketDesc;

                loadImages(product, scaImageList, scaNoImagesLabel);
                loadVideos(product, scaVideoList, scaNoVideosLabel);
                loadSellingPoints(product);
            }
            else
            {
                scaCompany.Text = @"";
                this.scaCategory.Text = @"";
                this.scaArticle.Text = @"";
                this.scaBagCount.Text = @"";
                this.scaCaseCount.Text = @"";
                this.scaPiecesPerCase.Text = @"";
                this.scaProductSize.Text = @"";
                this.scaWaistSize.Text = @"";
                this.scaProductName.Text = @"";

                this.scaProductName2.Text = @"";
                this.scaSellingPoints.ItemsSource = null;

//                this.scaProductName3.Text = @"";

                this.scaProductName4.Text = @"";
                scaImageList.Children.Clear();
                scaImageList.IsVisible = false;
                scaNoImagesLabel.IsVisible = true;

                scaVideoList.Children.Clear();
                scaVideoList.IsVisible = false;
                scaNoVideosLabel.IsVisible = true;
            }
        }

        private void loadSellingPoints(SCAService.Product product)
        {
            App app = Application.Current as App;
            SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

            client.getProductSellingPointsCompleted += getProductSellingPointsCompleted;
            client.getProductSellingPointsAsync(product.Id, _compProduct.Id, app.LoginToken, product);
        }

        private void getProductSellingPointsCompleted(object sender, SCAService.getProductSellingPointsCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                SCAService.Product product = e.UserState as SCAService.Product;

                if (e.Error == null && e.Result.Count() > 0)
                {
                    ObservableCollection<SellingPointViewModel> sellingPoints = new ObservableCollection<SellingPointViewModel>();

                    foreach (SCAService.SellingPoints sp in e.Result)
                    {
                        if (!string.IsNullOrWhiteSpace(sp.Point1))
                        {
                            sellingPoints.Add(new SellingPointViewModel
                            {
                                Description = sp.Point1
                            });
                        }

                        if (!string.IsNullOrWhiteSpace(sp.Point2))
                        {
                            sellingPoints.Add(new SellingPointViewModel
                            {
                                Description = sp.Point2
                            });
                        }

                        if (!string.IsNullOrWhiteSpace(sp.Point3))
                        {
                            sellingPoints.Add(new SellingPointViewModel
                            {
                                Description = sp.Point3
                            });
                        }

                        if (!string.IsNullOrWhiteSpace(sp.Point4))
                        {
                            sellingPoints.Add(new SellingPointViewModel
                            {
                                Description = sp.Point4
                            });
                        }

                        if (!string.IsNullOrWhiteSpace(sp.Point5))
                        {
                            sellingPoints.Add(new SellingPointViewModel
                            {
                                Description = sp.Point5
                            });
                        }
                    }

                    this.scaSellingPoints.ItemsSource = sellingPoints;
                }
                else if (product.Benefits.Count > 0)
                {
                    this.scaSellingPoints.ItemsSource =
                        new ObservableCollection<SellingPointViewModel>(
                            product.Benefits.Select(m => new SellingPointViewModel
                            {
                                Name = m.BenefitName,
                                Description = m.LongBenefit,
                                IsNameVisible = true
                            }));
                }
                else
                {
                    this.scaSellingPoints.ItemsSource = new ObservableCollection<SellingPointViewModel>
                    {
                        new SellingPointViewModel
                        {
                            Description = product.ProductDesc,
                        }
                    };
                }
            });
        }

        private void loadPerformance(SCAService.Product compProduct, SCAService.Product scaProduct)
        {
            ObservableCollection<PerformanceMetricViewModel> performance = new ObservableCollection<PerformanceMetricViewModel>();
            string compValue;
            string scaValue;

            compValue = compProduct.Absorption == null ? @"" : compProduct.Absorption.ToString();
            scaValue = scaProduct == null ? @"" : (scaProduct.Absorption == null ? @"" : scaProduct.Absorption.ToString());
            addPerformanceMetric(@"Rothwell", ref compValue, ref scaValue, performance);

            TypeInfo productTypeInfo = typeof(SCAService.Product).GetTypeInfo();

            foreach (PerformanceMetric pm in Globals.PerformanceMetrics)
            {
                PropertyInfo propInfo = productTypeInfo.GetDeclaredProperty(pm.PropName);

                if (propInfo != null)
                {
                    compValue = propInfo.GetValue(compProduct) as string;
                    scaValue = scaProduct == null ? @"" : propInfo.GetValue(scaProduct) as string;
                    addPerformanceMetric(pm.DisplayName, ref compValue, ref scaValue, performance);
                }
            }

            this.productPerformanceList.ItemsSource = performance;
            this.productPerformanceList.Header = new ProductNameViewModel
            {
                CompName = compProduct.MarketDesc,
                ScaName = scaProduct == null ? @"" : scaProduct.MarketDesc
            };

/*            {
                int row = productPerformanceList.RowDefinitions.Count();

                productPerformanceList.RowDefinitions.Add(new RowDefinition
                {
                    Height = GridLength.Star
                });

                productPerformanceList.Children.Add(new ContentView
                {
                    Content = new Label
                    {
                        HorizontalTextAlignment = TextAlignment.Center
                    },
                    Padding = 10,
                    BackgroundColor = Color.FromHex(@"f9f6f2")
                }, 0, row);

                productPerformanceList.Children.Add(new ContentView
                {
                    Content = new Label
                    {
                        HorizontalTextAlignment = TextAlignment.Center,
                        TextColor = Color.FromHex(@"006aa9")
                    },
                    Padding = 0,
                    BackgroundColor = Color.FromHex(@"f9f6f2")
                }, 1, row);

                productPerformanceList.Children.Add(new ContentView
                {
                    Content = new Label
                    {
                        HorizontalTextAlignment = TextAlignment.Center
                    },
                    Padding = 10,
                    BackgroundColor = Color.FromHex(@"f9f6f2")
                }, 2, row);
            }
*/
        }

        private void addPerformanceMetric(string name, ref string compValue, ref string scaValue, ObservableCollection<PerformanceMetricViewModel> performanceList)
        {
            if (!string.IsNullOrWhiteSpace(compValue) || !string.IsNullOrWhiteSpace(scaValue))
            {
                performanceList.Add(new PerformanceMetricViewModel
                {
                    Name = name,
                    CompValue = compValue,
                    ScaValue = scaValue,
                });
            }
        }

        /*
                private void loadPerformance(SCAService.Product compProduct, SCAService.Product scaProduct)
                {
                    string compValue;
                    string scaValue;

                    while (productPerformanceList.RowDefinitions.Count() > 1)
                    {
                        productPerformanceList.Children.RemoveAt(3);
                        productPerformanceList.Children.RemoveAt(3);
                        productPerformanceList.Children.RemoveAt(3);
                        productPerformanceList.RowDefinitions.RemoveAt(1);
                    }

                    compValue = compProduct.Absorption  == null ? @"" : compProduct.Absorption.ToString();
                    scaValue = scaProduct == null ? @"" : (scaProduct.Absorption == null ? @"" : scaProduct.Absorption.ToString());
                    addPerformanceMetric(@"Rothwell", ref compValue, ref scaValue);

                    TypeInfo productTypeInfo = typeof(SCAService.Product).GetTypeInfo();

                    foreach (PerformanceMetric pm in Globals.PerformanceMetrics)
                    {
                        PropertyInfo propInfo = productTypeInfo.GetDeclaredProperty(pm.PropName);

                        if (propInfo != null)
                        {
                            compValue = propInfo.GetValue(compProduct) as string;
                            scaValue = scaProduct == null ? @"" : propInfo.GetValue(scaProduct) as string;
        //                    addPerformanceMetric(pm.DisplayName, ref compValue, ref scaValue);
                        }
                    }

                    //            if (productPerformanceList.RowDefinitions.Count == 1)
                    {
                        int row = productPerformanceList.RowDefinitions.Count();

                        productPerformanceList.RowDefinitions.Add(new RowDefinition
                        {
                            Height = GridLength.Star
                        });

                        productPerformanceList.Children.Add(new ContentView
                        {
                            Content = new Label
                            {
                                HorizontalTextAlignment = TextAlignment.Center
                            },
                            Padding = 10,
                            BackgroundColor = Color.FromHex(@"f9f6f2")
                        }, 0, row);

                        productPerformanceList.Children.Add(new ContentView
                        {
                            Content = new Label
                            {
                                HorizontalTextAlignment = TextAlignment.Center,
                                TextColor = Color.FromHex(@"006aa9")
                            },
                            Padding = 0,
                            BackgroundColor = Color.FromHex(@"f9f6f2")
                        }, 1, row);

                        productPerformanceList.Children.Add(new ContentView
                        {
                            Content = new Label
                            {
                                HorizontalTextAlignment = TextAlignment.Center
                            },
                            Padding = 10,
                            BackgroundColor = Color.FromHex(@"f9f6f2")
                        }, 2, row);
                    }
                }
        */

        /*
                private void addPerformanceMetric(string name, ref string compValue, ref string scaValue)
                {
                    if (!string.IsNullOrWhiteSpace(compValue) || !string.IsNullOrWhiteSpace(scaValue))
                    {
                        int row = productPerformanceList.RowDefinitions.Count();

                        productPerformanceList.RowDefinitions.Add(new RowDefinition
                        {
                            Height = GridLength.Auto
                        });

                        StackLayout sl = new StackLayout
                        {
                            Padding = 10,
                            BackgroundColor = Color.FromHex(@"f9f6f2"),
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            Orientation = StackOrientation.Vertical
                        };

                        sl.Children.Add(new Label
                        {
                            Text = compValue,
                            HorizontalTextAlignment = TextAlignment.Center,
                            VerticalOptions = LayoutOptions.CenterAndExpand,
                            LineBreakMode = LineBreakMode.WordWrap,
                        });

                        productPerformanceList.Children.Add(sl, 0, row);

                        sl = new StackLayout
                        {
                            Padding = 10,
                            BackgroundColor = Color.FromHex(@"f9f6f2"),
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            Orientation = StackOrientation.Vertical
                        };

                        sl.Children.Add(new Label
                        {
                            Text = name,
                            HorizontalTextAlignment = TextAlignment.Center,
                            VerticalOptions = LayoutOptions.CenterAndExpand,
                            TextColor = Color.FromHex(@"006aa9"),
                            LineBreakMode = LineBreakMode.WordWrap,
                        });

                        productPerformanceList.Children.Add(sl, 1, row);

                        sl = new StackLayout
                        {
                            Padding = 10,
                            BackgroundColor = Color.FromHex(@"f9f6f2"),
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            Orientation = StackOrientation.Vertical
                        };

                        sl.Children.Add(new Label
                        {
                            Text = scaValue,
                            HorizontalTextAlignment = TextAlignment.Center,
                            VerticalOptions = LayoutOptions.CenterAndExpand,
                            LineBreakMode = LineBreakMode.WordWrap,
                        });

                        productPerformanceList.Children.Add(sl, 2, row);
                    }
                }
        */

        private void loadImages(SCAService.Product product, Grid grid, Label noImagesLabel)
        {
            var productImages = product.Images.Where(m => m.Type != @"VD");

            if (productImages.Count() > 0)
            {
                int cols = 4;
                int rows = productImages.Count() + (cols - 1) / cols;
                int col = 0, row = 0;

                grid.IsVisible = true;
                noImagesLabel.IsVisible = false;

                foreach (SCAService.ProductImage image in productImages)
                {
                    CachedImage cachedImage = new CachedImage
                    {
                        Source = image.ImageUrl,
                        HeightRequest = 80,
                        WidthRequest = 80,
                        HorizontalOptions = LayoutOptions.Center,
                        Aspect = Aspect.AspectFit
                    };

                    TapGestureRecognizer gesture = new TapGestureRecognizer();
                    gesture.Tapped += OnImageTapped;
                    cachedImage.GestureRecognizers.Add(gesture);

                    grid.Children.Add(cachedImage, col, row);

                    if (++col == cols)
                    {
                        row++;
                        col = 0;
                    }
                }
            }
            else
            {
                grid.IsVisible = false;
                noImagesLabel.IsVisible = true;
            }
        }

        private void loadVideos(SCAService.Product product, Grid grid, Label noVideosLabel)
        {
            App app = Application.Current as App;
            var productVideos = product.Images.Where(m => m.Type == @"VD").OrderBy(m => m.orderby);

            if (productVideos.Count() > 0)
            {
                int cols = 4;
                int rows = productVideos.Count() + (cols - 1) / cols;
                int col = 0, row = 0;

                grid.IsVisible = true;
                noVideosLabel.IsVisible = false;

                foreach (SCAService.ProductImage i in productVideos)
                {
                    string thumbNail = @"YouTube.png";
                    string videoId = App.getYouTubeVideoId(i.ImageUrl);

                    if (!string.IsNullOrWhiteSpace(videoId))
                        thumbNail = string.Format(@"https://img.youtube.com/vi/{0}/0.jpg", videoId);

                    CachedImage image = new CachedImage
                    {
                        Source = thumbNail,
                        HeightRequest = 80,
                        WidthRequest = 80,
                        HorizontalOptions = LayoutOptions.Center,
                        Aspect = Aspect.AspectFit,
                        LoadingPlaceholder = app.Login.SCACompanyId == product.CompanyId ? @"tena-product.png" : @"product.png"
                    };

                    TapGestureRecognizer gesture = new TapGestureRecognizer();
                    gesture.Tapped += OnVideoTapped;
                    image.GestureRecognizers.Add(gesture);

                    grid.Children.Add(image, col, row);

                    if (++col == cols)
                    {
                        row++;
                        col = 0;
                    }
                }
            }
            else
            {
                grid.IsVisible = false;
                noVideosLabel.IsVisible = true;
            }
        }

        private void OnDeleteClicked(object sender, EventArgs e)
        {
            scaAlternativeCarousel.RemovePage(scaAlternativeCarousel.Position);
        }

        private void OnAddClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Catalogue(@"Search", true, false, false));
        }

        private void OnImageTapped(object sender, EventArgs e)
        {
            CachedImage image = sender as CachedImage;
            List<string> productImages = new List<string>();
            SCAService.Product product;
            int imageIndex;

            if (image.Parent == competitorImageList)
            {
                product = _compProduct;
                imageIndex = competitorImageList.Children.IndexOf(image);
            }
            else
            {
                product = _scaProduct;
                imageIndex = scaImageList.Children.IndexOf(image);
            }

            foreach (SCAService.ProductImage i in product.Images.Where(m => m.Type != @"VD").OrderBy(m => m.orderby))
                productImages.Add(i.ImageUrl);

            if (image.Source is UriImageSource)
                Navigation.PushAsync(new ImageViewer(productImages, imageIndex, Tab, product.MarketDesc + " images", false));
        }

        private void OnVideoTapped(object sender, EventArgs e)
        {
            CachedImage image = sender as CachedImage;
            SCAService.Product product;
            int videoIndex;
            IList<SCAService.ProductImage> productVideos;

            if (image.Parent == competitorVideoList)
            {
                product = _compProduct;
                videoIndex = competitorVideoList.Children.IndexOf(image);
            }
            else
            {
                product = _scaProduct;
                videoIndex = scaVideoList.Children.IndexOf(image);
            }

            productVideos = product.Images.Where(m => m.Type == @"VD").OrderBy(m => m.orderby).ToList();
            Navigation.PushAsync(new VideoViewer(product, productVideos[videoIndex].ImageUrl, Tab, false));
//            Device.OpenUri(new Uri(productVideos[videoIndex].ImageUrl));
        }

        private void scaSellingPointSelected(object sender, SelectedItemChangedEventArgs e)
        {
            scaSellingPoints.SelectedItem = null;
        }
    }
}
