﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace SCAMobile.Pages
{
    public partial class ImageViewer : ContentPage
    {
        public string Tab { get; set; }

        public IList<string> Images { get; set; }

        public int InitialPosition { get; set; }

        public ImageViewer(IList<string> images, int initialImage, string tab, string title, bool navBarVisible = true)
        {
            BindingContext = this;
            Tab = tab;
            Images = images;
            InitialPosition = initialImage;

            InitializeComponent();

            Title = title;
            navBar.IsVisible = navBarVisible;
        }
    }
}
