﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using FormsToolkit;
using SCAMobile.Constants;
using SCAMobile.Messaging;

namespace SCAMobile.Pages
{
    public partial class RecentAccounts : ContentPage
    {
        public RecentAccounts(IList<SCAService.Account> accounts)
        {
            InitializeComponent();

            ObservableCollection<AccountViewModel> accountViewModels = new ObservableCollection<AccountViewModel>(accounts.Select(m => new AccountViewModel
            {
                Account = m,
                IsNotLast = true
            }));

            if (accountViewModels.Count > 0)
                accountViewModels.Last().IsNotLast = false;

            accountList.ItemsSource = accountViewModels;

            MessagingService.Current.Subscribe<MessagingServiceComparisonDeleted>(MessageKeys.ComparisonDeleted, (service, msg) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    IList<AccountViewModel> accountsViewModels = accountList.ItemsSource as IList<AccountViewModel>;
                    accountsViewModels = accountsViewModels.Where(m => m.Account.Opportunities.Where(c => c.Id == msg.Id).Count() > 0).ToList();

                    foreach (AccountViewModel acc in accountsViewModels)
                    {
                        SCAService.Opportunity comp = acc.Account.Opportunities.Where(m => m.Id == msg.Id).FirstOrDefault();
                        if (comp != null)
                            acc.Account.Opportunities.Remove(comp);
                    }
                });
            });

            MessagingService.Current.Subscribe<MessagingServiceComparisonAdded>(MessageKeys.ComparisonAdded, (service, msg) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ObservableCollection<AccountViewModel> accountsViewModels = accountList.ItemsSource as ObservableCollection<AccountViewModel>;
                    AccountViewModel acc = accountsViewModels.Where(m => m.Account.Id == msg.Comparison.AccountId).FirstOrDefault();

                    if (acc != null)
                    {
                        SCAService.Opportunity comparison = acc.Account.Opportunities.Where(m => m.Id == msg.Comparison.Id).FirstOrDefault();

                        if (comparison == null)
                            acc.Account.Opportunities.Add(msg.Comparison);
                    }
                });
            });
        }

        private void accountListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            accountList.SelectedItem = null;
        }

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            App app = Application.Current as App;
            AccountViewModel accountViewModel = e.Item as AccountViewModel;

            app.AddAccountToRecentList(accountViewModel.Account.Id);
            Navigation.PushAsync(new SCAMobile.Pages.AccountSummary(accountViewModel.Account));
        }

        private void OnSelectClicked(object sender, EventArgs e)
        {
            App app = Application.Current as App;
            Button btn = sender as Button;
            SCAService.Account account = btn.CommandParameter as SCAService.Account;

            app.AddAccountToRecentList(account.Id);
            Navigation.PushAsync(new SCAMobile.Pages.AccountSummary(account));
        }
    }
}
