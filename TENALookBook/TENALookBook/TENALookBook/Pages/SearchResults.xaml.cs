﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace SCAMobile.Pages
{
    public class ProductSet
    {
        public SCAService.Product Product1 { get; set; }
        public SCAService.Product Product2 { get; set; }
        public SCAService.Product Product3 { get; set; }
        public SCAService.Product Product4 { get; set; }
        public SCAService.Product Product5 { get; set; }
        public SCAService.Product Product6 { get; set; }
        public SCAService.Product Product7 { get; set; }
        public SCAService.Product Product8 { get; set; }
    }

    public partial class SearchResults : ContentPage
    {
        private SCAService.Product _selectedProduct;
        private ObservableCollection<ProductSet> _products = new ObservableCollection<ProductSet>();

        public ObservableCollection<ProductSet> Products
        {
            get { return _products; }
        }

        public SearchResults(IList<SCAService.Product> products)
        {
            BindingContext = this;

            int cols = 2;
            int rows = 4;
            int numOfProductPages = products.Count() + (cols * rows - 1) / (cols * rows);
            int col = 0, row = 0;
            ProductSet productSet = new ProductSet();

            foreach (SCAService.Product p in products)
            {
                switch (col)
                {
                    case 0:
                        switch (row)
                        {
                            case 0:
                                productSet.Product1 = p;
                                break;

                            case 1:
                                productSet.Product3 = p;
                                break;

                            case 2:
                                productSet.Product5 = p;
                                break;

                            case 3:
                                productSet.Product7 = p;
                                break;
                        }
                        break;

                    case 1:
                        switch (row)
                        {
                            case 0:
                                productSet.Product2 = p;
                                break;

                            case 1:
                                productSet.Product4 = p;
                                break;

                            case 2:
                                productSet.Product6 = p;
                                break;

                            case 3:
                                productSet.Product8 = p;
                                break;
                        }
                        break;
                }

                if (++col == cols)
                {
                    col = 0;

                    if (++row == rows)
                    {
                        row = 0;
                        _products.Add(productSet);
                        productSet = new ProductSet();
                    }
                }
            }

            if (productSet.Product1 != null)
            {
                if (productSet.Product2 == null)
                    productSet.Product2 = new SCAService.Product();
                if (productSet.Product3 == null)
                    productSet.Product3 = new SCAService.Product();
                if (productSet.Product4 == null)
                    productSet.Product4 = new SCAService.Product();
                if (productSet.Product5 == null)
                    productSet.Product5 = new SCAService.Product();
                if (productSet.Product6 == null)
                    productSet.Product6 = new SCAService.Product();
                if (productSet.Product7 == null)
                    productSet.Product7 = new SCAService.Product();
                if (productSet.Product8 == null)
                    productSet.Product8 = new SCAService.Product();

                _products.Add(productSet);
            }

            InitializeComponent();

            Title = @"Competitor Product Results";
        }

        private void onProductSelected(object sender, Controls.ProductSelectedEventArgs e)
        {
            App app = Application.Current as App;
            SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

            _selectedProduct = e.Product;
            client.getProductAlternativesCompleted += getProductAlternativesCompleted;
            client.getProductAlternativesAsync(_selectedProduct.Id, app.LoginToken);

        }

        private void getProductAlternativesCompleted(object sender, SCAService.getProductAlternativesCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (e.Error == null)
                {
                    List<object> products = new List<object>(e.Result);
                    products.Add(1);

                    Navigation.PushAsync(new Pages.CompareProducts(_selectedProduct, products, @"Search"));
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }
    }
}
