﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using FormsToolkit;
using SCAMobile.Constants;

namespace SCAMobile.Pages
{
    public class UserViewModel : BindableObject
    {
        public static readonly BindableProperty FrameColorProperty =
            BindableProperty.Create("FrameColor", typeof(string), typeof(UserViewModel), null);

        public static readonly BindableProperty IsNotLastProperty =
            BindableProperty.Create("IsNotLast", typeof(bool), typeof(UserViewModel), false, BindingMode.TwoWay);

        public SCAService.User User { get; set; }

        public string FrameColor
        {
            get
            {
                return (string)GetValue(FrameColorProperty);
            }

            set
            {
                SetValue(FrameColorProperty, value);
            }
        }

        public bool IsNotLast
        {
            get
            {
                return (bool)GetValue(IsNotLastProperty);
            }

            set
            {
                SetValue(IsNotLastProperty, value);
            }
        }
    }

    public partial class UserSelector : ContentPage
    {
        private SCAService.Opportunity _comparison;
        private Command _resetCommand;
        private List<SCAService.User> _selectedUsers = new List<SCAService.User>();

        public UserSelector(SCAService.Opportunity comparison)
        {
            _comparison = comparison;
            BindingContext = this;

            InitializeComponent();
        }

        public Command ResetCommand
        {
            get
            {
                return _resetCommand ?? (_resetCommand = new Command(resetForm));
            }
        }

        private void userListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            userList.SelectedItem = null;
        }

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            UserViewModel model = e.Item as UserViewModel;

            if (model.FrameColor == @"#f9f7f4")
            {
                model.FrameColor = @"#85ba35";
                _selectedUsers.Add(model.User);
            }
            else
            {
                model.FrameColor = @"#f9f7f4";
                _selectedUsers.Remove(model.User);
            }
        }

        private void OnShareClicked(object sender, EventArgs e)
        {
            if (_selectedUsers.Count() == 0)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = @"Error",
                    Cancel = @"OK",
                    Message = @"Please select at least one user"
                });
            }
            else
            {
                App.ShowLoading(true, @"Sharing");

                ObservableCollection<int> recipients = new ObservableCollection<int>(_selectedUsers.Select(m => m.Id).ToList());
                App app = Application.Current as App;
                SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

                client.addNotificationCompleted += addNotificationCompleted;
                client.addNotificationAsync(recipients, _comparison.Id, app.LoginToken);
            }
        }

        private void addNotificationCompleted(object sender, SCAService.addNotificationCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                    Navigation.PopAsync();
                else
                    App.scaServiceUnavailableErrorMessage(e.Error);
            });
        }

        private void OnSearchClicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.searchTerm.Text))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
                {
                    Title = @"Error",
                    Cancel = @"OK",
                    Message = @"Please enter Name or Email"
                });
            }
            else
            {
                App.ShowLoading(true, @"Searching");
                _selectedUsers.Clear();

                App app = Application.Current as App;
                SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

                client.searchUsersCompleted += searchUsersCompleted;
                client.searchUsersAsync(searchTerm.Text, app.LoginToken);
            }
        }

        private void searchUsersCompleted(object sender, SCAService.searchUsersCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                {

                    ObservableCollection<UserViewModel> users = new ObservableCollection<UserViewModel>();

                    foreach (SCAService.User u in e.Result)
                    {
                        users.Add(new UserViewModel
                        {
                            User = u,
                            FrameColor = @"#f9f7f4",
                            IsNotLast = true
                        });
                    }

                    if (users.Count > 0)
                        users.Last().IsNotLast = false;

                    userList.ItemsSource = users;
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }
        private void resetForm()
        {
            searchTerm.Text = @"";
            userList.ItemsSource = null;
            _selectedUsers.Clear();
        }
    }
}
