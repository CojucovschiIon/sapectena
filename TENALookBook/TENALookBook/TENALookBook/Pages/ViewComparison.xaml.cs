﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using FFImageLoading.Forms;
using FormsToolkit;
using SCAMobile.Constants;
using SCAMobile.Messaging;

namespace SCAMobile.Pages
{
    public partial class ViewComparison : ContentPage
    {
        private struct TabDescription
        {
            public string image;
            public string selectedImage;
            public ContentView view;
            public Button button;
            public Label label;
        }

        private int _currentTab;
        private List<TabDescription> _tabs = new List<TabDescription>();
        private SCAService.Opportunity _comparison;
        private Command _manageCommand;
        private bool _fromNotification;

        public ViewComparison(SCAService.Opportunity comparison, bool fromNotification = false, string tab = @"Accounts", bool showTabBar = true)
        {
            Tab = tab;
            BindingContext = this;

            _comparison = comparison;
            _fromNotification = fromNotification;

            InitializeComponent();

            navBar.IsVisible = (fromNotification || !showTabBar) ? false : true;
            attachButtonContainer.IsVisible = fromNotification;
            if (fromNotification)
                ToolbarItems.Clear();

            _tabs.Add(new TabDescription
            {
                image = @"Description-Large.png",
                selectedImage = @"Description-Large-Selected.png",
                view = this.productDescriptionComparision,
                button = this.descriptionBtn,
                label = this.descriptionLbl
            });

            _tabs.Add(new TabDescription
            {
                image = @"Image-Large.png",
                selectedImage = @"Image-Large-Selected.png",
                view = this.productMediaComparision,
                button = this.mediaBtn,
                label = this.mediaLbl
            });

            _tabs.Add(new TabDescription
            {
                image = @"Performance-Large.png",
                selectedImage = @"Performance-Large-Selected.png",
                view = this.productPerformanceComparision,
                button = this.performanceBtn,
                label = this.performanceLbl
            });

            _tabs.Add(new TabDescription
            {
                image = @"Selling Points-Large.png",
                selectedImage = @"SellingPoints-Large-Selected.png",
                view = this.productSellingPointsComparision,
                button = this.sellingPointsBtn,
                label = this.sellingPointsLbl
            });

            this.notes.Text = _comparison.Notes;

            this.competitorProductName.Text = comparison.CompProduct.MarketDesc;
//            this.competitorProductName2.Text = comparison.CompProduct.MarketDesc;
            this.competitorProductName4.Text = comparison.CompProduct.MarketDesc;
            this.competitorCompany.Text = comparison.CompProduct.CompanyName;
            this.competitorCategory.Text = comparison.CompProduct.Category;
            this.competitorArticle.Text = comparison.CompProduct.ArticleNumber;
            this.competitorBagCount.Text = string.Format(@"{0}", comparison.CompProduct.BagCount);
            this.competitorCaseCount.Text = string.Format(@"{0}", comparison.CompProduct.CaseCount);
            this.competitorPiecesPerCase.Text = string.Format(@"{0}", comparison.CompProduct.PiecesPerCase);
            this.competitorProductSize.Text = comparison.CompProduct.ProductSizeDesc;
            this.competitorWaistSize.Text = comparison.CompProduct.WaistSize;
            this.compProductImage.Source = App.getProductImage(comparison.CompProduct);
            loadImages(comparison.CompProduct, this.competitorImageList, competitorNoImagesLabel);
            loadVideos(comparison.CompProduct, competitorVideoList, competitorNoVideosLabel);

            loadSCAProduct();
            loadPerformance(comparison.CompProduct, comparison.TenaProducts[0]);

            MessagingService.Current.Subscribe<MessagingServiceComparisonUpdated>(MessageKeys.ComparisonUpdated, (service, msg) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    if (_comparison.Id == msg.Id)
                    {
                        notes.Text = _comparison.Notes;
                        loadSCAProduct();
                    }
                });
            });
        }

        public string Tab { get; set; }

        public Command ManageCommand
        {
            get
            {
                return _manageCommand ?? (_manageCommand = new Command(manageComparison));
            }
        }

        protected override bool OnBackButtonPressed()
        {
            MessagingService.Current.Unsubscribe<MessagingServiceComparisonUpdated>(MessageKeys.ComparisonUpdated);

            return base.OnBackButtonPressed();
        }

        private void loadSCAProduct()
        {
            SCAService.Product scaProduct = _comparison.TenaProducts[0];

            this.scaCompany.Text = scaProduct.CompanyName;
            this.scaCategory.Text = scaProduct.Category;
            this.scaArticle.Text = scaProduct.ArticleNumber;
            this.scaBagCount.Text = string.Format(@"{0}", scaProduct.BagCount);
            this.scaCaseCount.Text = string.Format(@"{0}", scaProduct.CaseCount);
            this.scaPiecesPerCase.Text = string.Format(@"{0}", scaProduct.PiecesPerCase);
            this.scaProductSize.Text = scaProduct.ProductSizeDesc;
            this.scaWaistSize.Text = scaProduct.WaistSize;
            this.scaProductName.Text = scaProduct.MarketDesc;

            this.scaProductName2.Text = scaProduct.MarketDesc;

//            this.scaProductName3.Text = scaProduct.MarketDesc;

            this.scaProductName4.Text = scaProduct.MarketDesc;

            this.scaProductImage.Source = App.getProductImage(scaProduct);

            loadImages(scaProduct, scaImageList, scaNoImagesLabel);
            loadVideos(scaProduct, scaVideoList, scaNoVideosLabel);
            loadSellingPoints(scaProduct);

            Title = _comparison.CompProduct.MarketDesc + @" vs " + scaProduct.MarketDesc;
        }

        private void loadSellingPoints(SCAService.Product product)
        {
            App app = Application.Current as App;
            SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

            client.getProductSellingPointsCompleted += getProductSellingPointsCompleted;
            client.getProductSellingPointsAsync(product.Id, _comparison.CompProduct.Id, app.LoginToken, product);
        }

        private void getProductSellingPointsCompleted(object sender, SCAService.getProductSellingPointsCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                SCAService.Product product = e.UserState as SCAService.Product;

                if (e.Error == null && e.Result.Count() > 0)
                {
                    ObservableCollection<SellingPointViewModel> sellingPoints = new ObservableCollection<SellingPointViewModel>();

                    foreach (SCAService.SellingPoints sp in e.Result)
                    {
                        if (!string.IsNullOrWhiteSpace(sp.Point1))
                        {
                            sellingPoints.Add(new SellingPointViewModel
                            {
                                Description = sp.Point1
                            });
                        }

                        if (!string.IsNullOrWhiteSpace(sp.Point2))
                        {
                            sellingPoints.Add(new SellingPointViewModel
                            {
                                Description = sp.Point2
                            });
                        }

                        if (!string.IsNullOrWhiteSpace(sp.Point3))
                        {
                            sellingPoints.Add(new SellingPointViewModel
                            {
                                Description = sp.Point3
                            });
                        }

                        if (!string.IsNullOrWhiteSpace(sp.Point4))
                        {
                            sellingPoints.Add(new SellingPointViewModel
                            {
                                Description = sp.Point4
                            });
                        }

                        if (!string.IsNullOrWhiteSpace(sp.Point5))
                        {
                            sellingPoints.Add(new SellingPointViewModel
                            {
                                Description = sp.Point5
                            });
                        }
                    }

                    this.scaSellingPoints.ItemsSource = sellingPoints;
                }
                else if (product.Benefits.Count > 0)
                {
                    this.scaSellingPoints.ItemsSource =
                        new ObservableCollection<SellingPointViewModel>(
                            product.Benefits.Select(m => new SellingPointViewModel
                            {
                                Name = m.BenefitName,
                                Description = m.LongBenefit,
                                IsNameVisible = true
                            }));
                }
                else
                {
                    this.scaSellingPoints.ItemsSource = new ObservableCollection<SellingPointViewModel>
                    {
                        new SellingPointViewModel
                        {
                            Description = product.ProductDesc,
                        }
                    };
                }
            });
        }

        private void loadImages(SCAService.Product product, Grid grid, Label noImagesLabel)
        {
            var productImages = product.Images.Where(m => m.Type != @"VD");

            if (productImages.Count() > 0)
            {
                int cols = 4;
                int rows = productImages.Count() + (cols - 1) / cols;
                int col = 0, row = 0;

                grid.IsVisible = true;
                noImagesLabel.IsVisible = false;

                foreach (SCAService.ProductImage image in productImages)
                {
                    CachedImage cachedImage = new CachedImage
                    {
                        Source = image.ImageUrl,
                        HeightRequest = 80,
                        WidthRequest = 80,
                        HorizontalOptions = LayoutOptions.Center,
                        Aspect = Aspect.AspectFit
                    };

                    TapGestureRecognizer gesture = new TapGestureRecognizer();
                    gesture.Tapped += OnImageTapped;
                    cachedImage.GestureRecognizers.Add(gesture);

                    grid.Children.Add(cachedImage, col, row);

                    if (++col == cols)
                    {
                        row++;
                        col = 0;
                    }
                }
            }
            else
            {
                grid.IsVisible = false;
                noImagesLabel.IsVisible = true;
            }
        }

        private void loadVideos(SCAService.Product product, Grid grid, Label noVideosLabel)
        {
            App app = Application.Current as App;
            var productVideos = product.Images.Where(m => m.Type == @"VD").OrderBy(m => m.orderby);

            if (productVideos.Count() > 0)
            {
                int cols = 4;
                int rows = productVideos.Count() + (cols - 1) / cols;
                int col = 0, row = 0;

                grid.IsVisible = true;
                noVideosLabel.IsVisible = false;

                foreach (SCAService.ProductImage i in productVideos)
                {
                    string thumbNail = @"YouTube.png";
                    string videoId = App.getYouTubeVideoId(i.ImageUrl);

                    if (!string.IsNullOrWhiteSpace(videoId))
                        thumbNail = string.Format(@"https://img.youtube.com/vi/{0}/0.jpg", videoId);

                    CachedImage image = new CachedImage
                    {
                        Source = thumbNail,
                        HeightRequest = 80,
                        WidthRequest = 80,
                        HorizontalOptions = LayoutOptions.Center,
                        Aspect = Aspect.AspectFit,
                        LoadingPlaceholder = app.Login.SCACompanyId == product.CompanyId ? @"tena-product.png" : @"product.png"
                    };

                    TapGestureRecognizer gesture = new TapGestureRecognizer();
                    gesture.Tapped += OnVideoTapped;
                    image.GestureRecognizers.Add(gesture);

                    grid.Children.Add(image, col, row);

                    if (++col == cols)
                    {
                        row++;
                        col = 0;
                    }
                }
            }
            else
            {
                grid.IsVisible = false;
                noVideosLabel.IsVisible = true;
            }
        }

        private void OnImageTapped(object sender, EventArgs e)
        {
            CachedImage image = sender as CachedImage;
            List<string> productImages = new List<string>();
            SCAService.Product product;
            int imageIndex;

            if (image.Parent == competitorImageList)
            {
                product = _comparison.CompProduct;
                imageIndex = competitorImageList.Children.IndexOf(image);
            }
            else
            {
                product = _comparison.TenaProducts[0];
                imageIndex = scaImageList.Children.IndexOf(image);
            }

            foreach (SCAService.ProductImage i in product.Images.Where(m => m.Type != @"VD").OrderBy(m => m.orderby))
                productImages.Add(i.ImageUrl);

            if (image.Source is UriImageSource)
                Navigation.PushAsync(new ImageViewer(productImages, imageIndex, _fromNotification ? @"Notifications" : @"Accounts", product.MarketDesc + " images", !_fromNotification));
        }

        private void OnVideoTapped(object sender, EventArgs e)
        {
            CachedImage image = sender as CachedImage;
            SCAService.Product product;
            int videoIndex;
            IList<SCAService.ProductImage> productVideos;

            if (image.Parent == competitorVideoList)
            {
                product = _comparison.CompProduct;
                videoIndex = competitorVideoList.Children.IndexOf(image);
            }
            else
            {
                product = _comparison.TenaProducts[0];
                videoIndex = scaVideoList.Children.IndexOf(image);
            }

            productVideos = product.Images.Where(m => m.Type == @"VD").OrderBy(m => m.orderby).ToList();

            Navigation.PushAsync(new VideoViewer(product, productVideos[videoIndex].ImageUrl, _fromNotification ? @"Notifications" : @"Accounts", !_fromNotification));
//            Device.OpenUri(new Uri(productVideos[videoIndex].ImageUrl));
        }

        private void OnDescriptionClicked(object sender, EventArgs e)
        {
            if (_currentTab != 0)
                showTab(0);
        }

        private void OnMediaClicked(object sender, EventArgs e)
        {
            if (_currentTab != 1)
                showTab(1);
        }

        private void OnPerformanceClicked(object sender, EventArgs e)
        {
            if (_currentTab != 2)
                showTab(2);
        }

        private void OnSellingPointsClicked(object sender, EventArgs e)
        {
            if (_currentTab != 3)
                showTab(3);
        }

        private void showTab(int tabIndex)
        {
            _currentTab = tabIndex;

            for (int i = 0; i < _tabs.Count(); i++)
            {
                TabDescription tab = _tabs[i];

                if (i == tabIndex)
                {
                    if (!tab.view.IsVisible)
                    {
                        tab.view.IsVisible = true;
                        tab.button.Image = tab.selectedImage;
                        tab.label.TextColor = Color.FromHex(@"85ba35");
                    }
                }
                else
                {
                    if (tab.view.IsVisible)
                    {
                        tab.view.IsVisible = false;
                        tab.button.Image = tab.image;
                        tab.label.TextColor = Color.FromHex(@"a2a2a0");
                    }
                }
            }
        }

        async private void manageComparison()
        {
            string action = await DisplayActionSheet(null, @"Cancel", @"Delete", @"Edit", @"Share");

            if (action == @"Delete")
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    App.ShowLoading(true, @"Deleting");
                });

                App app = Application.Current as App;
                SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

                client.deleteOpportunityCompleted += deleteOpportunityCompleted;
                client.deleteOpportunityAsync(_comparison.Id, app.LoginToken);
            }
            else if (action == @"Edit")
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new EditComparison(_comparison));
                });
            }
            else if (action == @"Share")
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new UserSelector(_comparison));
                });
            }
        }

        private void deleteOpportunityCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.ShowLoading(false);

                if (e.Error == null)
                {
                    App app = Application.Current as App;

                    app.RemoveComparisonFromRecentList(_comparison.Id);

                    MessagingService.Current.SendMessage(MessageKeys.ComparisonDeleted, new MessagingServiceComparisonDeleted { Id = _comparison.Id });

                    Navigation.PopAsync();
                }
                else
                {
                    App.scaServiceUnavailableErrorMessage(e.Error);
                }
            });
        }

        private void OnAttachClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new SaveComparision(_comparison));
        }

        private void loadPerformance(SCAService.Product compProduct, SCAService.Product scaProduct)
        {
            ObservableCollection<PerformanceMetricViewModel> performance = new ObservableCollection<PerformanceMetricViewModel>();
            string compValue;
            string scaValue;

            compValue = compProduct.Absorption == null ? @"" : compProduct.Absorption.ToString();
            scaValue = scaProduct == null ? @"" : (scaProduct.Absorption == null ? @"" : scaProduct.Absorption.ToString());
            addPerformanceMetric(@"Rothwell", ref compValue, ref scaValue, performance);

            TypeInfo productTypeInfo = typeof(SCAService.Product).GetTypeInfo();

            foreach (PerformanceMetric pm in Globals.PerformanceMetrics)
            {
                PropertyInfo propInfo = productTypeInfo.GetDeclaredProperty(pm.PropName);

                if (propInfo != null)
                {
                    compValue = propInfo.GetValue(compProduct) as string;
                    scaValue = scaProduct == null ? @"" : propInfo.GetValue(scaProduct) as string;
                    addPerformanceMetric(pm.DisplayName, ref compValue, ref scaValue, performance);
                }
            }

            this.productPerformanceList.ItemsSource = performance;
            this.productPerformanceList.Header = new ProductNameViewModel
            {
                CompName = compProduct.MarketDesc,
                ScaName = scaProduct == null ? @"" : scaProduct.MarketDesc
            };

            /*            {
                            int row = productPerformanceList.RowDefinitions.Count();

                            productPerformanceList.RowDefinitions.Add(new RowDefinition
                            {
                                Height = GridLength.Star
                            });

                            productPerformanceList.Children.Add(new ContentView
                            {
                                Content = new Label
                                {
                                    HorizontalTextAlignment = TextAlignment.Center
                                },
                                Padding = 10,
                                BackgroundColor = Color.FromHex(@"f9f6f2")
                            }, 0, row);

                            productPerformanceList.Children.Add(new ContentView
                            {
                                Content = new Label
                                {
                                    HorizontalTextAlignment = TextAlignment.Center,
                                    TextColor = Color.FromHex(@"006aa9")
                                },
                                Padding = 0,
                                BackgroundColor = Color.FromHex(@"f9f6f2")
                            }, 1, row);

                            productPerformanceList.Children.Add(new ContentView
                            {
                                Content = new Label
                                {
                                    HorizontalTextAlignment = TextAlignment.Center
                                },
                                Padding = 10,
                                BackgroundColor = Color.FromHex(@"f9f6f2")
                            }, 2, row);
                        }
            */
        }

        private void addPerformanceMetric(string name, ref string compValue, ref string scaValue, ObservableCollection<PerformanceMetricViewModel> performanceList)
        {
            if (!string.IsNullOrWhiteSpace(compValue) || !string.IsNullOrWhiteSpace(scaValue))
            {
                performanceList.Add(new PerformanceMetricViewModel
                {
                    Name = name,
                    CompValue = compValue,
                    ScaValue = scaValue,
                });
            }
        }

        /*
                private void loadPerformance(SCAService.Product compProduct, SCAService.Product scaProduct)
                {
                    string compValue;
                    string scaValue;

                    while (productPerformanceList.RowDefinitions.Count() > 1)
                    {
                        productPerformanceList.Children.RemoveAt(3);
                        productPerformanceList.Children.RemoveAt(3);
                        productPerformanceList.Children.RemoveAt(3);
                        productPerformanceList.RowDefinitions.RemoveAt(1);
                    }

                    compValue = compProduct.Absorption == null ? @"" : compProduct.Absorption.ToString();
                    scaValue = scaProduct == null ? @"" : (scaProduct.Absorption == null ? @"" : scaProduct.Absorption.ToString());
                    addPerformanceMetric(@"Rothwell", ref compValue, ref scaValue);

                    TypeInfo productTypeInfo = typeof(SCAService.Product).GetTypeInfo();

                    foreach (PerformanceMetric pm in Globals.PerformanceMetrics)
                    {
                        PropertyInfo propInfo = productTypeInfo.GetDeclaredProperty(pm.PropName);

                        if (propInfo != null)
                        {
                            compValue = propInfo.GetValue(compProduct) as string;
                            scaValue = scaProduct == null ? @"" : propInfo.GetValue(scaProduct) as string;
                            addPerformanceMetric(pm.DisplayName, ref compValue, ref scaValue);
                        }
                    }

        //            if (productPerformanceList.RowDefinitions.Count == 1)
                    {
                        int row = productPerformanceList.RowDefinitions.Count();

                        productPerformanceList.RowDefinitions.Add(new RowDefinition
                        {
                            Height = GridLength.Star
                        });

                        productPerformanceList.Children.Add(new ContentView
                        {
                            Content = new Label
                            {
                                HorizontalTextAlignment = TextAlignment.Center
                            },
                            Padding = 10,
                            BackgroundColor = Color.FromHex(@"f9f6f2")
                        }, 0, row);

                        productPerformanceList.Children.Add(new ContentView
                        {
                            Content = new Label
                            {
                                HorizontalTextAlignment = TextAlignment.Center,
                                TextColor = Color.FromHex(@"006aa9")
                            },
                            Padding = 0,
                            BackgroundColor = Color.FromHex(@"f9f6f2")
                        }, 1, row);

                        productPerformanceList.Children.Add(new ContentView
                        {
                            Content = new Label
                            {
                                HorizontalTextAlignment = TextAlignment.Center
                            },
                            Padding = 10,
                            BackgroundColor = Color.FromHex(@"f9f6f2")
                        }, 2, row);
                    }
                }

                private void addPerformanceMetric(string name, ref string compValue, ref string scaValue)
                {
                    if (!string.IsNullOrWhiteSpace(compValue) || !string.IsNullOrWhiteSpace(scaValue))
                    {
                        int row = productPerformanceList.RowDefinitions.Count();

                        productPerformanceList.RowDefinitions.Add(new RowDefinition
                        {
                            Height = GridLength.Auto
                        });

                        StackLayout sl = new StackLayout
                        {
                            Padding = 10,
                            BackgroundColor = Color.FromHex(@"f9f6f2"),
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            Orientation = StackOrientation.Vertical
                        };

                        sl.Children.Add(new Label
                        {
                            Text = compValue,
                            HorizontalTextAlignment = TextAlignment.Center,
                            VerticalOptions = LayoutOptions.CenterAndExpand,
                            LineBreakMode = LineBreakMode.WordWrap,
                        });

                        productPerformanceList.Children.Add(sl, 0, row);

                        sl = new StackLayout
                        {
                            Padding = 10,
                            BackgroundColor = Color.FromHex(@"f9f6f2"),
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            Orientation = StackOrientation.Vertical
                        };

                        sl.Children.Add(new Label
                        {
                            Text = name,
                            HorizontalTextAlignment = TextAlignment.Center,
                            VerticalOptions = LayoutOptions.CenterAndExpand,
                            TextColor = Color.FromHex(@"006aa9"),
                            LineBreakMode = LineBreakMode.WordWrap,
                        });

                        productPerformanceList.Children.Add(sl, 1, row);

                        sl = new StackLayout
                        {
                            Padding = 10,
                            BackgroundColor = Color.FromHex(@"f9f6f2"),
                            VerticalOptions = LayoutOptions.FillAndExpand,
                            Orientation = StackOrientation.Vertical
                        };

                        sl.Children.Add(new Label
                        {
                            Text = scaValue,
                            HorizontalTextAlignment = TextAlignment.Center,
                            VerticalOptions = LayoutOptions.CenterAndExpand,
                            LineBreakMode = LineBreakMode.WordWrap,
                        });

                        productPerformanceList.Children.Add(sl, 2, row);
                    }
                }
        */

        private void scaSellingPointSelected(object sender, SelectedItemChangedEventArgs e)
        {
            scaSellingPoints.SelectedItem = null;
        }
    }
}
