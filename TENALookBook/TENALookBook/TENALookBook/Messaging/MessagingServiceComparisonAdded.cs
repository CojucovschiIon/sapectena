﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCAMobile.Messaging
{
    public class MessagingServiceComparisonAdded
    {
        public SCAService.Opportunity Comparison { get; set; }
    }
}
