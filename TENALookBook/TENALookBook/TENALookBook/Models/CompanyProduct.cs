﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCAMobile.Models
{
    public class CompanyProduct
    {
        public string Article { get; set; }
        public string Size { get; set; }
        public int? BagCount { get; set; }
        public int? Pieces { get; set; }
        public string BackgroundColor { get; set; }
        public string Category { get; set; }
    }
    public class Companies
    {
        public List<CompanyProduct> companyProducts { get; set; }
        public string CompanyName { get; set; }
        public int CompanyId { get; set; }
    }
    public class CategoryViewModel
    {
        public string Category { get; set; }
        public bool IsSelected { get; set; }
    }
}
