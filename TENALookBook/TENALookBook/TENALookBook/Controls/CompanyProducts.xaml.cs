﻿using SCAMobile.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SCAMobile.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CompanyProducts : ContentView
    {

        public SCAEmlService.Companies companies;
        public List<SCAEmlService.CompanyProduct> listCompanyProdcts;

        public CompanyProducts(SCAEmlService.Companies company)
        {
            InitializeComponent();

            listCompanyProdcts = company.companyProducts.ToList();
            companyName.Text = company.CompanyName;
            companyProducts.ItemsSource = listCompanyProdcts;
        }

        private void OnDeleteClicked(object sender, EventArgs e)
        {
            //fullContent.IsVisible = false;
            fullContent.Children.Clear();
        }

    }
}