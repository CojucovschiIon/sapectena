﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SCAMobile.Controls
{
    public class CategoryButton : Grid
    {
        private static string checkedButtonImage = @"Selected Product BG.png";
        private static string uncheckedButtonImage = @"De Selected Product BG.png";

        private Label _titleLabel;
        private Button _btn;
        private bool _checked;

        public int CategoryId { get; set; }

        public bool Checked
        {
            get
            {
                return _checked;
            }

            set
            {
                if (_checked != value)
                {
                    _checked = value;
                    if (_checked)
                    {
                        _btn.Image = checkedButtonImage;
                        _titleLabel.TextColor = Color.White;
                    }
                    else
                    {
                        _btn.Image = uncheckedButtonImage;
                        _titleLabel.TextColor = Color.Black;
                    }
                }
            }
        }

        public string Title {
            get
            {
                return _titleLabel.Text;
            }

            set
            {
                _titleLabel.Text = value;
            }
        }

        public CategoryButton()
        {
            RowDefinitions = new RowDefinitionCollection
            {
                new RowDefinition { Height = GridLength.Auto }
            };

            _btn = new Button
            {
                Image = @"De Selected Product BG.png",
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                BackgroundColor = Color.Transparent,
            };
            _btn.Clicked += btnClicked;

            _titleLabel = new Label
            {
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                FontSize = 14,
                WidthRequest = 60,
                HorizontalTextAlignment = TextAlignment.Center,
                InputTransparent = true
            };

            Children.Add(_btn, 0, 0);
            Children.Add(_titleLabel, 0, 0);
        }

        private void btnClicked(object sender, EventArgs e)
        {
            if (!Checked)
            {
                _btn.Image = checkedButtonImage;
                _titleLabel.TextColor = Color.White;
            }
            else
            {
                _btn.Image = uncheckedButtonImage;
                _titleLabel.TextColor = Color.Black;
            }
            Checked = !Checked;
        }
    }
}
