﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using FFImageLoading.Transformations;
using FFImageLoading.Forms;

namespace SCAMobile.Controls
{
    public class ProductSelectedEventArgs : EventArgs
    {
        public SCAService.Product Product { get; set; }
    }

    public partial class ProductView : Frame
    {
        public static readonly BindableProperty ProductProperty =
            BindableProperty.Create("Product", typeof(SCAService.Product), typeof(ProductView), null);

        public delegate void ProductSelectedHandler(object sender, ProductSelectedEventArgs e);
        public event ProductSelectedHandler OnProductSelected;

        public SCAService.Product Product
        {
            get
            {
                return GetValue(ProductProperty) as SCAService.Product;
            }

            set
            {
                SetValue(ProductProperty, value);
            }
        }

        public ProductView()
        {
            InitializeComponent();

//            productImage.Transformations.Add(new RoundedTransformation(20));
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (ProductProperty.PropertyName == propertyName)
            {
                SCAService.Product product = this.Product;
                if (product.Id == 0)
                {
                    this.IsVisible = false;
                }
                else
                {
                    int infoCount = 1;

                    this.IsVisible = true;
                    this.productName.Text = product.MarketDesc;
                    this.productArticle.Text = product.ArticleNumber;

/*
                    if (product.Absorption != null ||
                        !string.IsNullOrWhiteSpace(product.ProductWeight) ||
                        !string.IsNullOrWhiteSpace(product.ProductThickness) ||
                        !string.IsNullOrWhiteSpace(product.Acq1) ||
                        !string.IsNullOrWhiteSpace(product.Acq2) ||
                        !string.IsNullOrWhiteSpace(product.Acq3) ||
                        !string.IsNullOrWhiteSpace(product.Acq12) ||
                        !string.IsNullOrWhiteSpace(product.Acq22) ||
                        !string.IsNullOrWhiteSpace(product.Acq32) ||
                        !string.IsNullOrWhiteSpace(product.Rewet) ||
                        !string.IsNullOrWhiteSpace(product.PackageLength) ||
                        !string.IsNullOrWhiteSpace(product.PackageWidth) ||
                        !string.IsNullOrWhiteSpace(product.PackageHeight) ||
                        !string.IsNullOrWhiteSpace(product.TotalProductLength) ||
                        !string.IsNullOrWhiteSpace(product.TotalProductWidthFront) ||
                        !string.IsNullOrWhiteSpace(product.TotalProductWidthCrotch) ||
                        !string.IsNullOrWhiteSpace(product.TotalProductWidthRear) ||
                        !string.IsNullOrWhiteSpace(product.OnePieceStretchWing) ||
                        !string.IsNullOrWhiteSpace(product.SmallCoreLength) ||
                        !string.IsNullOrWhiteSpace(product.SmallCoreWidth) ||
                        !string.IsNullOrWhiteSpace(product.BigCoreLength) ||
                        !string.IsNullOrWhiteSpace(product.BigCoreWidthCrotch) ||
                        !string.IsNullOrWhiteSpace(product.BigCoreWidthEnds) ||
                        !string.IsNullOrWhiteSpace(product.TransferLayer) ||
                        !string.IsNullOrWhiteSpace(product.Tll) ||
                        !string.IsNullOrWhiteSpace(product.Tlw) ||
                        !string.IsNullOrWhiteSpace(product.TransferLayerColorMaterial) ||
                        !string.IsNullOrWhiteSpace(product.Coverstock) ||
                        !string.IsNullOrWhiteSpace(product.BackingMaterial) ||
                        !string.IsNullOrWhiteSpace(product.CompressionPattern) ||
                        !string.IsNullOrWhiteSpace(product.StandingGathersMaterial) ||
                        !string.IsNullOrWhiteSpace(product.StandingGathersElastics) ||
                        !string.IsNullOrWhiteSpace(product.LegElasticsFront) ||
                        !string.IsNullOrWhiteSpace(product.LegElasticsBack) ||
                        !string.IsNullOrWhiteSpace(product.Reinforcement) ||
                        !string.IsNullOrWhiteSpace(product.WaistElasticsFront) ||
                        !string.IsNullOrWhiteSpace(product.WaistElasticsBack) ||
                        !string.IsNullOrWhiteSpace(product.WetnessIndicator) ||
                        !string.IsNullOrWhiteSpace(product.SizePrinted) ||
                        !string.IsNullOrWhiteSpace(product.AbsorbencyPrinted) ||
                        !string.IsNullOrWhiteSpace(product.TapeHook) ||
                        !string.IsNullOrWhiteSpace(product.BeltSidePanels))
*/
                    if (product.HasPerformanceMetrics)
                    {
                        infoCount++;
                        this.performanceIcon.Source = @"Performance - Selected.png";
                    }
                    else
                    {
                        this.performanceIcon.Source = @"Performance.png";
                    }

                    if (product.Benefits.Count() == 0 && !product.HasSellingPoints)
                    {
                        this.sellingPointsIcon.Source = @"SellingPoints-small.png";
                    }
                    else
                    {
                        infoCount++;
                        this.sellingPointsIcon.Source = @"SellingPoints-small - Selected.png";
                    }

                    this.informationLabel.Text = string.Format(@"Information ({0}/3)", infoCount);

                    this.productImage.Source = App.getProductImage(product);
                }
            }
        }

        private void OnSelectClicked(object sender, EventArgs e)
        {
            if (OnProductSelected != null)
            {
                ProductSelectedEventArgs e2 = new ProductSelectedEventArgs
                {
                    Product = this.Product
                };
                OnProductSelected(this, e2);
            }
        }
    }
}
