﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using SCAMobile.Pages;
using System.Runtime.CompilerServices;
using SCAMobile.Constants;
using FormsToolkit;

namespace SCAMobile.Controls
{
    public class NavTabChangedEventArgs : EventArgs
    {
        public string NewTab { get; set; }
    }

    public delegate void NavTabChangedEventHandler(object sender, NavTabChangedEventArgs e);

    public partial class NavigationTabBar : Grid
    {
        public static readonly BindableProperty ActiveTabProperty =
            BindableProperty.Create("ActiveTab", typeof(string), typeof(NavigationTabBar), null);

        public string ActiveTab
        {
            get
            {
                return (string)GetValue(ActiveTabProperty);
            }

            set
            {
                SetValue(ActiveTabProperty, value);
            }
        }

        public static event NavTabChangedEventHandler NavTabChanged;

        public NavigationTabBar()
        {
            InitializeComponent();

            App app = App.Current as App;

            notificationBadge.IsVisible = app.HasNewNotifications;

            MessagingService.Current.Subscribe<object>(MessageKeys.NotificationReceived, (service, info) => {
                Device.BeginInvokeOnMainThread(new Action(() =>
                {
                    notificationBadge.IsVisible = true;
                }));
            });

            MessagingService.Current.Subscribe<object>(MessageKeys.ResetNotificationBadge, (service, info) => {
                Device.BeginInvokeOnMainThread(new Action(() =>
                {
                    notificationBadge.IsVisible = false;
                }));
            });

        }

        void OnSearchClicked(object sender, EventArgs e)
        {
            App app = Application.Current as App;

            if (ActiveTab.Equals(@"Search", StringComparison.OrdinalIgnoreCase))
                return;

            app.MainPage = app.TabBar.getPage(MainUITabBar.PAGE_INDEX_SEARCH);
            fireEvent(@"Search");
        }

        void OnCatalogueClicked(object sender, EventArgs e)
        {
            App app = Application.Current as App;

            if (ActiveTab.Equals(@"Catalogue", StringComparison.OrdinalIgnoreCase))
                return;

            app.MainPage = app.TabBar.getPage(MainUITabBar.PAGE_INDEX_CATALOGUE);
            fireEvent(@"Catalogue");
        }

        void OnAccountsClicked(object sender, EventArgs e)
        {
            App app = Application.Current as App;

            if (ActiveTab.Equals(@"Accounts", StringComparison.OrdinalIgnoreCase))
                return;

            app.MainPage = app.TabBar.getPage(MainUITabBar.PAGE_INDEX_ACCOUNTS);
            fireEvent(@"Accounts");
        }

        void OnNotificationsClicked(object sender, EventArgs e)
        {
            App app = Application.Current as App;

            if (ActiveTab.Equals(@"Notifications", StringComparison.OrdinalIgnoreCase))
                return;

            app.MainPage = app.TabBar.getPage(MainUITabBar.PAGE_INDEX_NOTIFICATIONS);
            fireEvent(@"Notifications");
        }

        void OnSettingsClicked(object sender, EventArgs e)
        {
            App app = Application.Current as App;

            if (ActiveTab.Equals(@"Settings", StringComparison.OrdinalIgnoreCase))
                return;

            app.MainPage = app.TabBar.getPage(MainUITabBar.PAGE_INDEX_SETTINGS);
            fireEvent(@"Settings");
        }
        void OnCompareClicked(object sender, EventArgs e)
        {
            App app = Application.Current as App;

            if (ActiveTab.Equals(@"Compare", StringComparison.OrdinalIgnoreCase))
                return;

            app.MainPage = app.TabBar.getPage(MainUITabBar.PAGE_INDEX_COMPARE);
            fireEvent(@"Compare");
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (ActiveTabProperty.PropertyName == propertyName)
            {
                string activeTab = ActiveTab;

                if (activeTab.Equals(@"Search", StringComparison.OrdinalIgnoreCase))
                {
                    searchButton.Image = @"Search - Selected.png";
                    searchLabel.TextColor = Color.FromHex(@"004f86");
                }
                else if (activeTab.Equals(@"Catalogue", StringComparison.OrdinalIgnoreCase))
                {
                    catalogueButton.Image = @"Catalogue - Selected.png";
                    catalogueLabel.TextColor = Color.FromHex(@"004f86");
                }
                else if (activeTab.Equals(@"Accounts", StringComparison.OrdinalIgnoreCase))
                {
                    accountsButton.Image = @"Accounts - Selected.png";
                    accountsLabel.TextColor = Color.FromHex(@"004f86");
                }
                else if (activeTab.Equals(@"Notifications", StringComparison.OrdinalIgnoreCase))
                {
                    notificationsButton.Image = @"Bell - Selected.png";
                    notificationsLabel.TextColor = Color.FromHex(@"004f86");
                }
                else if (activeTab.Equals(@"Settings", StringComparison.OrdinalIgnoreCase))
                {
                    settingsButton.Image = @"Gear - Selected.png";
                    settingsLabel.TextColor = Color.FromHex(@"004f86");
                }
                else if (activeTab.Equals(@"Compare", StringComparison.OrdinalIgnoreCase))
                {
                    compareButton.Image = @"side_by_side Selected.png";
                    compareLabel.TextColor = Color.FromHex(@"004f86");
                }

                fireEvent(activeTab);
            }
        }

        private void fireEvent(string newTab)
        {
            if (NavTabChanged != null)
            {
                NavTabChanged.Invoke(this, new NavTabChangedEventArgs
                {
                    NewTab = newTab
                });
            }
        }
    }
}
