﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using FFImageLoading;
using FFImageLoading.Forms;

namespace SCAMobile.Controls
{
    public class CompanyButton : Grid
    {
        private static string imgBaseUrl = @"https://www.tenaportraits.com/mobile_app_services/photo_bank/company_logos/";
        private static Color uncheckedTitleColor = Color.FromHex(@"#a2a2a0");
        private static Color checkedTitleColor = Color.Black;

//        private Label _titleLabel;
//        private Button _btn;
        private CachedImage _img;
        private string _companyName;
        private string _companyLogo;
        private string _companyLogoChecked;
        private bool _checked;

        public int CompanyId { get; set; }

        public string CompanyName
        {
            get
            {
                return _companyName;
            }

            set
            {
                _companyName = value;
//                _titleLabel.Text = value;
            }
        }

        public string CompanyLogo
        {
            get
            {
                return _companyLogo;
            }

            set
            {
                int dotPos = value.IndexOf('.');

                if (dotPos > -1)
                {
                    _companyLogoChecked = imgBaseUrl + value.Substring(0, dotPos) + @"-Color@2x." + value.Substring(dotPos + 1);
                    _companyLogo = imgBaseUrl + value.Substring(0, dotPos) + @"@2x." + value.Substring(dotPos + 1);
                }
                else
                {
                    _companyLogoChecked = imgBaseUrl + value;
                    _companyLogo = imgBaseUrl + value;
                }

//                ImageService.Instance.LoadUrl(_companyLogoChecked).FadeAnimation(false).Preload();
//                ImageService.Instance.LoadUrl(_companyLogo).FadeAnimation(false).Preload();//.Success(() => { }); ;


                if (Checked)
                {
                    //                    _btn.Image = _companyLogoChecked;
                    _img.Source = _companyLogoChecked;
                }
                else
                {
                    _img.Source = _companyLogo;
//                    _btn.Image = value;
                }

            }
        }

        public bool Checked
        {
            get
            {
                return _checked;
            }

            set
            {
                if (_checked != value)
                {
                    _checked = value;
                    if (_checked)
                    {
//                        _btn.Image = _companyLogoChecked;
                        _img.Source = _companyLogoChecked;
                        //                        _titleLabel.TextColor = checkedTitleColor;
                    }
                    else
                    {
//                        _btn.Image = _companyLogo;
                        _img.Source = _companyLogo;
                        //                        _titleLabel.TextColor = uncheckedTitleColor;
                    }
                }
            }
        }

        public CompanyButton()
        {
            RowDefinitions = new RowDefinitionCollection
            {
                new RowDefinition { Height = GridLength.Auto },
//                new RowDefinition { Height = GridLength.Auto }
            };

            ColumnDefinitions = new ColumnDefinitionCollection
            {
                new ColumnDefinition { Width = GridLength.Star }
            };

            _img = new CachedImage
            {
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                BackgroundColor = Color.Transparent,
                WidthRequest = 150,
                InputTransparent = false
            };

            TapGestureRecognizer gesture = new TapGestureRecognizer();
            gesture.Tapped += btnClicked;
            _img.GestureRecognizers.Add(gesture);

/*
            _btn = new Button
            {
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                BackgroundColor = Color.Transparent,
                WidthRequest = 150
            };
            _btn.Clicked += btnClicked;
*/

  /*          _titleLabel = new Label
            {
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                FontSize = 14,
                HorizontalTextAlignment = TextAlignment.Center,
                InputTransparent = false,
                TextColor = uncheckedTitleColor
            };
*/

//            TapGestureRecognizer gesture = new TapGestureRecognizer();
//            gesture.Tapped += btnClicked;
//            _titleLabel.GestureRecognizers.Add(gesture);

//            Children.Add(_btn, 0, 0);
            Children.Add(_img, 0, 0);
            //            Children.Add(_titleLabel, 0, 1);
        }

        private void btnClicked(object sender, EventArgs e)
        {
            if (!Checked)
            {
//                _btn.Image = _companyLogoChecked;
                _img.Source = _companyLogoChecked;
                //                _titleLabel.TextColor = checkedTitleColor;
            }
            else
            {
//                _btn.Image = _companyLogo;
                _img.Source = _companyLogo;
                //                _titleLabel.TextColor = uncheckedTitleColor;
            }
            Checked = !Checked;
        }
    }
}
