﻿using SCAMobile.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SCAMobile.Controls
{
    public class CategoryViewSelectedEventArgs : EventArgs
    {
        public CategoryViewModel CategoryName { get; set; }
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CategoryViewFilter : ContentView
    {

        public static readonly BindableProperty CategoryName =
            BindableProperty.Create("Category", typeof(CategoryViewModel), typeof(CategoryViewFilter), null);

        public delegate void CategoryViewSelectedHandler(object sender, CategoryViewSelectedEventArgs e);
        public event CategoryViewSelectedHandler OnCategorySelected;

        public CategoryViewModel Category
        {
            get
            {
                return GetValue(CategoryName) as CategoryViewModel;
            }

            set
            {
                SetValue(CategoryName, value);
            }
        }

        public CategoryViewFilter(CategoryViewModel category)
        {
            InitializeComponent();
            categoryLabel.Text = category.Category;
            Category = category;
        }

        private void OnSelectClicked(object sender, EventArgs e)
        {
            if (OnCategorySelected != null)
            {
                ///categoryContent.BackgroundColor = Color.FromHex("#dedede");
                CategoryViewSelectedEventArgs e2 = new CategoryViewSelectedEventArgs
                {
                    CategoryName = this.Category
                };

                if (this.Category.IsSelected)
                {
                    buttonSelect.BackgroundColor = Color.FromHex("#ffffff");
                    buttonSelect.Text = "Select";
                }
                else {
                    buttonSelect.BackgroundColor = Color.FromHex("#cdcdcd");
                    buttonSelect.Text = "Unselect";
                }

                OnCategorySelected(this, e2);
            }
        }
    }
}