﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SCAMobile.Controls
{
    public class ToggleButton : Grid
    {
        public static readonly BindableProperty CheckedProperty =
            BindableProperty.Create("Checked", typeof(bool), typeof(ToggleButton), false, BindingMode.TwoWay);

        private Color _uncheckedTitleColor = Color.FromHex(@"#a2a2a0");
        private Color _checkedTitleColor = Color.Black;
        private Label _titleLabel;
        private Button _btn;
        private string _uncheckedImage;
        private string _checkedImage;
//        private bool _checked;
        private string _titleLocation = @"center";
        private string _uncheckedTitle;
        private string _checkedTitle;

        public event EventHandler Clicked;

        public string Title
        {
            get
            {
                return _titleLabel.Text;
            }

            set
            {
                _titleLabel.Text = value;
            }
        }

        public string UncheckedTitle
        {
            get
            {
                return _uncheckedTitle;
            }

            set
            {
                _uncheckedTitle = value;

                if (!Checked)
                    _titleLabel.Text = value;
            }
        }

        public string CheckedTitle
        {
            get
            {
                return _checkedTitle;
            }

            set
            {
                _checkedTitle = value;

                if (Checked)
                    _titleLabel.Text = value;
            }
        }

        public string TitleLocation
        {
            get
            {
                return _titleLocation;
            }

            set
            {
                if (!_titleLocation.Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    if (value.Equals(@"center", StringComparison.OrdinalIgnoreCase))
                    {
                        Children.Remove(_titleLabel);

                        RowDefinitions = new RowDefinitionCollection
                        {
                            new RowDefinition { Height = GridLength.Auto },
                        };

                        Children.Add(_titleLabel, 0, 0);
                        _titleLocation = value;
                        _titleLabel.VerticalOptions = LayoutOptions.Center;
                    }
                    else if (value.Equals(@"under", StringComparison.OrdinalIgnoreCase))
                    {
                        Children.Remove(_titleLabel);

                        RowDefinitions = new RowDefinitionCollection
                        {
                            new RowDefinition { Height = GridLength.Auto },
                            new RowDefinition { Height = GridLength.Star },
                        };

                        Children.Add(_titleLabel, 0, 1);
                        _titleLocation = value;
                        _titleLabel.VerticalOptions = LayoutOptions.End;
                    }
                }
            }
        }

        public double FontSize
        {
            get
            {
                return _titleLabel.FontSize;
            }

            set
            {
                _titleLabel.FontSize = value;
            }
        }

        public FontAttributes FontAttributes
        {
            get
            {
                return _titleLabel.FontAttributes;
            }

            set
            {
                _titleLabel.FontAttributes = value;
            }
        }

        public Color CheckedTitleColor
        {
            get
            {
                return _checkedTitleColor;
            }

            set
            {
                _checkedTitleColor = value;
                if (Checked)
                    _titleLabel.TextColor = value;
            }
        }

        public Color UncheckedTitleColor
        {
            get
            {
                return _uncheckedTitleColor;
            }

            set
            {
                _uncheckedTitleColor = value;
                if (!Checked)
                    _titleLabel.TextColor = value;
            }
        }

        public string CheckedImage
        {
            get
            {
                return _checkedImage;
            }

            set
            {
                _checkedImage = value;
                if (Checked)
                    _btn.Image = _checkedImage;

            }
        }

        public string UncheckedImage
        {
            get
            {
                return _uncheckedImage;
            }

            set
            {
                _uncheckedImage = value;
                if (!Checked)
                    _btn.Image = _uncheckedImage;

            }
        }

        public bool Checked
        {
            get
            {
                return (bool)GetValue(CheckedProperty);
            }

            set
            {
                bool curValue = (bool)GetValue(CheckedProperty);

                if (curValue != value)
                {
                    SetValue(CheckedProperty, value);
                    if (value)
                    {
                        _titleLabel.TextColor = _checkedTitleColor;
                        _btn.Image = _checkedImage;
                        if (!string.IsNullOrWhiteSpace(_checkedTitle))
                            _titleLabel.Text = _checkedTitle;
                    }
                    else
                    {
                        _titleLabel.TextColor = _uncheckedTitleColor;
                        _btn.Image = _uncheckedImage;
                        if (!string.IsNullOrWhiteSpace(_uncheckedTitle))
                            _titleLabel.Text = _uncheckedTitle;
                    }
                }
            }
        }

        public Color BorderColor
        {
            get
            {
                return _btn.BorderColor;
            }

            set
            {
                _btn.BorderColor = value;
            }
        }

        public int BorderRadius
        {
            get
            {
                return _btn.BorderRadius;
            }

            set
            {
                _btn.BorderRadius = value;
            }
        }

        public double BorderWidth
        {
            get
            {
                return _btn.BorderWidth;
            }

            set
            {
                _btn.BorderWidth = value;
            }
        }

        public object UserData { get; set; }

        public new double WidthRequest
        {
            get
            {
                return _btn.WidthRequest;
            }

            set
            {
                _btn.WidthRequest = value;
            }
        }

        public new double HeightRequest
        {
            get
            {
                return _btn.HeightRequest;
            }

            set
            {
                _btn.HeightRequest = value;
            }
        }

        public ToggleButton()
        {
            RowDefinitions = new RowDefinitionCollection
            {
                new RowDefinition { Height = GridLength.Auto },
            };

            ColumnDefinitions = new ColumnDefinitionCollection
            {
                new ColumnDefinition { Width = GridLength.Star }
            };

            _btn = new Button
            {
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.Fill,
                BackgroundColor = Color.Transparent,
                WidthRequest = 150
            };
            _btn.Clicked += btnClicked;

            _titleLabel = new Label
            {
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                FontSize = 14,
                HorizontalTextAlignment = TextAlignment.Center,
                InputTransparent = true,
                TextColor = _uncheckedTitleColor
            };

            Children.Add(_btn, 0, 0);
            Children.Add(_titleLabel, 0, 0);
        }

        private void btnClicked(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            if (!Checked)
            {
                btn.Image = _checkedImage;
                _titleLabel.TextColor = _checkedTitleColor;
                if (!string.IsNullOrWhiteSpace(_checkedTitle))
                    _titleLabel.Text = _checkedTitle;
            }
            else
            {
                btn.Image = _uncheckedImage;
                _titleLabel.TextColor = _uncheckedTitleColor;
                if (!string.IsNullOrWhiteSpace(_uncheckedTitle))
                    _titleLabel.Text = _uncheckedTitle;
            }
            Checked = !Checked;

            if (Clicked != null)
                Clicked.Invoke(this, e);
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == CheckedProperty.PropertyName)
            {
                bool value = (bool)GetValue(CheckedProperty);

                if (value)
                {
                    _titleLabel.TextColor = _checkedTitleColor;
                    _btn.Image = _checkedImage;
                }
                else
                {
                    _titleLabel.TextColor = _uncheckedTitleColor;
                    _btn.Image = _uncheckedImage;
                }
            }
            else if (propertyName == HeightRequestProperty.PropertyName)
            {
                _btn.HeightRequest = (double)GetValue(HeightRequestProperty);
            }
            else if (propertyName == WidthRequestProperty.PropertyName)
            {
                _btn.WidthRequest = (double)GetValue(WidthRequestProperty);
            }
        }
    }
}
