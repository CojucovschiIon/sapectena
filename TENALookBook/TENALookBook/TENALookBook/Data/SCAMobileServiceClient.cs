﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCAMobile.Data
{
    public class SCAMobileServiceClient : SCAService.SCAMobileServiceClient
    {
        public SCAMobileServiceClient() : base(GetBinding(), GetEndpointAddress())
        {
            this.Endpoint.EndpointBehaviors.Add(new SCAServiceMessageInspectorBehavior());
        }

        private static System.ServiceModel.Channels.Binding GetBinding()
        {
            System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
            result.MaxBufferSize = int.MaxValue;
            result.MaxReceivedMessageSize = int.MaxValue;
            return result;
        }

        private static System.ServiceModel.EndpointAddress GetEndpointAddress()
        {
            return new System.ServiceModel.EndpointAddress(Globals.WEB_SERVICE_URL);
        }

    }
}
