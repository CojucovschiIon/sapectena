﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FormsToolkit;
using Xamarin.Forms;
using SCAMobile.Constants;
using Acr.UserDialogs;
using SCAMobile.SCAService;
using SCAMobile.Pages;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SCAMobile
{
    public partial class App : Application
    {
        private SCAMobile.Pages.MainUITabBar _tabbar = new Pages.MainUITabBar();
        private string _deviceToken;
        private bool _hasNewNotifications;

        public App()
        {
            RecentAccounts = new List<int>();
            RecentComparisons = new List<int>();

            if (Properties.ContainsKey(ConfigKeys.RecentAccounts) && !string.IsNullOrWhiteSpace(Properties[ConfigKeys.RecentAccounts] as string))
            {
                string value = Properties[ConfigKeys.RecentAccounts] as string;
                string[] ids = value.Split(@",".ToCharArray());

                RecentAccounts.AddRange(ids.Select(m => int.Parse(m)));
            }

            if (Properties.ContainsKey(ConfigKeys.RecentComparisons) && !string.IsNullOrWhiteSpace(Properties[ConfigKeys.RecentComparisons] as string))
            {
                string value = Properties[ConfigKeys.RecentComparisons] as string;
                string[] ids = value.Split(@",".ToCharArray());

                RecentComparisons.AddRange(ids.Select(m => int.Parse(m)));
            }

            if (Properties.ContainsKey(ConfigKeys.DeviceToken) && !string.IsNullOrWhiteSpace(Properties[ConfigKeys.DeviceToken] as string))
            {
                _deviceToken = Properties[ConfigKeys.DeviceToken] as string;
            }

            if (Properties.ContainsKey(ConfigKeys.HasNewNotifications) && Properties[ConfigKeys.HasNewNotifications] is bool)
            {
                _hasNewNotifications = (bool)Properties[ConfigKeys.HasNewNotifications];
            }

            InitializeComponent();

            SubscribeToDisplayAlertMessages();

            // create a new NavigationPage
            var navPage = new NavigationPage(new SCAMobile.Pages.MainPage());

            navPage.BarTextColor = Color.White;

            // set the MainPage of the app to the navPage
            MainPage = navPage;
        }

        public event EventHandler LoginCompleted;

        public event EventHandler LogoutCompleted;

        public event EventHandler NotificationsRead;

        public string DeviceToken
        {
            get
            {
                return _deviceToken;
            }

            set
            {
                if (_deviceToken == value)
                    return;

                if (!string.IsNullOrWhiteSpace(value))
                {
                    SCAMobile.Data.SCAMobileServiceClient client = new SCAMobile.Data.SCAMobileServiceClient();

                    client.registerDeviceCompleted += registerDeviceCompleted;
                    client.registerDeviceAsync(Login.Token, value, DeviceType);
                }

                _deviceToken = value;

                Properties[ConfigKeys.DeviceToken] = value;
                SavePropertiesAsync();
            }
        }

        private void registerDeviceCompleted(object sender, SCAService.registerDeviceCompletedEventArgs e)
        {
        }

        public int DeviceType { get; set; }

        public Guid LoginToken
        {
            get { return Login.Token; }
        }

        public Login Login { get; set; }

        public List<int> RecentAccounts { get; set; }

        public List<int> RecentComparisons { get; set; }

        public SCAMobile.Pages.MainUITabBar TabBar
        {
            get { return _tabbar; }
        }

        public bool HasNewNotifications
        {
            get
            {
                return _hasNewNotifications;
            }

            set
            {
                _hasNewNotifications = value;
                Properties[ConfigKeys.HasNewNotifications] = _hasNewNotifications;
                SavePropertiesAsync();
            }
        }

        public bool SearchConfidentialityNoticeShown { get; set; }

        public void AddAccountToRecentList(int accountId)
        {
            if (RecentAccounts.Contains(accountId))
                RecentAccounts.Remove(accountId);

            RecentAccounts.Insert(0, accountId);
            Properties[ConfigKeys.RecentAccounts] = string.Join(@",", RecentAccounts);
            SavePropertiesAsync();
        }

        public void AddComparisonToRecentList(int id)
        {
            if (RecentComparisons.Contains(id))
                RecentComparisons.Remove(id);

            RecentComparisons.Insert(0, id);
            Properties[ConfigKeys.RecentComparisons] = string.Join(@",", RecentComparisons);
            SavePropertiesAsync();
        }

        public void RemoveComparisonFromRecentList(int id)
        {
            if (RecentComparisons.Contains(id))
            {
                RecentComparisons.Remove(id);

                Properties[ConfigKeys.RecentComparisons] = string.Join(@",", RecentComparisons);
                SavePropertiesAsync();
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        /// <summary>
        /// Subscribes to messages for displaying alerts.
        /// </summary>
        static void SubscribeToDisplayAlertMessages()
        {
            MessagingService.Current.Subscribe<MessagingServiceAlert>(MessageKeys.DisplayAlert, (service, info) =>
            {
                Device.BeginInvokeOnMainThread(new Action(async () =>
                {
                    var task = Current?.MainPage?.DisplayAlert(info.Title, info.Message, info.Cancel);
                    if (task != null)
                    {
                        await task;
                        info?.OnCompleted?.Invoke();
                    }
                }));
            });

            MessagingService.Current.Subscribe<MessagingServiceQuestion>(MessageKeys.DisplayQuestion, (service, info) => {
                Device.BeginInvokeOnMainThread(new Action(async () =>
                {
                    var task = Current?.MainPage?.DisplayAlert(info.Title, info.Question, info.Positive, info.Negative);
                    if (task != null)
                    {
                        var result = await task;
                        info?.OnCompleted?.Invoke(result);
                    }
                }));
            });
        }

        /// <summary>
        /// Shows the loading indicator.
        /// </summary>
        /// <param name="isRunning">If set to <c>true</c> is running.</param>
        /// <param name = "isCancel">If set to <c>true</c> user can cancel the loading event (just uses PopModalAync here)</param>
        public static void ShowLoading(bool isRunning, string title = @"Loading", bool isCancel = false)
        {
            if (isRunning == true)
            {
                if (isCancel == true)
                {
                    UserDialogs.Instance.Loading(title, new Action(async () => {
                        if (Application.Current.MainPage.Navigation.ModalStack.Count > 1)
                        {
                            await Application.Current.MainPage.Navigation.PopModalAsync();
                        }
                        else
                        {
                            System.Diagnostics.Debug.WriteLine("Navigation: Can't pop modal without any modals pushed");
                        }
                        UserDialogs.Instance.Loading().Hide();
                    }));
                }
                else
                {
                    UserDialogs.Instance.ShowLoading(title);
                }
            }
            else
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        public static string getProductImage(SCAService.Product product)
        {
            string[] productImageTypes =
            {
                @"PF",
                @"PB",
                @"PI",
                @"PO",
                @"PRF",
                @"PRB",
                @"PS",
            };

            foreach (string it in productImageTypes)
            {
                string compProductImage = getProductImage(product, it);
                if (compProductImage != null)
                    return compProductImage;
            }

            App app = App.Current as App;

            if (app.Login.SCACompanyId == product.CompanyId)
                return @"tena-product.png";
            else
                return @"product.png";
        }

        private static string getProductImage(SCAService.Product product, string type)
        {
            string compProductImage = product.Images.Where(m => m.Type == type).Select(m => m.ImageUrl).FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(compProductImage))
                return compProductImage;
            else
                return null;
        }

        public static string getYouTubeVideoId(string url)
        {
            if (url.ToLower().Contains(@"youtube.com"))
            {
                string videoId = string.Empty;
                int position = url.LastIndexOfAny(@"=/".ToCharArray());

                if (position > -1)
                    videoId = url.Substring(position + 1);

                if (!string.IsNullOrWhiteSpace(videoId))
                    return videoId;
            }

            return null;
        }

        public static void scaServiceUnavailableErrorMessage(Exception e)
        {
            //Trace.WriteLine(DateTime.Now.ToString() + @": " + e.ToString() + "\r\n");

            MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
            {
                Title = @"Error",
                Cancel = @"OK",
                Message = @"SCA mobile service is currently unavailable. Please check that you have internet access and try again."
            });
        }

        public static void comparisionDeletedErrorMessage()
        {
            MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.DisplayAlert, new MessagingServiceAlert()
            {
                Title = @"Error",
                Cancel = @"OK",
                Message = @"The comparision does not exist anymore."
            });
        }

        public static void notificationReceived()
        {
            App app = App.Current as App;

            app.HasNewNotifications = true;
            MessagingService.Current.SendMessage(MessageKeys.NotificationReceived, new object());
        }

        public static void resetNotifications()
        {
            App app = App.Current as App;

            app.HasNewNotifications = false;
            MessagingService.Current.SendMessage(MessageKeys.ResetNotificationBadge, new object());
            notifyNotificationsRead();
        }

        public static void notifyLoginCompleted()
        {
            App app = App.Current as App;

            if (app.LoginCompleted != null)
                app.LoginCompleted.Invoke(app, new EventArgs());
        }

        public static void notifyLogoutCompleted()
        {
            App app = App.Current as App;

            if (app.LogoutCompleted != null)
                app.LogoutCompleted.Invoke(app, new EventArgs());
        }

        public static void notifyNotificationsRead()
        {
            App app = App.Current as App;

            if (app.NotificationsRead != null)
                app.NotificationsRead.Invoke(app, new EventArgs());
        }
    }
}
