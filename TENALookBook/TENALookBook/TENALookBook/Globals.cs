﻿using System.Collections.Generic;

namespace SCAMobile
{
    public class PerformanceMetric
    {
        public string PropName { get; set; }

        public string DisplayName { get; set; }
    }

    public static class Globals
    {
        //public static string WEB_SERVICE_URL = @"http://192.168.0.36:100/SCAMobileService.svc";
        public static string WEB_SERVICE_URL = @"http://scamobileservice.valexconsulting.com/SCAMobileService.svc";

        public static List<PerformanceMetric> PerformanceMetrics = new List<PerformanceMetric>
        {
            new PerformanceMetric { PropName = @"ProductWeight", DisplayName = @"Product Weight (g)" },
            new PerformanceMetric { PropName = @"ProductThickness", DisplayName = @"Product Thickness (mm)" },
            new PerformanceMetric { PropName = @"Acq1", DisplayName = @"Briefs Acq Time (SCA): First Insult" },
            new PerformanceMetric { PropName = @"Acq2", DisplayName = @"Briefs Acq Time (SCA): Second Insult" },
            new PerformanceMetric { PropName = @"Acq3", DisplayName = @"Briefs Acq Time (SCA): Third Insult" },
            new PerformanceMetric { PropName = @"Acq12", DisplayName = @"Underwear and Pads Acq Time (SCA): First Insult" },
            new PerformanceMetric { PropName = @"Acq22", DisplayName = "Underwear and Pads Acq Time (SCA): Second Insult" },
            new PerformanceMetric { PropName = @"Rewet", DisplayName = @"Rewet" },
            new PerformanceMetric { PropName = @"PackageLength", DisplayName = @"Package Length (mm)" },
            new PerformanceMetric { PropName = @"PackageWidth", DisplayName = @"Package Width (mm)" },
            new PerformanceMetric { PropName = @"PackageHeight", DisplayName = @"Package Height (mm)" },
            new PerformanceMetric { PropName = @"TotalProductLength", DisplayName = @"Total Product Length (mm)" },
            new PerformanceMetric { PropName = @"TotalProductWidthFront", DisplayName = @"Total Product Width Front (mm)" },
            new PerformanceMetric { PropName = @"TotalProductWidthCrotch", DisplayName = @"Total Product Width Crotch (mm)" },
            new PerformanceMetric { PropName = @"TotalProductWidthRear", DisplayName = @"Total Product Width Rear (mm)" },
            new PerformanceMetric { PropName = @"OnePieceStretchWing", DisplayName = "One piece Stretch Wing Length x Width (mm)" },
            new PerformanceMetric { PropName = @"SmallCoreLength", DisplayName = @"Small Core Length (mm)" },
            new PerformanceMetric { PropName = @"SmallCoreWidth", DisplayName = @"Small Core Width (mm)" },
            new PerformanceMetric { PropName = @"BigCoreLength", DisplayName = @"Big Core Length (mm)" },
            new PerformanceMetric { PropName = @"BigCoreWidthCrotch", DisplayName = @"Big Core Width Crotch (mm)" },
            new PerformanceMetric { PropName = @"BigCoreWidthEnds", DisplayName = @"Big Core Width Ends (mm)" },
            new PerformanceMetric { PropName = @"TransferLayer", DisplayName = @"Transfer Layer: L x W (mm)" },
            new PerformanceMetric { PropName = @"Tll", DisplayName = @"TL L" },
            new PerformanceMetric { PropName = @"Tlw", DisplayName = @"TL W" },
            new PerformanceMetric { PropName = @"TransferLayerColorMaterial", DisplayName = @"Transfer Layer: Color & Material" },
            new PerformanceMetric { PropName = @"Coverstock", DisplayName = @"Coverstock (Topsheet)" },
            new PerformanceMetric { PropName = @"BackingMaterial", DisplayName = @"Backing Material & Color" },
            new PerformanceMetric { PropName = @"CompressionPattern", DisplayName = @"Compression pattern" },
            new PerformanceMetric { PropName = @"StandingGathersMaterial", DisplayName = @"Standing Gathers (material)" },
            new PerformanceMetric { PropName = @"StandingGathersElastics", DisplayName = "Standing Gathers Elastics (side)" },
            new PerformanceMetric { PropName = @"LegElasticsFront", DisplayName = @"Leg Elastics front" },
            new PerformanceMetric { PropName = @"LegElasticsBack", DisplayName = @"Leg Elastics back" },
            new PerformanceMetric { PropName = @"Reinforcement", DisplayName = @"Reinforcement (tissue, NW)" },
            new PerformanceMetric { PropName = @"WaistElasticsFront", DisplayName = @"Waist elastics front" },
            new PerformanceMetric { PropName = @"WaistElasticsBack", DisplayName = @"Waist elastics back" },
            new PerformanceMetric { PropName = @"WetnessIndicator", DisplayName = @"Wetness Indicator" },
            new PerformanceMetric { PropName = @"SizePrinted", DisplayName = @"Size printed?" },
            new PerformanceMetric { PropName = @"AbsorbencyPrinted", DisplayName = @"Absorbency printed?" },
            new PerformanceMetric { PropName = @"TapeHook", DisplayName = @"Tape/Hook" },
            new PerformanceMetric { PropName = @"BeltSidePanels", DisplayName = @"Belt/side Panels" },
            new PerformanceMetric { PropName = @"MedicalRateAcq", DisplayName = @"MediCal Rate of Acquisition (sec.)" },
            new PerformanceMetric { PropName = @"MedicalRewet", DisplayName = @"MediCal Rewet (g)" },
            new PerformanceMetric { PropName = @"MedicalTotAbs", DisplayName = @"Medi-Cal Total Absorption Capacity (g)" },
            new PerformanceMetric { PropName = @"MedicalRetCap", DisplayName = @"MediCal Retention Capacity (g)" },
            new PerformanceMetric { PropName = @"MedicalAirPermCfm", DisplayName = @"MediCal Air Permeability (cfm)" },
            new PerformanceMetric { PropName = @"MedicalAirPerm_mm", DisplayName = @"MediCal Air Permeability (mm/sec)" },
            new PerformanceMetric { PropName = @"MedicalElasElong", DisplayName = @"MediCal Elastic Elongation (%)" },
            new PerformanceMetric { PropName = @"BellyElasFront", DisplayName = @"Belly Elastics Front" },
            new PerformanceMetric { PropName = @"BellyElasBack", DisplayName = @"Belly Elastics Back" },
            new PerformanceMetric { PropName = @"CrotchElas", DisplayName = @"Crotch Elastics (/side)" },
            new PerformanceMetric { PropName = @"ElasThreadType", DisplayName = @"Elastic thread type" },
            new PerformanceMetric { PropName = @"FrBackIndic", DisplayName = @"Front & Back indicator" },
            new PerformanceMetric { PropName = @"AsbDryWeigt", DisplayName = @"Absorption / Dry weight (g/g)" },
            new PerformanceMetric { PropName = @"CoreShape", DisplayName = @"Core shape" },
            new PerformanceMetric { PropName = @"SheetSize", DisplayName = @"Sheet size" },
            new PerformanceMetric { PropName = @"ClosureType", DisplayName = @"Closure type" },
            new PerformanceMetric { PropName = @"DispMethod", DisplayName = @"Dispensing method" },
            new PerformanceMetric { PropName = @"DryStrengthStiffMd", DisplayName = @"Dry strength NW - Stiffness MD (kN/m)" },
            new PerformanceMetric { PropName = @"DryStrengthStiffCd", DisplayName = @"Dry strength NW - Stiffness CD (kN/m)" },
            new PerformanceMetric { PropName = @"DryStrengthStrengthMd", DisplayName = @"Dry strength NW - Strength MD (N/m)" },
            new PerformanceMetric { PropName = @"DryStrengthStrengthCd", DisplayName = @"Dry strength NW - Strength CD (N/m)" },
            new PerformanceMetric { PropName = @"DryStrengthStrengthIndex", DisplayName = @"Dry strength NW - Strength Index MD/CD (Nm/g)" },
            new PerformanceMetric { PropName = @"DryStrengthRatio", DisplayName = @"Dry strength NW - Ratio MD/CD" },
            new PerformanceMetric { PropName = @"DryStrengthStretchMd", DisplayName = @"Dry strength NW - Stretch MD (%)" },
            new PerformanceMetric { PropName = @"DryStrengthStretchCd", DisplayName = @"Dry strength NW - Stretch CD (%)" },
            new PerformanceMetric { PropName = @"DryStrengthStretchMdCd", DisplayName = @"Dry strength NW - Stretch MD/CD (%)" },
            new PerformanceMetric { PropName = @"DryStrengthWorkMd", DisplayName = @"Dry strength NW - Work MD (J/m²)" },
            new PerformanceMetric { PropName = @"DryStrengthWorkCd", DisplayName = @"Dry strength NW - Work CD (J/m²)" },
            new PerformanceMetric { PropName = @"DryStrengthWorkIndexMdCd", DisplayName = @"Dry strength NW - Work index MD/CD (J/g)" },
            new PerformanceMetric { PropName = @"BulktigoWetThick", DisplayName = @"BulkTiGo Wet - Thickness (microns)" },
            new PerformanceMetric { PropName = @"BulktigoWetGram", DisplayName = @"BulkTiGo Wet - grammage (g/m²)" },
            new PerformanceMetric { PropName = @"BulktigoWetBulk", DisplayName = @"BulkTiGo Wet - Bulk (cm³/g)" },
            new PerformanceMetric { PropName = @"BulktigoDryGram", DisplayName = @"BulkTiGo Dry - grammage (g/m²)" },
            new PerformanceMetric { PropName = @"BulktigoDryCm", DisplayName = @"BulkTiGo Dry - Bulk (cm³/g)" },
            new PerformanceMetric { PropName = @"ResidueCed", DisplayName = @"Residue in CED (%)" },
       };
    }
}
