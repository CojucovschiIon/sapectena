﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Notifications;
using Windows.Data.Xml.Dom;
using Windows.Networking.PushNotifications;
using System.Threading.Tasks;

namespace SCAMobile.UWP
{
    public sealed partial class MainPage
    {
        private PushNotificationChannel _channel;

        public MainPage()
        {
            this.InitializeComponent();

            SCAMobile.App app = new SCAMobile.App() { DeviceType = 1 };
            app.LoginCompleted += loginCompleted;
            app.LogoutCompleted += logoutCompleted;
            app.NotificationsRead += notificationsRead;

            LoadApplication(app);
        }

        private void notificationsRead(object sender, EventArgs e)
        {
        }

        private void logoutCompleted(object sender, EventArgs e)
        {
            if (_channel != null)
            {
                _channel.Close();

                SCAMobile.App app = SCAMobile.App.Current as SCAMobile.App;
                _channel = null;
            }
        }

        private void loginCompleted(object sender, EventArgs e)
        {
            registerForNotificationsAsync();
        }

        private async void registerForNotificationsAsync()
        {
            try
            {
                _channel = await PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();
                SCAMobile.App app = SCAMobile.App.Current as SCAMobile.App;
                app.DeviceToken = _channel.Uri.ToString();

                _channel.PushNotificationReceived += pushNotificationReceived;
            }
            catch (Exception ex)
            {
            }
        }

        private void pushNotificationReceived(PushNotificationChannel sender, PushNotificationReceivedEventArgs args)
        {
            SCAMobile.App.notificationReceived();
        }
    }
}
