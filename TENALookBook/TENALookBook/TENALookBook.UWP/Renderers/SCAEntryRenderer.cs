﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;
using SCAMobile.Controls;

[assembly: ExportRenderer(typeof(SCAEntry), typeof(SCAMobile.UWP.Renderers.SCAEntryRenderer))]
namespace SCAMobile.UWP.Renderers
{
    public class SCAEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            SCAEntry entry = (SCAEntry)this.Element;

            if (this.Control != null)
            {
                if (entry != null)
                {
                    this.Control.KeyDown += (object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs eventArgs) =>
                    {
                        if (eventArgs.Key == Windows.System.VirtualKey.Enter)
                        {
                            entry.InvokeCompleted();
                            // Make sure to set the Handled to true, otherwise the RoutedEvent might fire twice
                            eventArgs.Handled = true;
                        }
                    };
                }
            }
        }
    }
}
