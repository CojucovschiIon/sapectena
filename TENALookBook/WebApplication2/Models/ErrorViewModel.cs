using System;
using System.Collections.Generic;

namespace WebApplication2.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }

    public class CompanyProduct
    {
        public string Article { get; set; }
        public string Size { get; set; }
        public int? BagCount { get; set; }
        public int? Pieces { get; set; }
        public string BackgroundColor { get; set; }
        public string Category { get; set; }
    }

    public class Companies
    {
        public List<CompanyProduct> companyProducts { get; set; }
        public string CompanyName { get; set; }
        public int CompanyId { get; set; }
    }


}