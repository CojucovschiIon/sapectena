﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication2.HElpers;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            EmailGenerator emailGenerator = new EmailGenerator();
            string mesa = "";
            string SMTPSERVER = "smtp.gmail.com";
            int PORTNO = 587;
            string toClientEmail = "cojucovschi@bk.ru";


            SmtpClient smtpClient = new SmtpClient(SMTPSERVER, PORTNO);
            smtpClient.EnableSsl = true;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new NetworkCredential("readabookesy@gmail.com", "******");
            using (MailMessage message = new MailMessage())
            {
                message.From = new MailAddress(toClientEmail);
                message.Subject = "Essity MEssage Comparation message";
                message.Body = emailGenerator.renderHtmlPageMessage();//renderHtmlPageMessage();
                message.IsBodyHtml = true;

                message.To.Add(toClientEmail);

                //if (ccemailTo != null && ccemailTo.Length > 0)
                //{
                //    foreach (string emailCc in ccemailTo)
                //    {
                //        message.CC.Add(emailCc);
                //    }
                //}
                try
                {
                    smtpClient.Send(message);
                    mesa = "success";
                }
                catch (Exception ex)
                {
                    mesa = ex.Message;
                }


                return View("Index",mesa);
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        private string renderHtmlPageMessage()
        {
            string response = "<!DOCTYPE html PUBLIC ' -//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>";
            string hreader = @"<html xmlns='http://www.w3.org/1999/xhtml'>
                                 <head>
                                  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
                                       <title> Demystifying Email Design</title>
                                          <meta name = 'viewport' content = 'width=device-width, initial-scale=1.0' />
                                </head> ";
            string element = @"<div width='260' height='600' style='display: inline-block;
                                  color: #000000;
                                  text-align: center;
                                  padding: 14px;
                                  margin:10px;
                                  text-decoration: none;'>
                                    <div>Company Name</div>
                                    <br/>
                                    <div>1</div>
                                    <div>2</div>
                                    <div>3</div>
                                    <div>4</div>
                                    <div>5</div>
                               </div>";
            string bodddy = @"<body style='margin: 0; padding: 0;'>
                                     <table border='1' cellpadding='0' cellspacing='0' width='100%'>
                                      <tr>
                                       <td>
                                        <table align='center' border='1' cellpadding='0' cellspacing='0' width='600'>
                                         <tr>
                                          <td style='padding: 40px 0 30px 0; '>
                                           <h1>Hello!</h1><br/>
                                           <h3>This message is confidential</h3>
                                          </td>
                                         </tr>
                                         <tr>
                                          <td style='padding: 20px 0 30px 0; '>
                                              <div style='
                                      overflow: auto;
                                      white-space: nowrap;'>
                                                " +
                              element + element + element + element +
                              element + element + element + element +
                              element + element + element + element +
                              element + element + element + element +
                              element + element + element + element
                              + @"        </div>
                                          </td>
                                         </tr>
                                         <tr>
                                          <td>
                                           Row 3
                                          </td>
                                         </tr>
                                        </table>
                                       </td>
                                      </tr>
                                     </table>
                                    </body>";
            string footer = "</html>";
            return response + hreader + bodddy + footer;
                
        }
    }
}
