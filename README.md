There are four folders. Three of them are for building TENA LookBook client app:

CarouselView-master
TENALookBook
xamarin-range-slider-master

The fourth one - SCAMobileService - is for building the web service the client app uses.

To build TENA LookBook client app open TENALoolBool/TENALookBook.sln solution. For iOS version build TENALooBook.iOS project. For Windows version build TENALooBook.UWP project.

The URL to the web service is defined in Globals.cs in
TENALookBook(portable) project. Look for WEB_SERVICE_URL.

To build the client app (both iOS and Windows versions) you need a PC with MS Visual Studio 2015 14.0.25431.01 Update 3 with Xamarin 4.2.2.11 and Xamarin.iOS 10.3.1.8. For the iOS version you also need a Mac with Xamarin Studio Community edition 6.1.4(build 1). Newer versions of all tools and frameworks may also be OK but I have not tried them. Xamarin in general is very unstable and sensitive thing. Using versions of tools and frameworks other than ones given may break things in the least expected way.

The web service - SCAMobileService - may be build with MS Visual Studio
2013 and later. This is a pure .NET project. The script to build the database and the latest dump of the database are located in SCAMobileService/Database.

[Provided by Serguei Soltan, ext]